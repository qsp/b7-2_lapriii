/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              virtual void escreve(ostream &out) const                     */ 
/*              const TarefaDelegada& operator = (const TarefaDelegada &p)   */                                                   
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/


#include "TarefaDelegada.h"

TarefaDelegada::TarefaDelegada() {
}

TarefaDelegada::TarefaDelegada(Tarefa &tarefa, int idUtilizadorDestino, char publica, char aceite): Tarefa(tarefa){
    this->idUtilizadorDestino = idUtilizadorDestino;
    this->nomeUtilizador = "";
    this->nomeUtilizadorDestino = "";
    setPublicaChar(publica);
    setAceiteChar(aceite);
}

TarefaDelegada::TarefaDelegada(Tarefa &tarefa, Utilizador origem, Utilizador destino, char publica, char aceite): Tarefa(tarefa){
    this->idUtilizadorDestino = destino.GetIdUtilizador();
    this->nomeUtilizador = origem.GetNome();
    this->nomeUtilizadorDestino = destino.GetNome();
    setPublicaChar(publica);
    setAceiteChar(aceite);
}

TarefaDelegada::~TarefaDelegada() {
}

TarefaDelegada::TarefaDelegada(const TarefaDelegada & td) : Tarefa(td){
    this->idUtilizadorDestino = td.idUtilizadorDestino;
    this->nomeUtilizador = td.nomeUtilizador;
    this->nomeUtilizadorDestino = td.nomeUtilizadorDestino;
    this->aceite = td.aceite;
    this->visualizada = td.visualizada;
}

void TarefaDelegada::setNomeUtilizador(string nomeUtilizador) {
    this->nomeUtilizador = nomeUtilizador;
}

string TarefaDelegada::getNomeUtilizador() const {
    return nomeUtilizador;
}

void TarefaDelegada::setNomeUtilizadorDestino(string nomeUtilizadorDestino) {
    this->nomeUtilizadorDestino = nomeUtilizadorDestino;
}

string TarefaDelegada::getNomeUtilizadorDestino() const {
    return nomeUtilizadorDestino;
}

void TarefaDelegada::setAceiteChar(char aceite) {
    switch (aceite){
        case ACEITE_TRUE:
            setAceite(true);
            break;
        case ACEITE_FALSE:
            setAceite(false);
            break;            
        default:
            setVisualizada(false);            
    }
}

char TarefaDelegada::getAceiteChar() const {
    if (visualizada) {
        if (aceite)
            return ACEITE_TRUE;
        else
            return ACEITE_FALSE;
    }
    return 0;
}

void TarefaDelegada::setPublicaChar(char publica)
{
    switch(publica){
        case PUBLICA_TRUE:
            this->publica = true;
            break;
        default:
            this->publica = false;
    }
}

char TarefaDelegada::getPublicaChar() const
{
    if(publica)
        return PUBLICA_TRUE;
    return PUBLICA_FALSE;
}

void TarefaDelegada::setAceite(bool aceite){
    this->aceite = aceite;
    this->visualizada = true;
}

bool TarefaDelegada::isAceite() const{
    return aceite;
}

void TarefaDelegada::setVisualizada(bool visualizada){
    this->visualizada = visualizada;
    this->aceite = visualizada ? aceite : false;
}

bool TarefaDelegada::isVisualizada() const{
    return visualizada;
}

void TarefaDelegada::setIdUtilizadorDestino(int idUtilizadorDestino)
{
    this->idUtilizadorDestino = idUtilizadorDestino;
}

int TarefaDelegada::getIdUtilizadorDestino() const
{
    return idUtilizadorDestino;
}

void TarefaDelegada::escreve(ostream &out) const {
    Tarefa::escreve(out);
//    out<<"|"<< "Destinatário: " << idUtilizadorDestino
//            << "|" << (publica? "Publica" : "Privada")
//            <<"|"<< (visualizada? "Visualizada" : "Não Visualizada");
//    if (visualizada)
//        out <<"|"<< (aceite? "Aceite":"Não Aceite");
}

const TarefaDelegada& TarefaDelegada::operator = (const TarefaDelegada &t){
    Tarefa::operator=(t);
    this->idUtilizadorDestino = t.idUtilizadorDestino;
    this->aceite = t.aceite;
    this->visualizada = t.visualizada;
    return *this;
}

ostream& operator <<(ostream &out, const TarefaDelegada &t){
    t.escreve(out);
    return out;
}
