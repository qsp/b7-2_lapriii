/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*               void escreve(ostream &out) const                            */
/*               const Projeto& operator =(const Projeto &p                  */
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/



#ifndef PROJETO_H
#define	PROJETO_H
#include <iostream>
#include <string>
#include "AcaoBase.h"
#include "Tarefa.h"

using namespace std;

class Projeto : public AcaoBase {
    friend class BDados;
private:
    list<Tarefa> tarefas;
public:
    Projeto();
    Projeto(AcaoBase b);
    void SetTarefas(list<Tarefa> tarefas);
    list<Tarefa> GetTarefas() const;
    void escreve(ostream &out) const;
    const Projeto& operator =(const Projeto &p);

};

ostream& operator <<(ostream &out, const Projeto &p);

#endif	/* PROJETO_H */

