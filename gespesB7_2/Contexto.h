/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              void escreve(ostream& out) const                             */
/*              const Contexto& operator=(const Contexto& c)                 */
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/



#ifndef CONTEXTO_H
#define	CONTEXTO_H
#include <iostream>
#include <string>
#include <iomanip>
#include <occi.h>
using namespace std;
using namespace oracle::occi;

class Contexto {
    friend class BDados;
        private:
            int idUtilizador;
            int idContexto;
            string nome;
        public:
            Contexto();
            Contexto(int idUtilizador,const string & nome);
            Contexto(int idUtilizador,int idContexto,const string & nome);
            
            int getIdUtilizador() const;
            int getIdContexto() const;
            string getNome() const;
            void setIdContexto(int idContexto);
            void setNome(const string & nome);
            void escreve(ostream& out) const;
            const Contexto& operator=(const Contexto& c);
};

ostream& operator<< (ostream& out,const Contexto& c);


#endif	/* CONTEXTO_H */

