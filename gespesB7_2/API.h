/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              static Date stringToDate(string date)                        */
/*              static string intervalDSToString(IntervalDS intervalo)       */
/*              static IntervalDS readIntervalDS()                           */
/*              static bool simNao()                                         */
/*              static void waitForLine()                                    */
/*              static Utilizador criarUtilizador(const string nome)         */
/*              static void editaAcaoBase(AcaoBase & acaobase)               */
/*              static void mostraAcaoBase(AcaoBase & acaobase)              */
/*              static Contexto leContexto()                                 */
/*              static void mostraContexto(Contexto & contexto)              */
/*              static Contexto listContextos(list<Contexto> contextos)      */
/*              static void editaContexto(Contexto & contexto)               */
/*              static Referencia listReferencias(list<Referencia>)          */
/*              static void mostraReferencia(Referencia & ref)               */
/*              static Tarefa * listTarefas(list<Tarefa *>)                  */
/*              static Anotacao listAnotacao(list<Anotacao> listAno)         */
/*              static Tarefa leTarefa()                                     */
/*              static void mostraTarefa(Tarefa & tarefa)                    */
/*              static void editaTarefa(Tarefa & tarefa)                     */
/*              static Projeto listProjetos(list<Projeto>)                   */
/*              static void mostraProjeto(Projeto & projeto)                 */
/*              static void editaProjeto(Projeto & projeto)                  */
/*              static void mostraTarefasDoProjeto(Projeto & projeto)        */
/*              static void loadPath()                                       */
/*              static bool verificaDadosUtilizador(Utilizador & u)          */
/*              static Utilizador leUtilizador()                             */
/*              static Utilizador listaUtilizadores(                         */
/*                      list<Utilizador> utilizadores)                       */
/*              static Utilizador adicionaUtilizador()                       */
/*              static void adicionaLista(list<Tarefa*> tarefas,             */
/*                      const Utilizador& uti)                               */
/*              static void adicionaAnotacao(AcaoBase acaoBase)              */
/*              static bool delegarTarefa(Tarefa& t,int idUtilizador,        */
/*                      bool publica)                                        */
/*              static void setRemoto(bool remoto)                           */
/*              static bool isRemoto()                                       */
/*              static void mudarRemotoLocal()                               */
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/



#ifndef API_H
#define	API_H
#include<occi.h>
#include "Tarefa.h"
#include "BDDados.h"
#include <iostream>
#include "Utilizador.h"

#define SLASHYYYYMMDD "yyyy/mm/dd"
#define SLASHDDMMYYYY "dd/mm/yyyy"
#define DASHYYYYMMDD "yyyy-mm-dd"
#define DASHDDMMYYYY "dd-mm-yyyy"

#define INTERVALDS_COM_DIAS "DDD HH:MM"
#define INTERVALDS_SEM_DIAS "HH:MM"

#define DEFAULT_WAIT_MESSAGE "-\nPrima enter para continuar."
#define PARAMS_PATH "params.txt"

using namespace std;
using namespace oracle::occi;

class API {
public:
    API();
    API(const API& orig);
    virtual ~API();
    static Date stringToDate(string date);
    static string intervalDSToString(IntervalDS intervalo);
    static IntervalDS readIntervalDS();
    
    static string getDateFormat();
    static void setDateFormat(string format);
    
    static bool simNao();
    static void waitForLine();
    
    static Utilizador criarUtilizador(const string nome);
    static void editaAcaoBase(AcaoBase & acaobase);
    static void mostraAcaoBase(AcaoBase & acaobase);
    
    static Contexto leContexto();
    static void mostraContexto(Contexto & contexto);
    static Contexto listContextos(list<Contexto> contextos);
    static void editaContexto(Contexto & contexto);
    
    static Referencia listReferencias(list<Referencia>);
    static void mostraReferencia(Referencia & ref);    
    
    static Tarefa * listTarefas(list<Tarefa *>);
    static Tarefa listTarefas(list<Tarefa> listA);
    static Anotacao listAnotacao(list<Anotacao> listAno);
    
    static Tarefa leTarefa();
    static void mostraTarefa(Tarefa & tarefa);
    static void editaTarefa(Tarefa & tarefa);
    static IntervalDS getDuracaoEstimada(Tarefa & t);
    
    static Projeto listProjetos(list<Projeto>);
    static void mostraProjeto(Projeto & projeto);
    static void editaProjeto(Projeto & projeto);
    static void mostraTarefasDoProjeto(Projeto & projeto);
        
    
    static void setBDados(BDados* bD);    
    static BDados * getBDados();
    
    static void setLocalUser(Utilizador & localUser);    
    static Utilizador getLocalUser();
    
    static void loadPath();
    static void setDropboxPath(string path);
    static string getDropboxPath();
    
    static bool verificaDadosUtilizador(Utilizador & u);
    static Utilizador leUtilizador();
    static Utilizador listaUtilizadores(list<Utilizador> utilizadores);
    static Utilizador adicionaUtilizador();
    
    static void adicionaLista(list<Tarefa*> tarefas,const Utilizador& uti);
    static void adicionaAnotacao(AcaoBase acaoBase);
    
    static bool delegarTarefa(Tarefa& t,Utilizador destino,bool publica);
    static void leDelegacao(list<Tarefa*> aDelegar);

    static void setRemoto(bool remoto);

    static bool isRemoto() ;
    
    static void mudarRemotoLocal();
    
    
private:
    static string dateFormat;
    static string intervalDSFormat;
    static Utilizador localUser;    
    static BDados * bDados;
    static string dropboxPath;
    static bool remoto;
    static string utilizador;
    static string palavra;
    static string bd;
    
};

#endif	/* API_H */

