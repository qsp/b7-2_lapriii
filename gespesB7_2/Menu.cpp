/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              static int menuTarefa()                                      */
/*              static void showMenu()                                       */
/*              static int menuPrincipal()                                   */
/*              static int menuProjeto()                                     */
/*              static int menuReferencia()                                  */
/*              static int menuAnotacao()                                    */
/*              static int menuContexto()                                    */
/*              static void showMenuProjeto(Projeto & projeto)               */
/*              static void showMenuReferencia(Referencia & ref)             */
/*              static void showMenuContexto(Contexto & contexto)            */
/*              static void showMenuAnotacao(Anotacao & anotacao)            */
/*              static void showMenuTarefa(Tarefa * tarefa)                  */
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/


#include "Menu.h"
#include <iostream>
#include <string>
#include <iomanip>
#include <occi.h>
#include <stdlib.h>
#include <typeinfo>
#include "AcaoBase.h"
#include "API.h"
#include "ListaAdjGrafo.h"
#include "Referencia.h"
#include "CSVio.h"
#include "PriorityQueueTarefa.h"
#include <fstream>
#include "ListaAdjGrafo.h"
#include <unistd.h>

using namespace std;
using namespace oracle::occi;

int Menu::menuPrincipal() {

    int i;
    bool sair;
    do {
        cout << "\033[2J\033[1;1H";
        sair = false;

        cout << "\nEscolha uma das seguintes opções:" << endl
                << "1  - Criar Tarefas" << endl
                << "2  - Gerir Tarefa" << endl
                << "3  - Delegar Tarefas" << endl
                << "4  - Exportar lista tarefas" << endl
                << "5  - Importar Tarefas" << endl
                << "6  - Mostrar mapa de dependência de tarefas" << endl
                << "7  - Gerir Referência" << endl
                << "8  - Gerir Projeto" << endl
                << "9  - Gerir Contextos" << endl
                << "10 - Procurar Tarefas Delegadas" << endl
                << "11 - Obter sugestões de tarefas a realizar" << endl
                << "12 - Listar tarefas para \"qualquer dia\"" << endl
                << "13 - Mudar para Local/Remoto" << endl
                << "14 - Definir local da Pasta Partilhada" << endl
                << "X - Sair" << endl;

        string temp;
        getline(cin, temp);
        if (temp[0] == 'X' || temp[0] == 'x') return 0;
        char * err;
        i = strtol(temp.c_str(), &err, 0);
        if (*err) i = -1;

        if (i < 1 || i > 14) {
            cout << "Introduza uma opção válida" << endl;
            sair = true;
        }
    } while (sair);

    return i;
}

int Menu::menuTarefa() {

    int i;
    bool sair;
    do {
        cout << "\033[2J\033[1;1H";
        sair = false;

        cout << "\nEscolha uma das seguintes opções:" << endl

                << "1 - Editar" << endl
                << "2 - Eliminar" << endl
                << "3 - Marcar como feita" << endl
                << "4 - Diferir" << endl
                << "5 - Atribuir tarefa dependente" << endl
                << "6 - Eliminar dependência de tarefa" << endl
                << "7 - Transformar em referência" << endl
                << "8 - Transformar em Projeto" << endl
                << "9 - Adicionar Anotacao" << endl
                << "10- Listar Anotacões" << endl
                << "X - Voltar" << endl;

        string temp;
        getline(cin, temp);
        if (temp[0] == 'X' || temp[0] == 'x') return 0;
        char * err;
        i = strtol(temp.c_str(), &err, 0);
        if (*err) i = -1;
        if (i < 1 || i > 10) {
            cout << "Introduza uma opção válida" << endl;
            sair = true;
        }
    } while (sair);

    return i;
}

int Menu::menuProjeto() {

    int i;
    bool sair;
    do {
        cout << "\033[2J\033[1;1H";
        sair = false;

        cout << "\nEscolha uma das seguintes opções:" << endl
                << "1 - Ver listagem de tarefas" << endl
                << "2 - Editar" << endl
                << "3 - Eliminar" << endl
                << "4 - Atribuir Tarefa" << endl
                << "5 - Retirar Tarefa" << endl
                << "6 - Visualizar mapa dependência tarefa" << endl
                << "X - Voltar" << endl;
        string temp;
        getline(cin, temp);
        if (temp[0] == 'X' || temp[0] == 'x') return 0;
        char * err;
        i = strtol(temp.c_str(), &err, 0);
        if (*err) i = -1;
        if (i < 1 || i > 6) {
            cout << "Introduza uma opção válida" << endl;
            sair = true;
        }
    } while (sair);

    return i;
}

int Menu::menuReferencia() {

    int i;
    bool sair;
    do {
        cout << "\033[2J\033[1;1H";
        sair = false;

        cout << "\nEscolha uma das seguintes opções:" << endl
                << "1 - Eliminar referência" << endl
                << "2 - Exportar referência" << endl
                << "X - Voltar" << endl;
        string temp;
        getline(cin, temp);
        if (temp[0] == 'X' || temp[0] == 'x') return 0;
        char * err;
        i = strtol(temp.c_str(), &err, 0);
        if (*err) i = -1;
        if (i < 1 || i > 2) {
            cout << "Introduza uma opção válida" << endl;
            sair = true;
        }
    } while (sair);

    return i;
}

int Menu::menuContexto() {

    int i;
    bool sair;
    do {
        cout << "\033[2J\033[1;1H";
        sair = false;

        cout << "\nEscolha uma das seguintes opções:" << endl
                << "1 - Editar contexto" << endl
                << "2 - Eliminar contexto" << endl
                << "X - Voltar" << endl;
        string temp;
        getline(cin, temp);
        if (temp[0] == 'X' || temp[0] == 'x') return 0;
        char * err;
        i = strtol(temp.c_str(), &err, 0);
        if (*err) i = -1;
        if (i < 1 || i > 2) {
            cout << "Introduza uma opção válida" << endl;
            sair = true;
        }
    } while (sair);

    return i;
}

int Menu::menuAnotacao() {

    int i;
    bool sair;
    do {
        cout << "\033[2J\033[1;1H";
        sair = false;

        cout << "\nEscolha uma das seguintes opções:" << endl
                << "1 - Eliminar Anotacao" << endl
                << "2 - Exportar Anotacao" << endl
                << "X - Voltar" << endl;
        string temp;
        getline(cin, temp);
        if (temp[0] == 'X' || temp[0] == 'x') return 0;
        char * err;
        i = strtol(temp.c_str(), &err, 0);
        if (*err) i = -1;
        if (i < 1 || i > 2) {
            cout << "Introduza uma opção válida" << endl;
            sair = true;
        }
    } while (sair);

    return i;
}

void Menu::showMenuTarefaDelegada(TarefaDelegada & td) {        
    int escolha;
    bool continuar;
    do {
        cout << "\033[2J\033[1;1H";
        API::mostraTarefa(td);
        cout << "Delegada para: "  << td.getNomeUtilizadorDestino() << endl;
        cout << "Publica: " << td.getPublicaChar()<<endl;
        cout << "Aceite: " << td.getAceiteChar()<<endl;
        
        continuar = false;

        cout << "\nEscolha uma das seguintes opções:" << endl
                << "1 - Eliminar Delegação" << endl
                << "2 - Alterar Delegação" << endl
                << "X - Voltar" << endl;
        string tmp;
        getline(cin, tmp);
        if (tmp[0] == 'X' || tmp[0] == 'x') return;
        char * err;
        escolha = strtol(tmp.c_str(), &err, 0);
        if (*err) escolha = -1;
        
        if (escolha < 1 || escolha > 2 ) {
            cout << "Introduza uma opção válida" << endl;
            continuar = true;
        }
    } while (continuar);    

    //Escolhas do Menu Tarefa Delegada
    switch (escolha) {
        case 1: //Eliminar referencia
        {
            API::getBDados()->elimina(td);
            CSVio file(API::getDropboxPath().append("partilhas.csv"));            
            if(!API::isRemoto())file.remove(td);
            
            cout << "Delegação eliminada." << endl;
            break;
        }

        case 2: //Eliminar delegação
        {
//            string path;
//            cout << "Indique o diretorio onde pretende exportar: ";
//            getline(cin, path);
//
//            if (path.empty())
//                path = "./";
//
//            CSVio::exportarReferencia(td, path, API::getBDados());
            list<Tarefa*> aDelegar;
            aDelegar.push_back(&td);
            API::leDelegacao(aDelegar);
            cout << DEFAULT_WAIT_MESSAGE;
            API::waitForLine();
            break;
        }

    }

}

void Menu::showMenuReferencia(Referencia & ref) {
    cout << "\033[2J\033[1;1H";
    API::mostraReferencia(ref);
    cout << DEFAULT_WAIT_MESSAGE << endl;
    API::waitForLine();
    int escolha;
    do {
    escolha = menuReferencia();

    //Escolhas do Menu Tarefa
    switch (escolha) {
        case 1: //Eliminar referencia
        {
            API::getBDados()->elimina(ref);
            cout << "Referencia eliminada." << endl;
            cout << DEFAULT_WAIT_MESSAGE << endl;
            API::waitForLine();
            escolha = 0;
            break;
        }

        case 2: //Exportar referencia
        {
            string path;
            cout << "Indique o diretorio onde pretende exportar: ";
            getline(cin, path);

            if (path.empty())
                path = "./";

            CSVio::exportarReferencia(ref, path, API::getBDados());

            cout << DEFAULT_WAIT_MESSAGE;
            API::waitForLine();
            break;
        }

    }
      } while (escolha != 0);

}

void Menu::showMenuContexto(Contexto & contexto) {
    cout << "\033[2J\033[1;1H";
    API::mostraContexto(contexto);
    cout << DEFAULT_WAIT_MESSAGE << endl;
    API::waitForLine();
    int escolha;
    do {
      
    escolha = menuContexto();

    //Escolhas do Menu Contexto
    switch (escolha) {
        case 1: //Editar contexto
            API::editaContexto(contexto);
            API::getBDados()->atualiza(contexto);
            break;

        case 2: //Eliminar contexto
        {
            if (API::getBDados()->desligarContexto(contexto))
                cout << "O contexto foi removido das tarefas." << endl;
            else
                cout << "Erro ao remover contexto das tarefas." << endl;

            if (API::getBDados()->elimina(contexto))
                cout << "Contexto eliminado." << endl;
            else
                cout << "Erro ao remover contexto." << endl;

            cout << DEFAULT_WAIT_MESSAGE << endl;
            escolha=0;
            API::waitForLine();
            break;
        }

    }
  } while (escolha != 0);
}

void Menu::showMenuProjeto(Projeto & projeto) {
    cout << "\033[2J\033[1;1H";
    API::mostraProjeto(projeto);
    cout << DEFAULT_WAIT_MESSAGE << endl;
    API::waitForLine();
    int escolha;
    do {
         escolha = menuProjeto();

    //Escolhas do Menu Projeto
    switch (escolha) {
        case 1:
        {
            cout << "\033[2J\033[1;1H";
            API::mostraTarefasDoProjeto(projeto);
            cout << DEFAULT_WAIT_MESSAGE << endl;
            API::waitForLine();
            break;
        }
        case 2:
        {
            API::editaProjeto(projeto);
            API::getBDados()->atualiza(projeto);
            break;
        }
        case 3:
        {
            API::getBDados()->elimina(projeto);
            escolha=0;
            break;

        }
        case 4:
        {
            list<Tarefa*> tarefas = API::getBDados()->listaTarefasDeUtilizador(API::getLocalUser());
            Tarefa * tarefa = API::listTarefas(tarefas);
            API::getBDados()->adicionarTarefaAProjeto(*tarefa, projeto);
            cout << "Tarefa \"" << tarefa->getNome() << "\" associada ao projeto." << endl;
            API::waitForLine();
            break;

        }
        case 5:
        {
            list<Tarefa> tarefas = API::getBDados()->listarTarefasDeProjeto(projeto);
            list<Tarefa*> apontadores;

            for (Tarefa t : tarefas)
                apontadores.push_back(new Tarefa(t));

            Tarefa * tarefa = API::listTarefas(apontadores);
            API::getBDados()->removerTarefaDeProjeto(*tarefa, projeto);
            cout << "Tarefa \"" << tarefa->getNome() << "\" removida do projeto." << endl;
            API::waitForLine();

            break;
        }
        case 6:
        {
            ListAdjGrafo<Tarefa, int> grafo = API::getBDados()->criaGrafodependenciaTarefas(projeto);
            grafo.escreve_grafo();
            string temp;
            getline(cin, temp);
            break;
        }

    }
   } while (escolha != 0);

}

void Menu::showMenuTarefa(Tarefa * tarefa) {
    cout << "\033[2J\033[1;1H";
    API::mostraTarefa(*tarefa);
    cout << DEFAULT_WAIT_MESSAGE << endl;
    API::waitForLine();
    int escolha;
    do {
        
    escolha = menuTarefa();

    //Escolhas do Menu Tarefa
    switch (escolha) {
        case 1:
        {
            //Editar Tarefa
            API::editaTarefa(*tarefa);
            API::getBDados()->atualiza(*tarefa);
            break;
        }
        case 2:
        {
            //Elimina Tarefa
            Utilizador u(API::getLocalUser());
            if (API::getBDados()->getTarefaDelegadaPara(u, *tarefa).getIdUtilizadorDestino() > 0) {
                cout << "INFORMAÇÃO: Não pode eliminar uma tarefa delegada para si." << endl;
                cout << DEFAULT_WAIT_MESSAGE << endl;
                API::waitForLine();
                break;
            }
            API::getBDados()->elimina(*tarefa);
            escolha=0;
            break;
        }
        case 3:
        {
            //Marca a tarefa como feita
            bool precedenciaNaoFeita;
            list<Tarefa> listT = API::getBDados()->listaTarefasDependentes(*tarefa);
             for (list<Tarefa>::iterator it = listT.begin(); it != listT.end(); it++)
                 if (it->getEstado() != "Feita")
                 {
                     cout<<"Tarefa dependente não feita: "<<*it<<endl;
                     precedenciaNaoFeita = true;
                 }
                     
            if (!precedenciaNaoFeita)
            {
            tarefa->setEstado("Feita");
            API::getBDados()->atualiza(*tarefa);
            cout<<"Tarefa marcada como feita"<<endl;
            }
            else
            {
                cout<<"O seu pedido não foi realizado dado que existem tarefas precedentes não feitas"<<endl;
            }
            break;

        }
        case 4:
        {
            //Difere a tarefa por x dias
            if (tarefa->getInicio().isNull()) {
                cout << "Não é possível diferir uma tarefa sem data de início definida" << endl;
            } else {
                cout << "Diferir por quantos dias?" << endl;
                string tempstr;
                getline(cin, tempstr);
                int dias = strtol(tempstr.c_str(), NULL, 0);

                tarefa->setInicio(tarefa->getInicio().addDays(dias));

                API::getBDados()->atualiza(*tarefa);
            }
            break;

        }
        case 5:
        {
            //Adiciona uma ligação de tarefa dependente
            Tarefa * tDep;
            tDep = API::listTarefas(API::getBDados()->listaTarefasDeUtilizador(API::getLocalUser()));
            API::getBDados()->adicionaDependenciaTarefa(*tarefa, *tDep);
            //Verifica se a dependencia inserida não causa ciclos
            ListAdjGrafo<Tarefa, int> grafo = API::getBDados()->criaGrafodependenciaTarefas(API::getLocalUser());

            if (grafo.existemCiclos()) {
                cout << "Dependência não adicionada - causa um ciclo no grafo" << endl;
                API::getBDados()->eliminaDependenciaTarefa(*tarefa, *tDep);
            } else {
                cout << "Dependências adicionada" << endl;
            }

            break;
        }
        case 6:
        {
            //Retira uma dependencia de tarefa
            Tarefa tDep;
            tDep = API::listTarefas(API::getBDados()->listaTarefasDependentes(*tarefa));
            if (tDep.getIdUtilizador() != -1) {
                API::getBDados()->eliminaDependenciaTarefa(*tarefa, tDep);
                cout<<"A dependência foi eliminada";
            }
            break;
        }
        case 7:
        {
            //Tranforma a tarefa em referencia
            Utilizador u(API::getLocalUser());
                if (API::getBDados()->getTarefaDelegadaPara(u, *tarefa).getIdUtilizadorDestino() > 0) {
                    cout << "INFORMAÇÃO: Não pode eliminar uma tarefa delegada para si." << endl;
                    cout << DEFAULT_WAIT_MESSAGE << endl;
                    API::waitForLine();
                    break;
                }
            Referencia r;
            API::getBDados()->transformarTarefaReferencia(*tarefa, r);
            escolha = 0;
            break;
        }
        case 8:
        {
            //Transforma a tarefa em projeto
            Utilizador u(API::getLocalUser());
                if (API::getBDados()->getTarefaDelegadaPara(u, *tarefa).getIdUtilizadorDestino() > 0) {
                    cout << "INFORMAÇÃO: Não pode eliminar uma tarefa delegada para si." << endl;
                    cout << DEFAULT_WAIT_MESSAGE << endl;
                    API::waitForLine();
                    break;
                }
            Projeto projeto;
            API::getBDados()->transformarTarefaProjeto(*tarefa, projeto);
            cout << "Pretende editar o projeto agora?(S/N) [N]:";
            if (API::simNao()) {
                showMenuProjeto(projeto);
            } else {
                string tempstr;
                Environment * env = Environment::createEnvironment(Environment::DEFAULT);
                cout << "Daqui a quantos dias pretende editar o Projeto?";

                getline(cin, tempstr);
                int dias = strtol(tempstr.c_str(), NULL, 0);
                Date d = Date::getSystemDate(env);
                d = d.addDays(dias);

                Tarefa tarefaParaEditarProjeto(API::getLocalUser().GetIdUtilizador());
                tarefaParaEditarProjeto.setNome("Editar o projeto " + projeto.getNome());

                tarefaParaEditarProjeto.setInicio(d);
                API::getBDados()->adiciona(tarefaParaEditarProjeto);
                API::getBDados()->le(tarefaParaEditarProjeto);
                cout << "Foi adicionada a seguinte tarefa" << endl;
                cout << tarefaParaEditarProjeto << endl;
                API::waitForLine();
            }
            escolha = 0;
            break;
        }
        case 9:
        {
            //Adicionar Anotação à tarefa
            API::adicionaAnotacao((AcaoBase) * tarefa);

            break;
        }
        case 10:
        {
            //Listar Anotações da tarefa
            list<Anotacao> listAno = API::getBDados()->listaAnotacoesDeAcaoBase((AcaoBase) * tarefa);
            Anotacao anotacao = API::listAnotacao(listAno);
           
            if (!anotacao.GetIdUtilizador() == 0)
                showMenuAnotacao(anotacao);
            break;
        }
    }
   } while (escolha != 0);
}

void Menu::showMenuAnotacao(Anotacao & anotacao) {
    int escolha;
    do {
        escolha = menuAnotacao();
        switch (escolha) {
            case 1:
            {
                //Eliminar Anotacao
                API::getBDados()->elimina(anotacao);
                escolha = 0;
                break;
            }
            case 2:
            {
                //Exportar Anotacao para outra aplicação (Sistema Operativo)
                anotacao.exportar();
                break;
            }
            case 0:
            {

                break;
            }
        }
    } while (escolha != 0);
}

void Menu::showMenu() {
    Tarefa * tarefa = NULL;

    int i = 1, j, escolha;
    do {
        escolha = menuPrincipal();
        switch (escolha) {
            case 0:
            {
                i = 0;
                break;
            }
            case 1: //Criar tarefa.
            {
                Tarefa tarefaCriar = API::leTarefa();
                API::getBDados()->adiciona(tarefaCriar);
                break;
            }
                break;
            case 2: //Listar tarefas.
            {
                list <Tarefa*> todasTarefas = API::getBDados()->listaTarefasDeUtilizador(API::getLocalUser());
                for (Tarefa * t : API::getBDados()->listaTarefasDelegadasPorUtilizador(API::getLocalUser()))
                    if (t->getPrioridade()!="qqdia") todasTarefas.push_back(t);
                tarefa = API::listTarefas(todasTarefas);
                
                if (tarefa ==NULL) break;
                
                Utilizador u(API::getLocalUser());
                TarefaDelegada td = API::getBDados()->getTarefaDelegadaPor(u, *tarefa);
                if (td.getIdUtilizador() > 0) {
                        showMenuTarefaDelegada(td);
                } else {
                        showMenuTarefa(tarefa);
                }
                break;
            }
            case 3:
            {
                list<Tarefa*> aDelegar;
                Tarefa* tarefa;

                do {
                    tarefa = API::listTarefas(API::getBDados()->listaTarefasDeUtilizador(API::getLocalUser()));
                    if (tarefa == NULL) break;
                    aDelegar.push_back(tarefa);
                    cout << "Deseja adicionar mais alguma tarefa(S|N): ";
                    if (!API::simNao())
                        break;
                } while (true);

                API::leDelegacao(aDelegar);

                break;
            }
            case 4:
            {
                list<Tarefa*> exportar;
                Tarefa* tarefa;
                string path;
                string extraPath = "EXPTAREFAS.csv";
                int escolha;
                do {
                    tarefa = API::listTarefas(API::getBDados()->listaTarefasDeUtilizador(API::getLocalUser()));
                    if (tarefa == NULL) break;
                    exportar.push_back(tarefa);
                    cout << "Deseja adicionar mais alguma tarefa(S|N): ";
                    if (!API::simNao())
                        break;
                } while (true);

                do {
                    cout << "\033[2J\033[1;1H";
                    cout << "Escolha uma opção: " << endl
                            << "\t1 - Exportar para a pasta partilhada" << endl
                            << "\t2 - Escolher caminho" << endl;
                    cin >> escolha;
                    string tmp;
                    getline(cin, tmp);
                    if (escolha > 0 && escolha < 3)
                        break;
                } while (true);

                if (escolha == 1)
                    path = API::getDropboxPath();
                else {
                    cout << "Indique o caminho: ";
                    getline(cin, path);
                }

                if (!path.empty()) {
                    path += extraPath;
                    cout << "\033[2J\033[1;1H";
                    if (CSVio::exportarListaTarefas(exportar, path))
                        cout << "Ficheiro criado com sucesso no caminho " << path << endl;
                    else
                        cout << "Erro ao criar o ficheiro no caminho: " << path << endl;
                } else
                    cout << "Erro ao criar o ficherio com o caminho vazio" << endl;

                sleep(1);

                break;
            }
            case 5: //Importar tarefas
            {
                string path;
                list<Tarefa*> tarefas;
                int escolha;
                string extraPath = "EXPTAREFAS.csv";
                do {
                    cout << "\033[2J\033[1;1H";
                    cout << "Escolha uma opção: " << endl
                            << "\t1 - Importar da pasta partilhada" << endl
                            << "\t2 - Escolher caminho" << endl;
                    cin >> escolha;
                    string tmp;
                    getline(cin, tmp);
                    if (escolha > 0 && escolha < 3)
                        break;
                } while (true);

                if (escolha == 1)
                    path = API::getDropboxPath();
                else {
                    cout << "Indique a localização do ficheiro: ";
                    getline(cin, path);
                }


                if (!path.empty()) {
                    path += extraPath;
                    cout << "\033[2J\033[1;1H";

                    if (!CSVio::importarListaTarefas(tarefas, path)) {
                        cout << "Erro: não foi possivel importar as tarefas" << endl;
                        sleep(1);
                    } else {
                        API::adicionaLista(tarefas, API::getLocalUser());
                        cout << "As seguintes tarefas foram adicionadas/atualizadas:" << endl;
                        for (Tarefa* t : tarefas)
                            cout << *t << endl;
                    }
                } else
                    cout << "Erro ao ler o ficherio com o caminho vazio" << endl;

                cout << endl << "Prima [ENTER] para avançar" << endl;
                API::waitForLine();
                break;
            }
            case 6: //Mostrar mapa de dependências da tarefa.
            {
                ListAdjGrafo<Tarefa, int> grafo = API::getBDados()->criaGrafodependenciaTarefas(API::getLocalUser());
                grafo.escreve_grafo();
                string temp;
                getline(cin, temp);
                break;
            }
            case 7: //Listar referencias
            {
                Referencia ref = API::listReferencias(API::getBDados()->listaReferenciasDeUtilizador(API::getLocalUser()));
                if (ref.GetIdUtilizador() == 0) break;
                showMenuReferencia(ref);
                break;
            }
            case 8: //listar projetos
            {
                Projeto projeto = API::listProjetos(API::getBDados()->listarProjetosDeUtilizador(API::getLocalUser()));
                showMenuProjeto(projeto);
                break;
            }
            case 9: //listar contextos
            {
                cout << "Pretende configurar um novo contexto? (S/N) [N]:";
                if (API::simNao()) {
                    Contexto c = API::leContexto();
                    API::getBDados()->adiciona(c);
                }

                Contexto contexto = API::listContextos(API::getBDados()->listaContextosDeUtilizador(API::getLocalUser()));
                if (!contexto.getIdUtilizador() == 0)
                    showMenuContexto(contexto);
                break;
            }
            case 10: //procurar tarefas delegadas
            {
                int count = 0;
                if(!API::isRemoto())
                {
                    string path = API::getDropboxPath();
                    string extraPath = "partilhas.csv";
                    path += extraPath;
                    CSVio importa(path);
                    vector<TarefaDelegada> tarefas = importa.getTarefasDelegadas(API::getLocalUser().GetNome());
                    for (vector<TarefaDelegada>::iterator it = tarefas.begin(); it != tarefas.end(); it++) {
                        cout << *it << endl;
                        cout << "Deseja aceitar a tarefa?(S|N): ";
                        bool aceite = API::simNao();
                        importa.remove(*it);
                        if (aceite) {
                            (*it).setAceite(aceite);
                            importa.insert(*it);
                            (*it).setIdUtilizadorDestino(API::getLocalUser().GetIdUtilizador());
                            (*it).setIdUtilizador(API::criarUtilizador(it->getNomeUtilizador()).GetIdUtilizador());
                            Tarefa tmp = *it;
                            if (!API::getBDados()->adiciona(tmp))
                                API::getBDados()->atualiza(tmp);
                            if (!API::getBDados()->adiciona(*it))
                                API::getBDados()->atualiza(*it);
                            count++;
                        }
                    }
                }else
                {
                    list<Tarefa*> tarefas = API::getBDados()->listaTarefasDelegadasAoUtilizador(API::getLocalUser(),'N');
                    for(Tarefa* t:tarefas)
                    {
                        if(typeid(*t) == typeid(TarefaDelegada))
                        {
                            TarefaDelegada* td = dynamic_cast<TarefaDelegada*>(t);
                            cout << *td << endl;
                            cout << "Deseja aceitar a tarefa?(S|N): ";
                            bool aceite = API::simNao();
                            if (aceite) {
                                td->setAceite(aceite);
                                td->setIdUtilizadorDestino(API::getLocalUser().GetIdUtilizador());
                                API::getBDados()->atualiza(*td);
                                count ++;
                            }
                        }
                    }
                }
                cout << count << " tarefas foram adicionadas à sua base de dados" << endl;
                cout << endl << "Prima [ENTER] para avançar" << endl;
                API::waitForLine();

                break;
            }
            case 11: //obter sugestões.
            {
                int numSugestoes, escolha;
                PriorityQueueTarefa::Comparacao ordem;
                list<Contexto> todosContextos = API::getBDados()->listaContextosDeUtilizador(API::getLocalUser());
                list<Contexto> contextosSelecionados;

                do {
                    cout << "\033[2J\033[1;1H";
                    cout << "Escolha os contextos que se aplicam:" << endl;
                    Contexto c = API::listContextos(todosContextos);
                    if (c.getIdUtilizador() == 0) break;
                    contextosSelecionados.push_back(c);
                    cout << "Deseja escolher mais contextos(S|N): ";
                    if (!API::simNao())
                        break;
                } while (true);

                if (contextosSelecionados.empty()) break;

                do {
                    cout << "\033[2J\033[1;1H";
                    cout << "Obter:" << endl;
                    cout << "1 - Uma Sugestão" << endl;
                    cout << "2 - Duas Sugestões" << endl;
                    cout << "3 - Três Sugestões" << endl;
                    cout << "4 - Quatro Sugestões" << endl;
                    cout << "5 - Cinco Sugestões" << endl;

                    cin >> numSugestoes;
                    string tmp;
                    getline(cin, tmp);

                    if (numSugestoes > 0 && numSugestoes < 6)
                        break;

                    cout << "Introduza uma opção válida!" << endl;
                    sleep(1);
                } while (true);

                do {
                    cout << "\033[2J\033[1;1H";
                    cout << "Ordenar por:" << endl
                            << "1 - Data Limite" << endl
                            << "2 - Prioridade" << endl
                            << "3 - Data de Criação" << endl;

                    cin >> escolha;
                    string tmp;
                    getline(cin, tmp);

                    if (escolha > 0 && escolha < 4)
                        break;

                    cout << "Introduza uma opção válida!" << endl;
                    sleep(1);
                } while (true);

                switch (escolha) {
                    case 1:
                        ordem = PriorityQueueTarefa::Comparacao::dataFim;
                        break;
                    case 2:
                        ordem = PriorityQueueTarefa::Comparacao::prioridade;
                        break;
                    default:
                        ordem = PriorityQueueTarefa::Comparacao::criada;
                }

                PriorityQueueTarefa sugestoes(ordem, API::getBDados()->listaTarefasPorContextos(API::getLocalUser(), contextosSelecionados), API::getBDados());

                cout << "\033[2J\033[1;1H";

                if (sugestoes.vazia())
                    cout << "Não tem tarefas associadas aos contextos escolhidos" << endl;

                int count = 0;
                while (!sugestoes.vazia() && count < numSugestoes) {
                    Tarefa* t;
                    sugestoes.retira(t);
                    cout << *t << endl;
                    count++;
                }

                cout << endl << "Prima [ENTER] para avançar" << endl;
                API::waitForLine();

                break;
            }
            case 12: //Listar tarefas com prioridade qqdia
               
            {
               list <Tarefa*> todasTarefas = API::getBDados()->listaTarefasDeUtilizadorQQDia(API::getLocalUser());
                for (Tarefa * t : API::getBDados()->listaTarefasDelegadasPorUtilizador(API::getLocalUser()))
                    if (t->getPrioridade()=="qqdia") todasTarefas.push_back(t);
                tarefa = API::listTarefas(todasTarefas);
                
                if (tarefa ==NULL) break;
                
                Utilizador u(API::getLocalUser());
                TarefaDelegada td = API::getBDados()->getTarefaDelegadaPor(u, *tarefa);
                if (td.getIdUtilizador() > 0) {
                        showMenuTarefaDelegada(td);
                } else {
                        showMenuTarefa(tarefa);
                }
                break;
            }
            case 13: //Mudar para modo local Remoto. Se o utilizador não tiver uma conta de acesso
                // poderá registrar-se por este item de menu
            {
                API::mudarRemotoLocal();
                break;
            }
            case 14:
            {
                ifstream ficheiro;
                ficheiro.open(PARAMS_PATH);
                string path;
                string newPath;

                cout << "\033[2J\033[1;1H";

                if (ficheiro.is_open()) {
                    getline(ficheiro, path);
                    ficheiro.close();
                    ofstream novoFicheiro(PARAMS_PATH);
                    if (path == "") {
                        cout << "Não existe nenhum caminho definido!" << endl;
                        cout << "Caminho: ";
                        getline(cin, path);
                    } else {
                        cout << "Caminho actual: " << path << endl;
                        cout << "Caminho(Deixe vazio para manter o mesmo): ";
                        getline(cin, newPath);
                        if (!(newPath == ""))
                            path = newPath;
                    }
                    novoFicheiro << path;
                    novoFicheiro.close();
                } else {
                    ofstream novoFicheiro(PARAMS_PATH);
                    cout << "Não existe nenhum caminho definido!" << endl;
                    cout << "Caminho: ";
                    getline(cin, path);
                    novoFicheiro << path;
                    novoFicheiro.close();
                }
            }
        }
    } while (i != 0);
}
