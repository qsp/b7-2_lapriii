/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              virtual void escreve(ostream &out) const                     */
/*              const Tarefa& operator = (const Tarefa &p)                   */                                                   
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/



#ifndef TAREFA_H
#define	TAREFA_H
#include <iostream>
#include <string>
#include <iomanip>
#include <occi.h>
#include "AcaoBase.h"

using namespace std;
using namespace oracle::occi;

class Tarefa : public AcaoBase {
    
    friend class BDados;
private:

    IntervalDS duracaoEfetiva;
    IntervalDS duracaoEstimada;

public:
    Tarefa();
    Tarefa(int idUtilizador);
    Tarefa(int idUtilizador,Timestamp criada);
    Tarefa(int idUtilizador, string nome, Timestamp criada,
            Timestamp modificada, Date fim, Date inicio,
            string prioridade, string estado, IntervalDS duracaoEfetiva,
            IntervalDS duracaoEstimada);

    Tarefa(const Tarefa & t);
    
    void setDuracaoEstimada(IntervalDS duracaoEstimada);
    void setIdUtilizador(int idUtilizador);

    IntervalDS getDuracaoEstimada() const;

    void setDuracaoEfetiva(IntervalDS duracaoEfetiva);

    IntervalDS getDuracaoEfetiva() const;
    virtual void escreve(ostream &out) const;
    const Tarefa& operator = (const Tarefa &p);    
    
};

ostream& operator <<(ostream &out, const Tarefa &p);



#endif	/* TAREFA_H */

