/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              virtual void escreve(ostream &out) const                     */ 
/*              const TarefaDelegada& operator = (const TarefaDelegada &p)   */                                                   
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/


#ifndef TAREFADELEGADA_H
#define	TAREFADELEGADA_H
#include <iostream>
#include <string>
#include <iomanip>
#include <occi.h>
#include "Tarefa.h"
#include "Utilizador.h"

#define ACEITE_TRUE  'S'
#define ACEITE_FALSE 'N'
#define PUBLICA_TRUE 'S'
#define PUBLICA_FALSE 'N'

class TarefaDelegada : public Tarefa {
public:
    TarefaDelegada();
    TarefaDelegada(Tarefa &tarefa, int idUtilizadorDestino, char publica, char aceite = NULL);
    TarefaDelegada(Tarefa &tarefa, Utilizador origem, Utilizador destino, char publica, char aceite = NULL);
    TarefaDelegada(const TarefaDelegada & td);
    
    void setNomeUtilizador(string nomeUtilizador);
    string getNomeUtilizador() const;
    
    void setNomeUtilizadorDestino(string nomeUtilizadorDestino);
    string getNomeUtilizadorDestino() const;
    
    void setAceite(bool aceite);    
    bool isAceite() const;
    
    void setVisualizada(bool visualizada);    
    bool isVisualizada() const;    
    
    void setAceiteChar(char aceite);
    char getAceiteChar() const;
    
    void setPublicaChar(char publica);
    char getPublicaChar() const;
    
    void setIdUtilizadorDestino(int idUtilizadorDestino);
    int getIdUtilizadorDestino() const;    
    
    virtual void escreve(ostream &out) const;
    const TarefaDelegada& operator = (const TarefaDelegada &p);
    
    ~TarefaDelegada();
private:
    
    string nomeUtilizador;
    int idUtilizadorDestino;
    string nomeUtilizadorDestino;
    bool aceite;
    bool visualizada;
    bool publica;

};

ostream& operator <<(ostream &out, const TarefaDelegada &p);

#endif	/* TAREFADELEGADA_H */

