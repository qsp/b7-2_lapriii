/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              void escreve(ostream &out)const                              */
/*              void dadosToFile(string fileName) const                      */
/*              const Anotacao& operator =(const Anotacao &p)                */
/*              void exportar()                                              */
/*              void openFileFromBlob(                                       */
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/



#include <fstream>
#include <iomanip>
#include "Anotacao.h"

Anotacao::Anotacao() {
};

Anotacao::Anotacao(int idUtilizador) {
    this->idUtilizador = idUtilizador;
}

Anotacao::Anotacao(int idUtilizador, int idAnotacao, string nome, string tipo, Blob dados) {
    this->idUtilizador = idUtilizador;
    this->idAnotacao = idAnotacao;

    this->nome = nome;
    this->tipo = tipo;
    this->dados = dados;
}

void Anotacao::SetDados(Blob dados) {
    this->dados = dados;
}

Blob Anotacao::GetDados() const {
    return dados;
}

void Anotacao::SetTipo(string tipo) {
    this->tipo = tipo;
}

string Anotacao::GetTipo() const {
    return tipo;
}

void Anotacao::SetNome(string nome) {
    this->nome = nome;
}

string Anotacao::GetNome() const {
    return nome;
}

void Anotacao::SetIdAnotacao(int idAnotacao) {
    this->idAnotacao = idAnotacao;
}

int Anotacao::GetIdAnotacao() const {
    return idAnotacao;
}

void Anotacao::SetIdUtilizador(int idUtilizador) {
    this->idUtilizador = idUtilizador;
}

int Anotacao::GetIdUtilizador() const {
    return idUtilizador;
}

void Anotacao::escreve(ostream& out) const {
    out << setw(30) << nome << " | " << setw(12)
            << tipo << " | ";
    //    if (tipo == "txt" || tipo == "email" || tipo == "link") {
    //        Blob b(dados);
    //        b.open(OCCI_LOB_READONLY);
    //        if (b.length() > 0) {
    //            unsigned int size = b.length();
    //            Stream *instream = b.getStream(1, size);
    //            char *buffer = new char[size];
    //            instream->readBuffer(buffer, b.length());
    //            out << buffer << "|";
    //            delete buffer;
    //            b.closeStream(instream);
    //        }
    //        b.close();
    //    }
}

void Anotacao::dadosToFile(string fileName) const {
    Blob b(dados);
    b.open(OCCI_LOB_READONLY);
    if (b.length() > 0) {
        unsigned int size = b.length();
        Stream *instream = b.getStream(1, size);
        char *buffer = new char[size];
        instream->readBuffer(buffer, b.length());
       
        ofstream file;
        file.open(fileName, ios::out | ios::binary);
        file.write(buffer, size);
        file.close();
        delete buffer;
        b.closeStream(instream);
    }
    b.close();
}

void Anotacao::exportar() {
    if (tipo == "file") {
        openFileFromBlob();
        return;
    }
    if (tipo == "txt") {
        string com = "xdg-open ";
        com.append(GetNome());
        system(com.data());
    }


}

const Anotacao& Anotacao::operator =(const Anotacao &p) {
    this->idAnotacao = p.idAnotacao;
    this->idUtilizador = p.idUtilizador;
    this->nome = p.nome;
    this->tipo = p.tipo;
    this->dados = p.dados;
    return *this;
}

ostream& operator <<(ostream &out, const Anotacao &p) {
    p.escreve(out);
    return out;
}

void Anotacao::openFileFromBlob() {
    dadosToFile(GetNome());
    string com = "xdg-open ";
    com.append(GetNome());
    system(com.data());
}