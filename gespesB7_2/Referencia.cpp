/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              void escreve(ostream &out) const                             */                                                   
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/



#include "Referencia.h"

Referencia::Referencia(){
}

Referencia::Referencia(int idReferencia, int idUtilizador, string nome){
    this->idReferencia = idReferencia;
    this->idUtilizador = idUtilizador;
    this->nome=nome;
}

void Referencia::SetNome(string nome) {
    this->nome = nome;
}

string Referencia::GetNome() const {
    return nome;
}

void Referencia::SetIdUtilizador(int idUtilizador) {
    this->idUtilizador = idUtilizador;
}

int Referencia::GetIdUtilizador() const {
    return idUtilizador;
}

void Referencia::SetIdReferencia(int idReferencia) {
    this->idReferencia = idReferencia;
}

int Referencia::GetIdReferencia() const {
    return idReferencia;
}

void Referencia::escreve(ostream &out) const {
    out<<"| "<<nome<<" |";
}

ostream& operator <<(ostream &out, const Referencia &p) {
    p.escreve(out);
    return out;
}