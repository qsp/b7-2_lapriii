/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              static Date stringToDate(string date)                        */
/*              static string intervalDSToString(IntervalDS intervalo)       */
/*              static IntervalDS readIntervalDS()                           */
/*              static bool simNao()                                         */
/*              static void waitForLine()                                    */
/*              static Utilizador criarUtilizador(const string nome)         */
/*              static void editaAcaoBase(AcaoBase & acaobase)               */
/*              static void mostraAcaoBase(AcaoBase & acaobase)              */
/*              static Contexto leContexto()                                 */
/*              static void mostraContexto(Contexto & contexto)              */
/*              static Contexto listContextos(list<Contexto> contextos)      */
/*              static void editaContexto(Contexto & contexto)               */
/*              static Referencia listReferencias(list<Referencia>)          */
/*              static void mostraReferencia(Referencia & ref)               */
/*              static Tarefa * listTarefas(list<Tarefa *>)                  */
/*              static Anotacao listAnotacao(list<Anotacao> listAno)         */
/*              static Tarefa leTarefa()                                     */
/*              static void mostraTarefa(Tarefa & tarefa)                    */
/*              static void editaTarefa(Tarefa & tarefa)                     */
/*              static Projeto listProjetos(list<Projeto>)                   */
/*              static void mostraProjeto(Projeto & projeto)                 */
/*              static void editaProjeto(Projeto & projeto)                  */
/*              static void mostraTarefasDoProjeto(Projeto & projeto)        */
/*              static void loadPath()                                       */
/*              static bool verificaDadosUtilizador(Utilizador & u)          */
/*              static Utilizador leUtilizador()                             */
/*              static Utilizador listaUtilizadores(                         */
/*                      list<Utilizador> utilizadores)                       */
/*              static Utilizador adicionaUtilizador()                       */
/*              static void adicionaLista(list<Tarefa*> tarefas,             */
/*                      const Utilizador& uti)                               */
/*              static void adicionaAnotacao(AcaoBase acaoBase)              */
/*              static bool delegarTarefa(Tarefa& t,int idUtilizador,        */
/*                      bool publica)                                        */
/*              static void setRemoto(bool remoto)                           */
/*              static bool isRemoto()                                       */
/*              static void mudarRemotoLocal()                               */
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/


#include "API.h"
#include "BDDados.h"
#include "CSVio.h"
#include <sstream>
#include <limits>
#include <fstream>
#include <typeinfo>
#include <unistd.h>


string API::dateFormat = SLASHDDMMYYYY;
string API::intervalDSFormat = INTERVALDS_SEM_DIAS;


Utilizador API::localUser;
string API::dropboxPath;
bool API::remoto = false;

string API::utilizador = "B7-2";
string API::palavra = "pass unica deste grupo";
string API::bd = "193.136.62.27:1521/isepdb";

BDados * API::bDados = new BDados(utilizador, palavra, bd, "");

API::API() {
}

API::API(const API& orig) {
}

API::~API() {
}

string API::getDateFormat() {
    return API::dateFormat;
}

void API::setDateFormat(string format) {
    API::dateFormat = format;
}

Date API::stringToDate(string date) {
    Date d(Environment::createEnvironment(Environment::DEFAULT));
    d.fromText(date, API::dateFormat);
    return d;
}

void API::setRemoto(bool rem) {
    API::remoto = rem;
}

bool API::isRemoto() {
    return API::remoto;
}

void API::mudarRemotoLocal() {
    if (API::isRemoto()) {
        setLocalUser(bDados->listaUtilizadores().front());
        delete bDados;
        bDados = new BDados(utilizador, palavra, bd, "");
        cout << "Mudou para modo local";
        setRemoto(false);
    } else {

        BDados *tempBDados = new BDados(utilizador, palavra, bd, "REMOTO_");


        cout << "Tem uma conta registada no servidor? (S/N) [N]:";
        if (!API::simNao()) {
            cout<<"Novo registo"<<endl;
            Utilizador u;
            bool sair;
            do {
                sair = true;
                u = leUtilizador();
                if (tempBDados->existeUtilizadorPorNome(u)) {
                    cout << "Já existe um utilizador com esse nome por favor escolha outro"<<endl;
                    sair = false;
                }
            } while (!sair);

            tempBDados->adiciona(u);
            setLocalUser(u);
            cout << "Utilizador registado com sucesso - Encontra-se em modo remoto";
            delete bDados;
            bDados = tempBDados;
            setRemoto(true);

        } else {
            Utilizador u = leUtilizador();
            if (tempBDados->existeUtilizadorPorNomePassword(u)) {
                setLocalUser(u);
                cout << "Utilizador valido - login efetuado";
                delete bDados;
                bDados = tempBDados;
                setRemoto(true);
            } else {
                cout << "Utilizador não valido - login recusado";
                delete tempBDados;
            }
        }
    }
    waitForLine();
}

Projeto API::listProjetos(list<Projeto> projetos) {
    int nLinha = 0;
    int i = 0;
    int escolha;
    bool sair;
    do {
        cout << "\033[2J\033[1;1H";
        sair = false;
        nLinha = 0;
        cout << "----------------ESCOLHA UM PROJETO-------------------" << endl;
        cout << " " << setw(30) << "NOME" << " | " << setw(12)
                << "DATA INICIO" << " | " << setw(12)
                << "DATA FIM" << " | " << setw(12)
                << "PRIORIDADE" << " | " << setw(12)
                << "ESTADO" << " | " << endl;

        for (list<Projeto>::iterator it = projetos.begin(); it != projetos.end(); it++)
            cout << ++nLinha << *it << endl;
        cin >> escolha;
        string tmp;
        getline(cin, tmp);
        if (escolha < 1 || escolha > nLinha) {
            cout << "Introduza uma opção válida" << endl;
            sair = true;
        }
    } while (sair);
    list<Projeto>::iterator it = projetos.begin();


    for (list<Projeto>::iterator it = projetos.begin(); it != projetos.end(); it++) {
        if (++i == escolha) return *it;
    }

}

Utilizador API::criarUtilizador(const string nome) {
    int idUtilizador = bDados->nomeUtilizadorToId(nome);

    Utilizador u(nome);
    //se o utilizador não existe cria.
    if (idUtilizador == 0) {
        u.SetPassword("<gerado automaticamente>");
        bDados->adiciona(u);
    } else {
        u.SetIdUtilizador(idUtilizador);
    }
    //retorna o utilizador que corresponde ao nome indicado.
    bDados->le(u);
    return Utilizador(u);
}

Referencia API::listReferencias(list<Referencia> refs) {
    int nLinha = 0;
    int i = 0;
    int escolha;
    bool sair;
    do {
        cout << "\033[2J\033[1;1H";
        sair = false;
        nLinha = 0;
        cout << "----------------ESCOLHA UMA REFERENCIA --------------" << endl;
        cout << " " << setw(30) << "NOME" << endl;

        for (list<Referencia>::iterator it = refs.begin(); it != refs.end(); it++)
            cout << ++nLinha << *it << endl;
        
        cout << "X Voltar" << endl;
        string temp;
        getline(cin, temp);
        if (temp[0] == 'X' || temp[0] == 'x') return Referencia(0,0,"");
        char * err;
        escolha = strtol(temp.c_str(), &err, 0);
        if (*err) escolha = -1;
        if (escolha < 1 || escolha > nLinha) {
            cout << "Introduza uma opção válida" << endl;
            sair = true;
        }
    } while (sair);
    list<Referencia>::iterator it = refs.begin();


    for (list<Referencia>::iterator it = refs.begin(); it != refs.end(); it++) {
        if (++i == escolha) return *it;
    }
return Referencia(0,0,"");
}

void API::mostraTarefasDoProjeto(Projeto & projeto) {
    int nLinha = 0;
    list<Tarefa> tarefas = bDados->listarTarefasDeProjeto(projeto);

    cout << " " << setw(30) << "NOME" << " | " << setw(12)
            << "DATA INICIO" << " | " << setw(12)
            << "DATA FIM" << " | " << setw(12)
            << "PRIORIDADE" << " | " << setw(12)
            << "ESTADO" << " | " << endl;
    
    if (tarefas.size() == 0) {
        cout << " (nenhuma) " << endl;
    } else {
        for (list<Tarefa>::iterator it = tarefas.begin(); it != tarefas.end(); it++)
            cout << ++nLinha << *it << endl;
    }

}

Tarefa * API::listTarefas(list<Tarefa *> listA) {
    
    int i = 0;
    int escolha;
    bool sair;
    do {
        cout << "\033[2J\033[1;1H";
        int nLinha = 0;
        sair = false;
        nLinha = 0;
        cout << "----------------ESCOLHA UMA TAREFA-------------------" << endl;
        cout << "  " << setw(30) << "NOME" << " | " << setw(12)
                << "DATA INICIO" << " | " << setw(12)
                << "DATA FIM" << " | " << setw(12)
                << "PRIORIDADE" << " | " << setw(12)
                << "ESTADO" << " | * |" << endl;

        Utilizador u(API::getLocalUser());
        for (list<Tarefa*>::iterator it = listA.begin(); it != listA.end(); it++) {
            cout << setw(2) << ++nLinha << **it;
            if ( typeid(TarefaDelegada) == typeid(**it)) {
                if ( API::getBDados()->getTarefaDelegadaPara(u, **it).getIdUtilizadorDestino() > 0){
                    cout << "< |";
                }else if(API::getBDados()->getTarefaDelegadaPor(u, **it).getIdUtilizador() > 0) {
                    cout << "> |";
                }
            } else {
                cout << "  |";
            }
            cout << endl;
        }
        cout << "< Tarefa delegada para si;"<<endl
             << "> Tarefa que delegou para outra pessoa"<<endl
             <<" X - Sair"<<endl;

        string temp;
        getline(cin, temp);
        if (temp[0] == 'X' || temp[0] == 'x') return NULL;
        char * err;
        escolha = strtol(temp.c_str(), &err, 0);
        if (*err) escolha = -1;
        
        if (escolha < 1 || escolha > nLinha) {
            cout << "Introduza uma opção válida" << endl;
            sair = true;
        }

    } while (sair);

    if (escolha != 0) {
        for (list<Tarefa*>::iterator it = listA.begin(); it != listA.end(); it++) {
            if (++i == escolha) return *it;
        }
    }
    
    return NULL;
}

Tarefa API::listTarefas(list<Tarefa> listA) {
    
    int i = 0;
    int escolha;
    bool sair;
    do {
        cout << "\033[2J\033[1;1H";
        int nLinha = 0;
        sair = false;
        nLinha = 0;
        cout << "----------------ESCOLHA UMA TAREFA-------------------" << endl;
        cout << " " << setw(30) << "NOME" << " | " << setw(12)
                << "DATA INICIO" << " | " << setw(12)
                << "DATA FIM" << " | " << setw(12)
                << "PRIORIDADE" << " | " << setw(12)
                << "ESTADO" << " | " << endl;

        for (list<Tarefa>::iterator it = listA.begin(); it != listA.end(); it++)
            cout << ++nLinha << *it << endl;
        cout << "X - Sair";

        string temp;
        getline(cin, temp);
        if (temp[0] == 'X' || temp[0] == 'x') return Tarefa(-1);
        char * err;
        escolha = strtol(temp.c_str(), &err, 0);
        if (*err) escolha = -1;
        
        if (escolha < 1 || escolha > nLinha) {
            cout << "Introduza uma opção válida" << endl;
            sair = true;
        }

    } while (sair);

    if (escolha != 0) {
        for (list<Tarefa>::iterator it = listA.begin(); it != listA.end(); it++) {
            if (++i == escolha) return *it;
        }
    }
    
    return Tarefa(-1);
}

Anotacao API::listAnotacao(list<Anotacao> listAno) {
    int nLinha = 0;
    int i = 0;
    int escolha;
    bool sair;
    do {
        cout << "\033[2J\033[1;1H";
        sair = false;
        nLinha = 0;
        cout << "----------------ESCOLHA UMA ANOTACAO-------------------" << endl;
        cout << " " << setw(30) << "NOME" << " | " << setw(12)
                << "TIPO" << " | " << endl;
        
        
        for (list<Anotacao>::iterator it = listAno.begin(); it != listAno.end(); it++)
            cout << ++nLinha << *it << endl;
        cout << "X - Sair";
        string temp;
        getline(cin, temp);
        if (temp[0] == 'X' || temp[0] == 'x') return Anotacao(-1);
        char * err;
        escolha = strtol(temp.c_str(), &err, 0);
        if (*err) escolha = -1;
        
        if (escolha < 1 || escolha > nLinha) {
            cout << "Introduza uma opção válida" << endl;
            sair = true;
        }
        
    } while (sair);


    for (list<Anotacao>::iterator it = listAno.begin(); it != listAno.end(); it++) {
        if (++i == escolha) return *it;
    }

}

void API::adicionaAnotacao(AcaoBase acaoBase) {
    string tempstr;
    vector<string> tipoAnotacao = bDados->listaTipoAnotacao();
    int iTipo;
    bool sair;
    do {
        cout << "Introduza o tipo de anotacao (";

        for (int i = 1; i < tipoAnotacao.size() + 1; ++i)
            cout << "\t  [" << i << "]" << tipoAnotacao[i - 1];
        cout << "\t): ";
        getline(cin, tempstr);
        iTipo = strtol(tempstr.c_str(), NULL, 0);

        if (iTipo > 0 && iTipo < tipoAnotacao.size()+1) {
            sair = true;

        } else {
            cout << "Opcao invalida" << endl;
            sair = false;
        }
    } while (!sair);

    if (iTipo == 1) {
        cout << "Introduza o texto da anotacao: ";
        getline(cin, tempstr);
        bDados->adicionaAnotacaoPorTexto(acaoBase, tempstr, "txt");

    }

    if (iTipo == 2) {
        cout << "Introduza o caminho para o ficheiro: ";
        getline(cin, tempstr);
        bDados->adicionaAnotacaoPorFicheiro(acaoBase, tempstr, "file");
    }



}

Contexto API::listContextos(list<Contexto> contextos) {
    int i = 0;
    int escolha;
    bool sair;
    do {
        int nLinha = 0;
        cout << "\033[2J\033[1;1H";
        sair = false;
        string input;
        nLinha = 0;
        cout << "----------------ESCOLHA UM CONTEXTO------------------" << endl;
        cout << " " << setw(30) << "NUMERO" << " | " << setw(12)
                << "DESCRIÇÃO" << endl;
   
        for (list<Contexto>::iterator it = contextos.begin(); it != contextos.end(); it++) {
            Contexto c = *it;
            cout << " " << setw(30) << ++nLinha << " | " << setw(12)
                    << c.getNome() << endl;
        }
         cout << "X Sair" << endl;
         
        
        string temp;
        getline(cin, temp);
        if (temp[0] == 'X' || temp[0] == 'x') return Contexto(0,"");
        char * err;
        escolha = strtol(temp.c_str(), &err, 0);
        if (*err) escolha = -1;
        
        if (escolha < 1 || escolha > nLinha) {
            cout << "Introduza uma opção válida" << endl;
            sair = true;
        }
    } while (sair);

    for (list<Contexto>::iterator it = contextos.begin(); it != contextos.end(); it++) {
        if (++i == escolha) return *it;
    }

}

void API::setBDados(BDados* bD) {
    API::bDados = bD;
}

BDados * API::getBDados() {
    return API::bDados;
}

void API::setLocalUser(Utilizador & user) {
    API::localUser = user;
}

Utilizador API::listaUtilizadores(list<Utilizador> utilizadores) {
    
    int i = 0;
    int escolha;
    bool sair;
    do {
        int nLinha= 0;
        cout << "\033[2J\033[1;1H";
        sair = true;

        cout << "----------------ESCOLHER UM UTILIZADOR EXISTENTE------------------" << endl;
        cout << " " << setw(12) << "ID" << " | " << setw(30)
                << "NOME" << " | " << setw(12)
                << "CODIGO" << " | " << endl;

        for (Utilizador u : utilizadores)
            cout << ++nLinha << u;

        cout << "0 ADICIONAR NOVO UTILIZADOR" << endl;
        cout << "X CANCELAR" << endl;

        string tmp;
        getline(cin, tmp);
        if (tmp[0] == 'X' || tmp[0] == 'x') return Utilizador(0);
        char * err;
        escolha = strtol(tmp.c_str(), &err, 0);
        if (*err) escolha = -1;
        
        if (escolha < 0 || escolha > nLinha) {
            cout << "Introduza uma opção válida" << endl;
            sair = false;
        }
    } while (!sair);

    if (escolha == 0)
        return API::adicionaUtilizador();

    for (list<Utilizador>::iterator it = utilizadores.begin(); it != utilizadores.end(); it++) {
        if (++i == escolha) return *it;
    }

    //nunca deveria chegar aqui.
    return Utilizador(0);
}

Utilizador API::adicionaUtilizador() {
    string nome;
    cout << "\033[2J\033[1;1H";
    cout << "Introduza os Dados do Utilizador:" << endl;
    cout << "Nome: ";
    getline(cin, nome);
    Utilizador u(nome);
    bDados->adiciona(u);
    return u;
}

Utilizador API::getLocalUser() {
    return API::localUser;
}

void API::loadPath() {
    ifstream ficheiro;
    string path;
    ficheiro.open(PARAMS_PATH);
    if (ficheiro.is_open()) {
        getline(ficheiro, path);
        ficheiro.close();
        setDropboxPath(path);
    } else
        cout << "Erro ao abrir o ficheiro de parametros!" << endl;

}

void API::setDropboxPath(string path) {
    dropboxPath = path;
}

string API::getDropboxPath() {
    return dropboxPath;
}

Contexto API::leContexto() {

    string nome;

    cout << "Escreva o nome do contexto: ";
    getline(cin, nome);
    Contexto c(localUser.GetIdUtilizador(), nome);

    return Contexto(c);
}

Tarefa API::leTarefa() {

    string nome;

    cout << "Escreva o texto da tarefa: ";
    getline(cin, nome);
    Tarefa t(localUser.GetIdUtilizador());
    t.setNome(nome);
    t.setDuracaoEstimada(API::getDuracaoEstimada(t));

    return Tarefa(t);
}

void API::editaContexto(Contexto & contexto) {
    //Environment * env = Environment::createEnvironment(Environment::DEFAULT);

    // NOME -------------------------------------------------------------------

    cout << "Nome: \"" << contexto.getNome() << "\". Alterar? (S/N) [N]:";
    if (API::simNao()) {
        string tempstr;
        cout << "Introduza o novo nome:";
        getline(cin, tempstr);
        contexto.setNome(tempstr);
    }

}

void API::editaAcaoBase(AcaoBase & acaobase) {

    Environment * env = Environment::createEnvironment(Environment::DEFAULT);

    // NOME -------------------------------------------------------------------

    cout << "Nome: \"" << acaobase.getNome() << "\". Alterar? (S/N) [N]:";
    if (API::simNao()) {
        string tempstr;
        cout << "Introduza o novo nome:";
        getline(cin, tempstr);
        acaobase.setNome(tempstr);
    }

    // DATA_INICIO ------------------------------------------------------------
    cout << "Data de inicio: \""
            << (acaobase.getInicio().isNull() ? " --- " : acaobase.getInicio().toText(API::dateFormat))
            << "\". Alterar? (S/N) [N]:";

    if (API::simNao()) {
        string tempstr;
        Date temp(env);
        cout << "Introduza a nova data (" << API::dateFormat << "): ";
        getline(cin, tempstr);

        if (!temp.isNull()) {
            temp = API::stringToDate(tempstr);
            acaobase.setInicio(temp);
        } else {
            cout << "Formato ou data invalida, foi mantida a anterior." << endl;
        }
    }

    // DATA_FIM ---------------------------------------------------------------
    cout << "Data de conclusão: \""
            << (acaobase.getFim().isNull() ? " --- " : acaobase.getFim().toText(API::dateFormat))
            << "\". Alterar? (S/N) [N]:";

    if (API::simNao()) {
        string tempstr;
        Date temp(env);
        cout << "Introduza a nova data (" << API::dateFormat << "): ";
        getline(cin, tempstr);

        if (!temp.isNull()) {
            temp = API::stringToDate(tempstr);
            acaobase.setFim(temp);
        } else {
            cout << "Formato ou data invalida, foi mantida a anterior." << endl;
        }
    }

    // PRIORIDADE ------------------------------------------------------------
    cout << "Prioridade: \""
            << acaobase.getPrioridade()
            << "\". Alterar? (S/N) [N]:";

    if (API::simNao()) {
        string tempstr;
        Date temp(env);
        vector<string> prioridades = bDados->listaPrioridades();
        int prioridade;

        cout << "Introduza o numero da nova prioridade (";

        for (int i = 1; i < prioridades.size(); ++i)
            cout << "\t  [" << i << "]" << prioridades[i];
        cout << "\t): ";
        getline(cin, tempstr);
        prioridade = strtol(tempstr.c_str(), NULL, 0);

        if (prioridade > 0 && prioridade < prioridades.size()) {
            acaobase.setPrioridade(prioridades[prioridade]);
        } else {
            cout << "Opcao invalida, foi mantida a anterior." << endl;
        }
    }

    // ESTADO -----------------------------------------------------------------
    cout << "Estado: \""
            << acaobase.getEstado()
            << "\". Alterar? (S/N) [N]:";

    if (API::simNao()) {
        string tempstr;
        Date temp(env);
        vector<string> estados = bDados->listaEstados();
        int estado;

        cout << "Introduza o numero do novo estado (";

        for (int i = 1; i < estados.size(); ++i)
            cout << "\t  [" << i << "]" << estados[i];
        cout << "\t): ";
        getline(cin, tempstr);
        estado = strtol(tempstr.c_str(), NULL, 0);

        if (estado > 0 && estado < estados.size()) {
            acaobase.setEstado(estados[estado]);
        } else {
            cout << "Opcao invalida, foi mantida a anterior." << endl;
        }
    }
}

IntervalDS API::getDuracaoEstimada(Tarefa & t) {
    stringstream nome(t.getNome());
    string palavra, tmp;
    long duracaoTotal = 0;
    int numMedias = 0;

    while (nome >> palavra) {
        if (palavra.length() > 3) {
            long media = bDados->mediaDuracaoPorNomeParcial(palavra);
            if (media > 0) {
                duracaoTotal += media;
                numMedias++;
            }
        }
    }

    IntervalDS retorno(Environment::createEnvironment(Environment::DEFAULT));

    if (numMedias == 0) {
        retorno.setNull();
        return retorno;
    }

    long duracaoMedia = numMedias > 0 ? (duracaoTotal / numMedias) : 0;

    int dias, horas, minutos;
    dias = duracaoMedia / 86400;
    duracaoMedia -= dias * 86400;
    horas = duracaoMedia / 3600;
    duracaoMedia -= horas * 3600;
    minutos = duracaoMedia / 60;
    retorno.set(dias, horas, minutos, 0, 0);

    return IntervalDS(retorno);
}

void API::editaTarefa(Tarefa & tarefa) {

    Environment * env = Environment::createEnvironment(Environment::DEFAULT);
    API::editaAcaoBase(tarefa);

    // DURACAO_ESTIMADA -------------------------------------------------------
    cout << "Duracao estimada: \""
            << API::intervalDSToString(tarefa.getDuracaoEstimada())
            << "\". Alterar? (S/N) [N]:";

    if (API::simNao()) {
        string tempstr;
        IntervalDS temp(env);
        vector<string> estados = bDados->listaEstados();
        int estado;

        IntervalDS sugestao = API::getDuracaoEstimada(tarefa);
        if (!sugestao.isNull()) {
            cout << "Utilizar a sugestão do sistema \""
                    << API::intervalDSToString(sugestao)
                    << "\" (S/N) [N]:";
            if (API::simNao()) {
                tarefa.setDuracaoEstimada(sugestao);
                return;
            }
        } else {
            cout << "Informação: Não foi possível sugerir uma estimativa "
                    "de duração porque ainda não existem tarefas similares "
                    "cuja duração efetiva tenha sido indicada." << endl;
        }

        cout << "Introduza a nova duração estimada:" << endl;
        temp = API::readIntervalDS();

        if (!temp.isNull()) {
            tarefa.setDuracaoEstimada(temp);
        } else {
            cout << "Intervalo invalido, foi mantido o anterior." << endl;
        }
    }

    // DURACAO_EFETIVA -------------------------------------------------------
    cout << "Duracao efetiva: \""
            << API::intervalDSToString(tarefa.getDuracaoEfetiva())
            << "\". Alterar? (S/N) [N]:";

    if (API::simNao()) {
        string tempstr;
        IntervalDS temp(env);
        vector<string> estados = bDados->listaEstados();
        int estado;

        cout << "Introduza a nova duração efetiva:" << endl;
        temp = API::readIntervalDS();

        if (!temp.isNull()) {
            tarefa.setDuracaoEfetiva(temp);
        } else {
            cout << "Intervalo invalido, foi mantido o anterior." << endl;
        }
    }
    
    // CONTEXTOS -------------------------------------------------------------
    cout << "Contextos da tarefa: \"";
    
    for ( Contexto c : bDados->listaContextosDeTarefa(tarefa)) {
        cout << "[" << c.getNome() << "]";
    }
    
    cout << "\". Alterar? (S/N) [N]:";

    if (API::simNao()) {
        Tarefa * ptrTarefa = &tarefa;
        list<Tarefa*> listaTarefas;
        listaTarefas.push_back(ptrTarefa);
        bDados->limparContextosDaTarefa(listaTarefas);
        Contexto c = API::listContextos(bDados->listaContextosDeUtilizador(localUser));
        if (c.getIdUtilizador()>0){
                bDados->adiciona(tarefa, c);
                cout << "Contexto \""<< c.getNome() 
                        << "\" associado á tarefa." << endl
                        << "Prima enter para selecionar mais um contexto." << endl;
                waitForLine();
                c = API::listContextos(bDados->listaContextosDeUtilizador(localUser));
                if (c.getIdUtilizador()>0){
                bDados->adiciona(tarefa, c);
                cout << "Contexto \""<< c.getNome() 
                        << "\" associado á tarefa." << endl;
                }
        }
    }
}

void API::mostraContexto(Contexto & contexto) {
    // NOME -------------------------------------------------------------------    
    cout << "Nome: \"" << contexto.getNome() << "\";" << endl;
}

void API::mostraAcaoBase(AcaoBase & acaobase) {

    // NOME -------------------------------------------------------------------    
    cout << "Nome: \"" << acaobase.getNome() << "\";" << endl;

    // DATA_INICIO ------------------------------------------------------------
    cout << "Data de inicio: \""
            << (acaobase.getInicio().isNull() ? " --- " : acaobase.getInicio().toText(API::dateFormat))
            << "\";" << endl;

    // DATA_FIM ---------------------------------------------------------------
    cout << "Data de conclusão: \""
            << (acaobase.getFim().isNull() ? " --- " : acaobase.getFim().toText(API::dateFormat))
            << "\";" << endl;

    // PRIORIDADE ------------------------------------------------------------
    cout << "Prioridade: \""
            << acaobase.getPrioridade()
            << "\";" << endl;

    // ESTADO -----------------------------------------------------------------
    cout << "Estado: \""
            << acaobase.getEstado()
            << "\";" << endl;

}

void API::mostraTarefa(Tarefa & tarefa) {
    cout << "Detalhes da tarefa:" << endl;
    mostraAcaoBase(tarefa);


    // DURACAO_ESTIMADA -------------------------------------------------------
    cout << "Duracao estimada: \""
            << API::intervalDSToString(tarefa.getDuracaoEstimada())
            << "\";" << endl;

    // DURACAO_EFETIVA --------------------------------------------------------
    if (!tarefa.getDuracaoEfetiva().isNull()) {
        cout << "Duracao efetiva: \""
                << API::intervalDSToString(tarefa.getDuracaoEfetiva())
                << "\";" << endl;
    }
    
    // CONTEXTOS -------------------------------------------------------------
    cout << "Contextos da tarefa: \"";
    
    for ( Contexto c : bDados->listaContextosDeTarefa(tarefa)) {
        cout << "[" << c.getNome() << "]";
    }
    
    cout << "\";" << endl;

    Projeto * projeto = bDados->projetoDaTarefa(tarefa);
    if (projeto != NULL) {
        cout << "Associada ao projeto: \""
                << projeto->getNome()
                << "\";" << endl;
    }
}

void API::mostraProjeto(Projeto & projeto) {
    cout << "Detalhes do projeto:" << endl;
    mostraAcaoBase(projeto);
}

void API::mostraReferencia(Referencia & ref) {
    cout << "Detalhes da referencia:" << endl;

    // NOME -------------------------------------------------------------------    
    cout << "Nome: \"" << ref.GetNome() << "\";" << endl;

    list<Anotacao> anots = bDados->listaAnotacoesDeReferencia(ref);
    if (!anots.empty()) {
        cout << "Anotações: " << endl;
        for (Anotacao anot : anots) {
            cout << "\t\""
                    << anot.GetNome()
                    << "(" << anot.GetTipo()
                    << ")\""
                    << endl;
        }
    }
}

void API::editaProjeto(Projeto & projeto) {
    API::editaAcaoBase(projeto);
}
//

string API::intervalDSToString(IntervalDS intervalo) {
    ostringstream output;

    if (intervalo.isNull()) return "---";

    if (intervalo.getDay() != 0) {
        output << intervalo.getDay() << " dias, ";
    }

    output << setfill('0') << setw(2) << intervalo.getHour()
            << ":" << setfill('0') << setw(2) << intervalo.getMinute();

    return output.str();
}

IntervalDS API::readIntervalDS() {
    Environment * env = Environment::createEnvironment(Environment::DEFAULT);
    IntervalDS output(env);
    string tempstr;
    int dias, horas, minutos;

    cout << "\tDias? [0]: ";
    getline(cin, tempstr);
    dias = strtol(tempstr.c_str(), NULL, 0);

    cout << "\tHoras? [0]: ";
    getline(cin, tempstr);
    horas = strtol(tempstr.c_str(), NULL, 0);

    cout << "\tMinutos? [0]: ";
    getline(cin, tempstr);
    minutos = strtol(tempstr.c_str(), NULL, 0);

    output.set(dias, horas, minutos, 0, 0);
    return output;
}

bool API::simNao() {
    string opcao;
    getline(cin, opcao);

    if (opcao == "S" || opcao == "s")
        return true;

    return false;
}

void API::waitForLine() {
    string none;
    getline(cin, none);
}

bool API::verificaDadosUtilizador(Utilizador & u) {
    list<Utilizador> listU = API::bDados->listaUtilizadores();
    for (list<Utilizador>::iterator it = listU.begin(); it != listU.end(); it++) {
        if (it->GetNome() == u.GetNome() && it->GetPassword() == u.GetPassword()) {
            u = *it;
            return true;
        }

    }
    return false;
}

Utilizador API::leUtilizador() {
    Utilizador u;
    string nome, pass;
    cout << "Intruduza os dados do utilizador" << endl;
    cout << "Nome: ";
    getline(cin, nome);
    cout << "Password: ";
    getline(cin, pass);
    u.SetNome(nome);
    u.SetPassword(pass);
    return u;
}

void API::adicionaLista(list<Tarefa*> tarefas, const Utilizador& uti) {
    list<Tarefa*> existentes = bDados->listaTarefasDeUtilizador(uti);
    bool flag = false;
    for (Tarefa* tn : tarefas) {
        tn->setIdUtilizador(API::getLocalUser().GetIdUtilizador());
        for (Tarefa* te : existentes) {
            if (*tn == *te)
                flag = true;

        }
        
        if (flag) {
            if (tn->getIdUtilizador() == uti.GetIdUtilizador())
                bDados->atualiza(*tn);
            else {
                TarefaDelegada* td = new TarefaDelegada(*tn, API::localUser, uti, 'N', 'S');
                bDados->atualiza(*td);
            }
        } else {
            bDados->adiciona(*tn);
            if (tn->getIdUtilizador() != uti.GetIdUtilizador()) {
                TarefaDelegada* td = new TarefaDelegada(*tn, API::localUser, uti, 'N', 'S');
                bDados->adiciona(*td);
            }
        }
        flag = false;
    }
}

bool API::delegarTarefa(Tarefa& t, Utilizador destino, bool publica) {
    char pub;
    string extraPath = "partilhas.csv";
    string path = API::dropboxPath + extraPath;
    if (publica)
        pub = 'S';
    else
        pub = 'N';

    TarefaDelegada td(t, API::localUser, destino, pub, 'N');

    CSVio csv(path);
    bool sucesso = true;
    if(!API::remoto)sucesso = csv.insert(td);
    if (sucesso) {
        bDados->adiciona(td);
        return true;
    }
    return false;

}

void API::leDelegacao(list<Tarefa*> aDelegar) {
    if (aDelegar.empty()) return;

    Utilizador aQuemDelegar;
    
    cout << "Deseja tornar as tarefas publicas? (S|N): ";
    bool publica = API::simNao();

    aQuemDelegar = API::listaUtilizadores(API::getBDados()->listaUtilizadores());
    if (aQuemDelegar.GetIdUtilizador() == 0) return;


    if (!API::getBDados()->limparContextosDaTarefa(aDelegar)) {
        cout << "Não foi possivel limpar os contextos associados às tarefas" << endl;
        sleep(1);
    }
    for (Tarefa* t : aDelegar) {
        bool sucesso;
        sucesso = API::delegarTarefa(*t, aQuemDelegar, publica);
        if (!sucesso)
            cout << "Não foi possível exportar a tarefa: " << *t << endl;
        else
            cout << "Tarefa delegada com sucesso: " << *t << endl;
        sleep(2);
    }
}
