/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              virtual void insere(Tarefa* t,BDados* ligacao)               */
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/


#ifndef PRIORITYQUEUETAREFA_H
#define	PRIORITYQUEUETAREFA_H
#include "Tarefa.h"
#include "Queue.h"
#include "BDDados.h"


class PriorityQueueTarefa: public Queue<Tarefa*>
{
    public:
        enum Comparacao{
            prioridade = 0,
            dataFim,
            estado,
            criada
        };
        
        PriorityQueueTarefa();
        PriorityQueueTarefa(Comparacao comp, const list<Tarefa*>& tarefas,BDados* ligacao);
        PriorityQueueTarefa(const PriorityQueueTarefa& q);
        virtual ~PriorityQueueTarefa();
        
        void setComparacao(Comparacao comp,BDados* ligacao);
        Comparacao getComparacao() const;
        
        virtual void insere(Tarefa* t,BDados* ligacao);
        
    private:
        Comparacao comp;
            
        bool maiorOuIgual(Tarefa* maior,Tarefa* menor,BDados* ligacao);

};

#endif	/* PRIORITYQUEUETAREFA_H */

