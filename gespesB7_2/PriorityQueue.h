/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              virtual void insere(const T& elem)                           */
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/



#ifndef PriorityQueue_
#define PriorityQueue_

#include "Queue.h"

template<class T>
class PriorityQueue : public Queue<T>
{
public:
		PriorityQueue();
		PriorityQueue(const PriorityQueue<T>& q);
		virtual ~PriorityQueue();

		virtual void insere(const T& elem);
};

template<class T>
PriorityQueue<T>::PriorityQueue() : Queue()
{
}

template<class T>
PriorityQueue<T>::PriorityQueue(const PriorityQueue<T>& q) : Queue(q)
{
}

template<class T>
PriorityQueue<T>::~PriorityQueue()
{
}

template<class T>
void PriorityQueue<T>::insere(const T& elem)
{
	Queue <T> qtemp = *this;
	T el;

	while (!vazia()) retira(el);

	while ((!qtemp.vazia()) && (qtemp.frente() >= elem)) {
		qtemp.retira(el);
		Queue<T>::insere(el);
	}

	Queue<T>::insere(elem);

	while (!qtemp.vazia()) {
		qtemp.retira(el);
		Queue<T>::insere(el);
	}
}


#endif