/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              virtual void escreve(ostream &out) const                     */
/*              const Tarefa& operator = (const Tarefa &p)                   */                                                   
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/

#include "Tarefa.h"

Tarefa::Tarefa():AcaoBase(){}

Tarefa::Tarefa(int idUtilizador, string nome, Timestamp criada, 
        Timestamp modificada, Date fim, Date inicio, string prioridade, 
        string estado, IntervalDS duracaoEfetiva, IntervalDS duracaoEstimada):
        AcaoBase(idUtilizador,nome, criada, modificada, fim, inicio, prioridade, estado)
{
    this->duracaoEfetiva=duracaoEfetiva;
    this->duracaoEstimada=duracaoEstimada;

}


Tarefa::Tarefa(int idUtilizador) : AcaoBase(idUtilizador) {
}

Tarefa::Tarefa(int idUtilizador, Timestamp criada) : AcaoBase(idUtilizador, criada) {}

Tarefa::Tarefa(const Tarefa & t) : AcaoBase(t){
    this->duracaoEfetiva=t.duracaoEfetiva;
    this->duracaoEstimada=t.duracaoEstimada;
}

void Tarefa::setIdUtilizador(int idUtilizador)
{
    this->idUtilizador = idUtilizador;
}

void Tarefa::setDuracaoEstimada(IntervalDS duracaoEstimada) {
    this->duracaoEstimada = duracaoEstimada;
}

IntervalDS Tarefa::getDuracaoEstimada() const {
    return duracaoEstimada;
}

void Tarefa::setDuracaoEfetiva(IntervalDS duracaoEfetiva) {
    this->duracaoEfetiva = duracaoEfetiva;
}

IntervalDS Tarefa::getDuracaoEfetiva() const {
    return duracaoEfetiva;
}

void Tarefa::escreve(ostream &out) const {
    AcaoBase::escreve(out);
//    out << "|" << (duracaoEfetiva.isNull() ? "Indefinido" : duracaoEfetiva.toText(3, 2))
//            << "|" << (duracaoEstimada.isNull() ? "Indefinido" : duracaoEstimada.toText(3, 2))
//            << "|";
}

const Tarefa& Tarefa::operator =(const Tarefa &p) {
    AcaoBase::operator=(p);
    this->duracaoEfetiva = p.duracaoEfetiva;
    this->duracaoEstimada = p.duracaoEstimada;
    return *this;
}

ostream& operator <<(ostream &out, const Tarefa &p) {
    p.escreve(out);
    return out;
}
