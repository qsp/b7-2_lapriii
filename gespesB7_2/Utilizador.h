/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              void escreve(ostream &out) const                             */ 
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/


#ifndef UTILIZADOR_H
#define	UTILIZADOR_H
#include <iostream>
#include <string>

using namespace std;

class Utilizador {
private:
    int idUtilizador;
    string nome;
    string password;
    int codigo;

public:
    Utilizador();
    Utilizador(int idU, string no, int cod,string pass);
    Utilizador(string nome);
    Utilizador(int idU);
    Utilizador(string no, int cod,string pass);
    void SetNome(string nome);
    string GetNome() const;
    void SetIdUtilizador(int idUtilizador);
    int GetIdUtilizador() const;
    void SetCodigo(int codigo);
    int GetCodigo() const;
    void SetPassword(string password);
    string GetPassword() const;
    void escreve(ostream &out) const;
};

ostream & operator << (ostream &out, const Utilizador &c);



#endif	/* UTILIZADOR_H */

