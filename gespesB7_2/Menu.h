/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              static int menuTarefa()                                      */
/*              static void showMenu()                                       */
/*              static int menuPrincipal()                                   */
/*              static int menuProjeto()                                     */
/*              static int menuReferencia()                                  */
/*              static int menuAnotacao()                                    */
/*              static int menuContexto()                                    */
/*              static void showMenuProjeto(Projeto & projeto)               */
/*              static void showMenuReferencia(Referencia & ref)             */
/*              static void showMenuContexto(Contexto & contexto)            */
/*              static void showMenuAnotacao(Anotacao & anotacao)            */
/*              static void showMenuTarefa(Tarefa * tarefa)                  */
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/



#ifndef MENU_H
#define	MENU_H
#include <iostream>
#include <string>
#include <iomanip>
#include <occi.h>
#include "TarefaDelegada.h"
#include "Projeto.h"
#include "Referencia.h"
#include "Contexto.h"


using namespace std;
using namespace oracle::occi;

class Menu {
    
public:
  static int menuTarefa();
  static void showMenu();
  static int menuPrincipal();
  static int menuProjeto();
  static int menuReferencia();
  static int menuAnotacao();
  static int menuContexto();
  static void showMenuProjeto(Projeto & projeto);
  static void showMenuReferencia(Referencia & ref);
  static void showMenuContexto(Contexto & contexto);
  static void showMenuAnotacao(Anotacao & anotacao);
  static void showMenuTarefa(Tarefa * tarefa);
  static void showMenuTarefaDelegada(TarefaDelegada & td);
};



#endif	/* MENU_H */

