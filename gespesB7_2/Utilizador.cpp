/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              void escreve(ostream &out) const                             */ 
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/

#include <iomanip>
#include "Utilizador.h"

Utilizador::Utilizador(int idU, string no,  int cod,string pass){
    this->idUtilizador=idU;
    this->nome=no;
    this->password=pass;
    this->codigo=cod;
}

Utilizador::Utilizador(string nome)
{
    this->nome = nome;
}

Utilizador::Utilizador(string no,  int cod,string pass){
    this->nome=no;
    this->password=pass;
    this->codigo=cod;
}
Utilizador::Utilizador(int idU){
    this->idUtilizador=idU;
}
Utilizador::Utilizador(){
}
void Utilizador::SetNome(string nome) {
    this->nome = nome;
    this->codigo = 0;
}

string Utilizador::GetNome() const {
    return nome;
}

void Utilizador::SetIdUtilizador(int idUtilizador) {
    this->idUtilizador = idUtilizador;
}

int Utilizador::GetIdUtilizador() const {
    return idUtilizador;
}

void Utilizador::SetCodigo(int codigo) {
    this->codigo = codigo;
}

int Utilizador::GetCodigo() const {
    return codigo;
}

void Utilizador::SetPassword(string password) {
    this->password = password;
}

string Utilizador::GetPassword() const {
    return password;
}
void Utilizador::escreve(ostream &out) const {
out << setw(12) << idUtilizador << " | ";
out << setw(30) << nome << " | ";
out << setw(12) << codigo << " | " <<endl;
}

ostream & operator << (ostream &out, const Utilizador &c) {
c.escreve(out);
return out;
}