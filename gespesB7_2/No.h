/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções                                                              */                                                   
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/


#ifndef No_
#define No_
#include<iostream>

using namespace std;

template <class T> 
class Lista ;

template <class T> 
class Queue ;

template <class T> 
class Stack ;

template <class T> 
class Lista_Iterador ;

template<class T>
class No
{
	friend class Lista<T>;
	friend class Queue<T>;
	friend class Stack<T>;
	friend class Lista_Iterador<T>;

	private:
		T info;
		No<T>* prox;

	public:
		No () 
		{  
			prox = NULL ; 
		}

		No (const No<T> &n)
		{
			info = n.info ; 
			prox = n.prox ; 
		}
		~No () 
		{  
		}

		void setInfo (const T i) 
		{
			info = i ; 
		}

		T& getInfo()  const 
		{
			return info ; 
		}

		void setProx (No<T>* ap) 
		{
			prox = ap ; 
		}

		No<T>* getProx() const 
		{
			return prox ; 
		}
};

#endif