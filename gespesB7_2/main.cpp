/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              main()                                                       */
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/


#include <iostream>
#include <string>
#include "BDDados.h"
#include "Utilizador.h"
#include "Tarefa.h"
#include "TarefaDelegada.h"
#include "CSVio.h"
#include <stdio.h>
#include <stdlib.h>
#include <typeinfo>
#include "API.h"
#include "Menu.h"
#include <limits.h>
#include <unistd.h>

using namespace std;

#define FPART "/home/qsp/Insync/bmflavio@gmail.com/ISEP/LAPR3/TESTE_PARTILHA.csv"
#define FMOBILE "/home/qsp/Insync/bmflavio@gmail.com/ISEP/LAPR3/TESTE_MOBILE.csv"
#define FTAR "/home/qsp/Insync/bmflavio@gmail.com/ISEP/LAPR3/TESTE_TAR.csv"

int main(int argc, char* argv[]) {
    API::loadPath();
    try {

        //Verifica se existe um utilizador local ou é a primeira vez que utiliza o programa
        string tmp;
        if(argc > 2) tmp = argv[1];

        if (tmp == "-unome") {
            string nome = argv[2];
            Utilizador u = API::criarUtilizador(nome);
            API::setLocalUser(u);
        } else {
            list<Utilizador> listU = API::getBDados()->listaUtilizadores();

            if (listU.empty()) {
                if (argc > 0) {
                    cout << "Tem que correr o programa uma vez antes de utilizar esta funcionalidade";
                    return -1;
                }
                Utilizador u;
                string nome;
                cout << "Intruduza o seu nome de utilizador:";
                getline(cin, nome);
                u.SetNome(nome);
                API::getBDados()->adiciona(u);
                API::setLocalUser(u);
            } else {
                API::setLocalUser(listU.front());
            }
            string argv1, argv2;
            if (argc == 3) {

                argv1 = argv[1];
                argv2 = argv[2];
                Tarefa t(API::getLocalUser().GetIdUtilizador());
                t.setNome(argv2);


                if (argv1 == "txt") {
                    if (API::getBDados()->adiciona(t) && API::getBDados()->adicionaAnotacaoPorTexto(t, argv2, argv1)) {
                        API::getBDados()->le(t);
                        cout << "Tarefa e anotacção adicionadas com sucesso" << endl;
                    } else {
                        cout << "Erro na introdução da tarefa";
                    }
                } else if (argv1 == "file") {
                    char workingPath[ PATH_MAX ];
                    getcwd(workingPath, PATH_MAX);
                    string fileName = workingPath;
                    fileName.append("/");
                    fileName.append(argv2);
                    cout << fileName;
                    if (API::getBDados()->adiciona(t) && API::getBDados()->adicionaAnotacaoPorFicheiro(t, fileName, argv1)) {
                        cout << "Tarefa e anotacção adicionadas com sucesso" << endl;
                    } else {
                        cout << "Erro na introdução da tarefa";
                    }
                } else {
                    cout << "Opção não reconhecida" << endl;
                }
                return 1;

            }
        }
        Menu::showMenu();
    } catch (SQLException erro) {
        cerr << "Erro: " << erro.getMessage() << endl;
    };

    return 0;
}