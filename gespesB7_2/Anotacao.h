/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              void escreve(ostream &out)const                              */
/*              void dadosToFile(string fileName) const                      */
/*              const Anotacao& operator =(const Anotacao &p)                */
/*              void exportar()                                              */
/*              void openFileFromBlob(                                       */
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/



#ifndef ANOTACAO_H
#define	ANOTACAO_H
#include <iostream>
#include <string>
#include "occi.h"

using namespace std;
using namespace oracle::occi;

class Anotacao {
    friend class BDados;
private:
    int idUtilizador;
    int idAnotacao;
    string nome;
    string tipo;
    Blob dados;

public:
    Anotacao();
    Anotacao(int idUtilizador);
    Anotacao(int idUtilizador, int idAnotacao, string nome, string tipo, Blob dados);
    void SetDados(Blob dados);
    Blob GetDados() const;
    void SetTipo(string tipo);
    string GetTipo() const;
    void SetNome(string nome);
    string GetNome() const;
    void SetIdAnotacao(int idAnotacao);
    int GetIdAnotacao() const;
    void SetIdUtilizador(int idUtilizador);
    int GetIdUtilizador() const;
    void escreve(ostream &out)const;
    void dadosToFile(string fileName) const;
    const Anotacao& operator =(const Anotacao &p);
    void exportar();
    void openFileFromBlob();
};
ostream& operator << (ostream &out, const Anotacao &p);



#endif	/* ANOTACAO_H */

