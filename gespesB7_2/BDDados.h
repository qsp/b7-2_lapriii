/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              list <Utilizador> listaUtilizadores()                        */
/*              bool le(Utilizador & uti)                                    */
/*              bool adiciona(Utilizador & uti)                              */
/*              bool atualiza(const Utilizador & uti)                        */
/*              bool elimina(const Utilizador & uti)                         */
/*              bool existeUtilizadorPorNomePassword(Utilizador & uti)       */
/*              list <Tarefa*> listaTarefasDeUtilizador(                     */
/*                      const Utilizador & uti)                              */
/*              list <Tarefa*> listaTarefasDelegadasAoUtilizador(            */
/*                      const Utilizador & uti,char aceite)                  */
/*              list<Tarefa*> listaTarefasPorContexto(                       */
/*                      const Utilizador& uti, string contexto)              */
/*              list <Tarefa*> listaTarefasPorContextos(                     */
/*                      const Utilizador& uti,list<Contexto> contextos)      */
/*              list<Tarefa> listarTarefasDeProjeto(Projeto & p)             */
/*              list<Tarefa> listarTarefasPorNome(string nome)               */
/*              bool limparContextosDaTarefa(list<Tarefa*> tarefas)          */
/*              bool atualiza(Tarefa & t)                                    */
/*              bool atualiza(Tarefa & t)                                    */
/*              bool le(Tarefa & t)                                          */
/*              bool adiciona(Tarefa & t)                                    */
/*              bool elimina(const Tarefa & t)                               */
/*              bool adiciona(TarefaDelegada & t)                            */
/*              bool atualiza(TarefaDelegada & t)                            */
/*              bool le(TarefaDelegada & t)                                  */
/*              bool elimina(TarefaDelegada & t)                             */
/*              bool adicionaDependenciaTarefa(Tarefa & t, Tarefa & tDep)    */
/*              bool eliminaDependenciaTarefa(Tarefa & t, Tarefa & tDep)     */
/*              ListAdjGrafo<Tarefa, int> criaGrafodependenciaTarefas(       */
/*                      Utilizador u)                                        */
/*              ListAdjGrafo<Tarefa, int> criaGrafodependenciaTarefas(       */
/*                      Projeto j)                                           */
/*              bool adiciona(Contexto & c)                                  */
/*              bool le(Contexto & c)                                        */
/*              bool atualiza(const Contexto & c)                            */
/*              bool desligarContexto(const Contexto& c)                     */
/*              bool elimina(const Contexto & c)                             */
/*              bool adiciona(const Tarefa & t,const Contexto & c)           */
/*              list<Contexto> listaContextosDeTarefa(const Tarefa& t)       */
/*              list<Contexto> listaContextosDeUtilizador(                   */
/*                      const Utilizador& uti)                               */
/*              list<Anotacao> listaAnotacoesDeAcaoBase(AcaoBase & t)const   */
/*              bool adicionaAnotacaoPorFicheiro(AcaoBase b, string fileName,*/
/*                      string tipoAno)                                      */
/*              bool adicionaAnotacaoPorTexto(AcaoBase b, string nome,       */
/*                      string tipoAno)                                      */
/*              bool elimina(Anotacao &a)                                    */
/*              list<Anotacao>listaAnotacoesDeReferencia(                    */
/*                      const Referencia & t)const                           */
/*              bool le(Projeto & projeto)                                   */
/*              list<Projeto>  listarProjetosDeUtilizador(Utilizador uti)    */
/*              bool transformarTarefaProjeto(Tarefa & t,Projeto & p)        */
/*              bool adicionarTarefaAProjeto(Tarefa t, Projeto p)            */
/*              bool removerTarefaDeProjeto(Tarefa t, Projeto p)             */
/*              Projeto * projetoDaTarefa(const Tarefa & t)                  */
/*              bool atualiza(Projeto & t)                                   */
/*              bool elimina(const Projeto & t)                              */
/*              bool transformarTarefaReferencia(Tarefa & t, Referencia &p)  */
/*              list<Referencia> listaReferenciasDeUtilizador(Utilizador u)  */
/*              bool elimina(Referencia & r)                                 */
/*              Timestamp agora()                                            */
/*              vector<string> listaPrioridades()                            */
/*              vector<string> listaEstados()                                */
/*              int tipoAnotacaoToIdTipoAnotacao(string p)                   */
/*              int prioridadeToIdPrioridade(string p)                       */
/*              int estadoToIdEstado(string p)                               */
/*              string idUtilizadorToNome(int idUtilizador)                  */
/*              int nomeUtilizadorToId(string const nome)                    */
/*              int contextoToIdContexto(string c)                           */
/*              vector<string> listaTipoAnotacao()                           */
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/


#ifndef BDados_
#define BDados_
#include <iostream>
#include <iomanip>
#include <occi.h>
#include <list>
#include "Utilizador.h"
#include "Tarefa.h"
#include "TarefaDelegada.h"
#include "Contexto.h"
#include "Projeto.h"
#include "Referencia.h"
#include "ListaAdjGrafo.h"

using namespace oracle::occi;

class BDados {
private:
    Environment *env;
    Connection *ligacao;
    bool commit;
    string tablePrefix;
public:
    BDados(std::string user, std::string passwd, std::string db, std::string tablePrefix);
    ~BDados();
    // Getters/Setters
    void setLigacao(Connection* ligacao);
    Connection* getLigacao() const;

    // Classe Utilizador
    list <Utilizador> listaUtilizadores();
    bool le(Utilizador & uti);
    bool adiciona(Utilizador & uti);
    bool atualiza(const Utilizador & uti);
    bool elimina(const Utilizador & uti);
    bool existeUtilizadorPorNomePassword(Utilizador & uti);
    bool existeUtilizadorPorNome(Utilizador & uti);

    // Classe Tarefas
    list <Tarefa*> listaTarefasDeUtilizador(const Utilizador & uti);
    list <Tarefa*> listaTarefasDelegadasAoUtilizador(const Utilizador & uti,char aceite);
    list <Tarefa*> listaTarefasDelegadasPorUtilizador(const Utilizador & uti);
    TarefaDelegada getTarefaDelegadaPor(Utilizador & u, Tarefa & t);
    TarefaDelegada getTarefaDelegadaPara(Utilizador & u, Tarefa & t);
    list<Tarefa*> listaTarefasPorContexto(const Utilizador& uti, string contexto);
    list <Tarefa*> listaTarefasPorContextos(const Utilizador& uti,list<Contexto> contextos);
    list<Tarefa> listarTarefasDeProjeto(Projeto & p);
    list<Tarefa> listarTarefasPorNome(string nome);
    list <Tarefa*> listaTarefasDeUtilizadorQQDia(const Utilizador & uti);
    bool limparContextosDaTarefa(list<Tarefa*> tarefas);
    bool atualiza(Tarefa & t);
    bool le(Tarefa & t);    
    bool adiciona(Tarefa & t);
    bool elimina(const Tarefa & t);
    
    
    // Classe Tarefa_Delegada
    bool adiciona(TarefaDelegada & t);
    bool atualiza(TarefaDelegada & t);
    bool le(TarefaDelegada & t);
    bool elimina(TarefaDelegada & t);

    // Dependencia Tarefas
    bool adicionaDependenciaTarefa(Tarefa & t, Tarefa & tDep);
    bool eliminaDependenciaTarefa(Tarefa & t, Tarefa & tDep);
    list<Tarefa> listaTarefasDependentes(Tarefa & t);
    ListAdjGrafo<Tarefa, int> criaGrafodependenciaTarefas(Utilizador u);
    ListAdjGrafo<Tarefa, int> criaGrafodependenciaTarefas(Projeto j);

    // Classe contexto
    bool adiciona(Contexto & c);
    bool le(Contexto & c);
    bool atualiza(const Contexto & c);
    bool desligarContexto(const Contexto& c);
    bool elimina(const Contexto & c);
    bool adiciona(const Tarefa & t,const Contexto & c);
    list<Contexto> listaContextosDeTarefa(const Tarefa& t);
    list<Contexto> listaContextosDeUtilizador(const Utilizador& uti);
    
    // Classe Anotacao
    list<Anotacao> listaAnotacoesDeAcaoBase(AcaoBase t)const;
    bool adicionaAnotacaoPorFicheiro(AcaoBase b, string fileName, string tipoAno);
    bool adicionaAnotacaoPorTexto(AcaoBase b, string nome, string tipoAno);
    bool elimina(Anotacao &a);
    list<Anotacao>listaAnotacoesDeReferencia(const Referencia & t)const;

    // Classe Projeto
    bool le(Projeto & projeto);  
    list<Projeto>  listarProjetosDeUtilizador(Utilizador uti);
    bool transformarTarefaProjeto(Tarefa & t,Projeto & p);
    bool adicionarTarefaAProjeto(Tarefa t, Projeto p);
    bool removerTarefaDeProjeto(Tarefa t, Projeto p);
    Projeto * projetoDaTarefa(const Tarefa & t);
    bool atualiza(Projeto & t);
    bool elimina(const Projeto & t);

    // Classe Referência   
    bool transformarTarefaReferencia(Tarefa & t, Referencia &p);
    list<Referencia> listaReferenciasDeUtilizador(Utilizador u);
    bool elimina(Referencia & r);

    // Metodos Auxiliares
    Timestamp agora();
    vector<string> listaPrioridades();
    vector<string> listaEstados();
    int tipoAnotacaoToIdTipoAnotacao(string p);
    int prioridadeToIdPrioridade(string p);
    int estadoToIdEstado(string p);
    string idUtilizadorToNome(int idUtilizador);
    int nomeUtilizadorToId(string const nome);
    long mediaDuracaoPorNomeParcial(string const parteDoNome);
    int contextoToIdContexto(string c);
    vector<string> listaTipoAnotacao();

};
#endif
