/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              static string toMobileCSVstring(const Tarefa & t, BDados& db)*/
/*              static string toCSVstring(const Tarefa & t)                  */
/*              static Tarefa tarefaFromCSVstring(const string & input)      */
/*              static string toCSVstring(const TarefaDelegada & t)          */
/*              static TarefaDelegada tarefaDelegadaFromCSVstring(           */
/*              const string & input)                                        */
/*              static string encode(const string original)                  */
/*              static string decode(const string original)                  */
/*              static vector<string> parseCSV(const string & text)          */
/*              bool insert(const TarefaDelegada & td)                       */
/*              bool remove(const TarefaDelegada & td)                       */
/*              static bool exportarListaTarefasMobile (                     */
/*                      list<Tarefa*> & tarefas, string file, BDados & db)   */
/*              static bool exportarListaTarefas (                           */
/*                      list<Tarefa*> & tarefas, string file)                */
/*              static bool importarListaTarefas (                           */
/*                      list<Tarefa*> & tarefas, string file)                */
/*              static void exportarReferencia(const Referencia & ref,       */
/*                      string path, BDados * db)                            */
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/



#include "CSVio.h"
#include "TarefaDelegada.h"
#include "BDDados.h"
#include <sstream>
#include <cstdlib>
#include <fstream>
#include <sys/stat.h>
#include <sys/types.h>


CSVio::CSVio() {
    path = "";
}

CSVio::CSVio(string path) {
    this->path = path;
}

CSVio::CSVio(const CSVio& orig) {
    path = orig.path;
    dataLines = orig.dataLines;
}

CSVio::~CSVio() {
}

int CSVio::read (){
    if (path.empty()) return FILE_PATH_UNDEFINED;
    
    dataLines.erase(dataLines.begin(), dataLines.end());
    int numLinhas = 0;
    ifstream ficheiro;
    ficheiro.open(path);

    if (ficheiro.is_open()) {
        while (!ficheiro.eof()) {
            string line;
            getline(ficheiro, line);
            if (!line.empty()) {
                dataLines.push_back(line);
                numLinhas++;
            }
        }
        ficheiro.close();
    } else return FILE_OPEN_ERROR;
    
    ficheiro.close();
    return numLinhas;
}

int CSVio::write() {
    
    if (path.empty()) return FILE_PATH_UNDEFINED;
    
    int numLinhas = 0;
    ofstream ficheiro;
    ficheiro.open(path, ios::trunc);

    if (ficheiro.is_open()) {
        numLinhas++;
        for (string linha : dataLines) {
            ficheiro << linha << endl;
        }
        ficheiro.close();
    } else return FILE_OPEN_ERROR;
    ficheiro.flush();
    ficheiro.close();
    return numLinhas;
}

bool CSVio::remove(const TarefaDelegada &td) {
    //para manter a segurança dos dados optamos por ler e escrever o ficheiro antes de cada inserção.
    if (read() < 0){
        cout << "INFORMAÇÃO: Não foi possível ler o ficheiro de delegações ou "
                "o mesmo ainda não existe. Será criado um novo."<<endl;
    }
    
    bool removido = false;
    //começa por eliminar a tarefa, se já existe no ficheiro    
    for (vector<string>::iterator num = dataLines.begin(); num != dataLines.end() ; num++) {
        TarefaDelegada comp = CSVio::tarefaDelegadaFromCSVstring(*num);
        if (td == comp) {            
            dataLines.erase(num);
            removido = true;
            break;
        }
    }    
    
    if (removido)
        if (write() < 0) {
            cout << "INFORMAÇÃO: Não foi possível escrever o ficheiro de delegações ou "
                "o mesmo ainda não existe. Será criado um novo."<<endl;
            return false;
        }
    
    return removido;
}

bool CSVio::insert(const TarefaDelegada &td) {    
    //para manter a segurança dos dados optamos por ler e escrever o ficheiro antes de cada inserção.
    if (read() < 0){
        cout << "INFORMAÇÃO: Não foi possível ler o ficheiro de delegações ou "
                "o mesmo ainda não existe. Será criado um novo."<<endl;
    }
    
    remove(td);
    dataLines.push_back(CSVio::toCSVstring(td));
    
    if(write() < 0) {
        cout << "INFORMAÇÃO: Não foi possível ler o ficheiro de delegações ou "
                "o mesmo ainda não existe. Será criado um novo."<<endl;
        return false;
    }
    return true;
}

vector<TarefaDelegada> CSVio::getTarefasDelegadas(string nomeUtilizador){
        
    vector<TarefaDelegada> retorno;
    int numLinha = 0;
    
    //para manter a segurança dos dados optamos por ler e escrever o ficheiro antes de cada inserção.
    if (read() < 0) return retorno;
    
    for (string linha : dataLines) {
        numLinha++;
        try {
            TarefaDelegada td = CSVio::tarefaDelegadaFromCSVstring(linha);
            if (td.getNomeUtilizador() == nomeUtilizador ||
                td.getNomeUtilizadorDestino() == nomeUtilizador) {
                retorno.push_back(td);
            }
        }
        catch (...) {
            cerr << "Erro ao ler a linha de dados: \"" << linha << "\"." << endl;
        }
    }
    return retorno;
}

string CSVio::encode(const string original) {
    //prepara o nome da tarefa para substituir virgulas por \,
    string sub;
    sub.append(1,CSV_ESCAPE_CHAR);
    sub.append(1,CSV_SEP);
    string ori;
    ori.append(1,CSV_SEP);
    
    return coder(original, ori, sub);
}

string CSVio::decode(const string original) {
    
    string sub;
    sub.append(1,CSV_SEP);
    string ori;
    ori.append(1,CSV_ESCAPE_CHAR);
    ori.append(1,CSV_SEP);
        
    return coder(original, ori, sub);
}

string CSVio::coder(string entrada,string & ori, string & sub) {
        
    size_t pos = 0;    
    while((pos = entrada.find(ori, pos)) != std::string::npos)
    {
       entrada.replace(pos, ori.length(), sub);
       pos += sub.length();
    }
    
    return entrada;
}

void CSVio::exportarReferencia(const Referencia & ref, string path, BDados * db) {
    string ficheiro(path);
    ficheiro.append("/referencia_");
    ficheiro.append(static_cast<ostringstream*>( &(ostringstream() << ref.GetIdUtilizador()) )->str());
    ficheiro.append("_");
    ficheiro.append(static_cast<ostringstream*>( &(ostringstream() << ref.GetIdReferencia()) )->str());    
    
    stringstream csvOutput;    
    string nome_preparado = encode(ref.GetNome());
    
    list<Anotacao> anots = db->listaAnotacoesDeReferencia(ref);    
                    
    if (!anots.empty()) {
        try {
                //cria diretorio para os ficheiros de anotacao
                mkdir(ficheiro.c_str(), 0777);
                for (Anotacao anot : anots) {  
                    if (anot.GetTipo() == "file") {
                        string anotFile(ficheiro);
                        anotFile.append("/");
                        anotFile.append(anot.GetNome());

                        anot.dadosToFile(anotFile);   
                    }
                    
                }
                cout << "Anotações exportadas para o diretório \""
                        << ficheiro << "\"." << endl;
        } catch (...) {
            cout << "Não foi possível exportar as anotações." << endl;
        }
        
        for (Anotacao anot : anots) {
        csvOutput << ref.GetIdUtilizador()
                << CSV_SEP
            << ref.GetIdReferencia()
                << CSV_SEP
            << anot.GetIdAnotacao()
                << CSV_SEP
            << encode(anot.GetNome())
                << CSV_SEP
            << encode(anot.GetTipo())
                << CSV_ENDLINE << endl;            
        }
    }
    else {
        csvOutput << ref.GetIdUtilizador()
                << CSV_SEP
            << ref.GetIdReferencia()
                << CSV_ENDLINE << endl;
    }

    ficheiro.append(".csv");
    CSVio refexport(ficheiro);
        
    refexport.dataLines.push_back(csvOutput.str());
    
    if (refexport.write() < 0 ) {
        cout << "Erro durante a escrita da referência." << endl;
    } else {
        cout << "Referência exportada." << endl;
    };
}

string CSVio::toCSVstring(const Tarefa & t){
    
    stringstream csvOutput;    
    string nome_preparado = encode(t.getNome());
    
    csvOutput<< t.getIdUtilizador()
                << CSV_SEP
            << (t.getCriada().isNull() ? "" : t.getCriada().toText(CSV_TIMESTAMPFORMAT, 9))
                << CSV_SEP
            << nome_preparado
                << CSV_SEP
            << (t.getInicio().isNull() ? "" : t.getInicio().toText(CSV_DATEFORMAT))
                << CSV_SEP
            << (t.getModificada().isNull() ? "" : t.getModificada().toText(CSV_TIMESTAMPFORMAT, 9))
                << CSV_SEP
            << t.getPrioridade()
                << CSV_SEP
            << t.getEstado()
                << CSV_SEP
            << (t.getFim().isNull() ? "" : t.getFim().toText(CSV_DATEFORMAT))
                << CSV_SEP
            << (t.getDuracaoEfetiva().isNull() ? "" : t.getDuracaoEfetiva().toText(3, 2))
                << CSV_SEP
            << (t.getDuracaoEstimada().isNull() ? "" : t.getDuracaoEstimada().toText(3, 2))
                << CSV_ENDLINE;
    
    
    return csvOutput.str();
}

string CSVio::toMobileCSVstring(const Tarefa & t, BDados & db){
    
    stringstream csvOutput;    
    int prioridade = db.prioridadeToIdPrioridade(t.getPrioridade());
    
    string contextosStr;    
    list <Contexto> contextos = db.listaContextosDeTarefa(t);
    cout << "NumContextos:" << contextos.size() << endl;
    
    list <int> idContextos;
    
    for (Contexto ct : contextos) {
        idContextos.push_back(ct.getIdContexto());
    }
    
    idContextos.sort();
    list<int>::iterator icont = idContextos.begin();
    
    for ( int pos = 1; icont!=idContextos.end(); ++pos ) {
        cout<<"icont"<<*icont<<endl;
        if ( pos == *icont ){
            contextosStr.append("1");
            icont++;
        } else {
            contextosStr.append("0");
        }  
    }
        
    string nome_preparado = encode(t.getNome());
        
    
    csvOutput<< (t.getCriada().isNull() ? "" : t.getCriada().toText(MOBILE_CSV_TIMESTAMPFORMAT, 9))
                << MOBILE_CSV_SEP
            << nome_preparado
                << MOBILE_CSV_SEP
            << prioridade
                << MOBILE_CSV_SEP            
            << contextosStr
                << MOBILE_CSV_ENDLINE;
    
    
    return csvOutput.str();
}

bool CSVio::exportarListaTarefasMobile (list<Tarefa*> & tarefas, string file, BDados & db) {
    
    CSVio ficheiro(file);
    
    for (Tarefa * t : tarefas) {        
        ficheiro.dataLines.push_back(CSVio::toMobileCSVstring(*t, db));
    }
    
    if (ficheiro.write() < 0 ) return false;
    return true;
    
}

bool CSVio::exportarListaTarefas (list<Tarefa*> & tarefas, string file) {
    
    CSVio ficheiro(file);
    
    for (Tarefa *t : tarefas) {        
        ficheiro.dataLines.push_back(CSVio::toCSVstring(*t));
    }
    
    if (ficheiro.write() < 0 ) return false;
    return true;    
}

bool CSVio::importarListaTarefas (list<Tarefa*> & tarefas, string file) {
    CSVio ficheiro(file);
    
    if (ficheiro.read() < 0) return false;
    
    for (string linha : ficheiro.dataLines) {        
        tarefas.push_back(new Tarefa(CSVio::tarefaFromCSVstring(linha)));
    }
    
    if (ficheiro.write() < 0 ) return false;
    return true;
};

Tarefa CSVio::tarefaFromCSVstring(const string & input){
    
    vector<string> campos = CSVio::parseCSV(input);
    
    Environment * env = Environment::createEnvironment(Environment::DEFAULT);
    
    int idUtilizador = strtol(campos[0].c_str(), NULL, 10);
    
    Timestamp criada(env);
    criada.fromText(campos[1], CSV_TIMESTAMPFORMAT);
    
    string nome = decode(campos[2]);

    Date inicio(env);
    if (campos[3].length() >= string(CSV_DATEFORMAT).length())
        inicio.fromText(campos[3], CSV_DATEFORMAT);
    else inicio.setNull();
    
    Timestamp modificada(env);
    modificada.fromText(campos[4], CSV_TIMESTAMPFORMAT);
    
    string prioridade(campos[5]);
    
    string estado(campos[6]);
    
    Date fim(env);
    if (campos[7].length() >= string(CSV_DATEFORMAT).length())
        fim.fromText(campos[7], CSV_DATEFORMAT);
    else fim.setNull();
    
    IntervalDS duracaoEfetiva(env);
    if (campos[8].length() >= string(CSV_INTERVALDSFORMAT).length())
        duracaoEfetiva.fromText(string(campos[8]));
    else duracaoEfetiva.setNull();
    

    IntervalDS duracaoEstimada(env);
    if (campos[9].length() >= string(CSV_INTERVALDSFORMAT).length())
        duracaoEstimada.fromText(string(campos[9]));
    else duracaoEstimada.setNull();
    
    Tarefa t( idUtilizador, nome, criada,
            modificada, fim, inicio,
            prioridade, estado, duracaoEfetiva,
            duracaoEstimada);
    
    return t;
}

string CSVio::toCSVstring(const TarefaDelegada & td){
    
    stringstream csvOutput;    
    string tarefa = CSVio::toCSVstring((Tarefa)td);
    
    //eliminamos o fim de linha da exportação anterior.
    tarefa.erase(tarefa.find_last_not_of(CSV_ENDLINE) +1);        
    
    csvOutput<< tarefa
                << CSV_SEP
            << CSVio::encode(td.getNomeUtilizador())
                << CSV_SEP
            << td.getIdUtilizadorDestino()
                << CSV_SEP
            << CSVio::encode(td.getNomeUtilizadorDestino())
                << CSV_SEP
            << td.getAceiteChar()
                << CSV_SEP
            << td.getPublicaChar()                
                << CSV_ENDLINE;    
    
    return csvOutput.str();
}

TarefaDelegada CSVio::tarefaDelegadaFromCSVstring(const string & input){
    
    Tarefa tarefa = CSVio::tarefaFromCSVstring(input);
    
    vector<string> campos = CSVio::parseCSV(input);    
    Environment * env = Environment::createEnvironment(Environment::DEFAULT);
    
    string nomeUtilizador = CSVio::decode(campos[10]);    
    int idUtilizadorDestino = strtol(campos[11].c_str(), NULL, 10);        
    string nomeUtilizadorDestino = CSVio::decode(campos[12]);
    
    char aceite = campos[13][0];
    char publica = campos[14][0];
    
    Utilizador origem(tarefa.getIdUtilizador());
    origem.SetNome(nomeUtilizador);
    
    Utilizador destino(idUtilizadorDestino);
    destino.SetNome(nomeUtilizadorDestino);
    
    TarefaDelegada tarefaDelegada( tarefa, origem, destino, publica, aceite);
    
    return tarefaDelegada;
}

vector<string> CSVio::parseCSV(const string & text) {
    stringstream linha(text);
    string campo, tmp;
    vector<string> campos;
    
    while(getline(linha, tmp, CSV_SEP)) {
        if (tmp[tmp.length()-1] == CSV_ESCAPE_CHAR)
                campo += tmp;
        else {
            campo = tmp;
            campos.push_back(campo);
            campo = "";
        }            
    }
    
    //retira os caracteres do fim de linha da ultima string:
    campo = campos.back();
    campos.pop_back();
    campo.erase(campo.find_last_not_of(CSV_ENDLINE) +1);
    campos.push_back(campo);
    
    return campos;
}

