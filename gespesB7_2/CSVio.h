/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              static string toMobileCSVstring(const Tarefa & t, BDados& db)*/
/*              static string toCSVstring(const Tarefa & t)                  */
/*              static Tarefa tarefaFromCSVstring(const string & input)      */
/*              static string toCSVstring(const TarefaDelegada & t)          */
/*              static TarefaDelegada tarefaDelegadaFromCSVstring(           */
/*              const string & input)                                        */
/*              static string encode(const string original)                  */
/*              static string decode(const string original)                  */
/*              static vector<string> parseCSV(const string & text)          */
/*              bool insert(const TarefaDelegada & td)                       */
/*              bool remove(const TarefaDelegada & td)                       */
/*              static bool exportarListaTarefasMobile (                     */
/*                      list<Tarefa*> & tarefas, string file, BDados & db)   */
/*              static bool exportarListaTarefas (                           */
/*                      list<Tarefa*> & tarefas, string file)                */
/*              static bool importarListaTarefas (                           */
/*                      list<Tarefa*> & tarefas, string file)                */
/*              static void exportarReferencia(const Referencia & ref,       */
/*                      string path, BDados * db)                            */
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/



#ifndef CSVIO_H
#define	CSVIO_H

#include "occi.h"
#include "Tarefa.h"
#include "TarefaDelegada.h"
#include "BDDados.h"

#define MOBILE_CSV_SEP ','
#define MOBILE_CSV_ESCAPE_CHAR '\\'
#define MOBILE_CSV_ENDLINE " "
#define MOBILE_CSV_TIMESTAMPFORMAT "RRRRMMDDHH24MISSFF"

#define CSV_SEP ','
#define CSV_ESCAPE_CHAR '\\'
#define CSV_ENDLINE ";"
#define CSV_TIMESTAMPFORMAT "RRRRMMDDHH24MISS.FF"
#define CSV_INTERVALDSFORMAT "SDDD HH:MM:SS"
#define CSV_DATEFORMAT "yyyymmdd"

#define FILE_PATH_UNDEFINED -2
#define FILE_OPEN_ERROR -1

class CSVio {
public:
    CSVio();
    CSVio(string path);
    
    CSVio(const CSVio& orig);
    virtual ~CSVio();
    
    static string toMobileCSVstring(const Tarefa & t, BDados & db);
    
    static string toCSVstring(const Tarefa & t);
    static Tarefa tarefaFromCSVstring(const string & input);
    
    static string toCSVstring(const TarefaDelegada & t);
    static TarefaDelegada tarefaDelegadaFromCSVstring(const string & input);
    
    static string encode(const string original);
    static string decode(const string original);
    static vector<string> parseCSV(const string & text);
    
    
    bool insert(const TarefaDelegada & td);
    bool remove(const TarefaDelegada & td);    
    
    vector<TarefaDelegada> getTarefasDelegadas(string nomeUtilizador);
    
    static bool exportarListaTarefasMobile (list<Tarefa*> & tarefas, string file, BDados & db);
    
    static bool exportarListaTarefas (list<Tarefa*> & tarefas, string file);
    static bool importarListaTarefas (list<Tarefa*> & tarefas, string file);
    
    static void exportarReferencia(const Referencia & ref, string path, BDados * db);

private:
    string path;    
    vector<string> dataLines;
    
    //retorna o numero de linhas lidas do ficheiro. -1 se não conseguiu abrir.
    // -2 se não foi definido o caminho.
    int read ();
    int write();
    
    //bool write(const string &line);

    static string coder(string entrada,string & ori, string & sub);
};

#endif	/* CSVIO_H */

