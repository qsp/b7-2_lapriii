/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              virtual void escreve(ostream &out) const                     */
/*              virtual const AcaoBase& operator = (const AcaoBase &p)       */
/*              bool operator ==(const AcaoBase &a) const                    */
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/


#include "AcaoBase.h"

AcaoBase::AcaoBase(int idUtilizador, string nome, Timestamp criada, Timestamp modificada, Date fim, Date inicio, string prioridade, string estado, list<Anotacao>* anotacoes) {
    this->idUtilizador = idUtilizador;
    this->nome = nome;
    this->criada = criada;
    this->modificada = modificada;
    this->fim = fim;
    this->inicio = inicio;
    this->prioridade = prioridade;
    this->estado = estado;
    this->anotacoes = anotacoes;
}

AcaoBase::AcaoBase(int idUtilizador, string nome, Timestamp criada,
        Timestamp modificada, Date fim, Date inicio, string prioridade,
        string estado) {
    this->idUtilizador = idUtilizador;
    this->nome = nome;
    this->criada = criada;
    this->modificada = modificada;
    this->fim = fim;
    this->inicio = inicio;
    this->prioridade = prioridade;
    this->estado = estado;
}

AcaoBase::AcaoBase(int idUtilizador, const Timestamp criada) {
    this->idUtilizador = idUtilizador;
    this->criada = criada;
}

AcaoBase::AcaoBase(int idUtilizador) {
    this->idUtilizador = idUtilizador;
    this->prioridade = "media";
    this->estado = "inicial";
}

AcaoBase::AcaoBase() {
}

AcaoBase::~AcaoBase() {
}

AcaoBase::AcaoBase(const AcaoBase& a) {
    this->idUtilizador = a.idUtilizador;
    this->nome = a.nome;
    this->criada = a.criada;
    this->modificada = a.modificada;
    this->fim = a.fim;
    this->inicio = a.inicio;
    this->prioridade = a.prioridade;
    this->estado = a.estado;
}

list<Anotacao> * AcaoBase::getAnotacoes() const {
    return anotacoes;
}

Timestamp AcaoBase::getModificada() const {
    return modificada;
}

Timestamp AcaoBase::getCriada() const {
    return criada;
}

void AcaoBase::setEstado(string estado) {
    this->estado = estado;
}

string AcaoBase::getEstado() const {
    return estado;
}

void AcaoBase::setAnotacoes(list<Anotacao> * anotacoes) {
    this->anotacoes = anotacoes;
}

void AcaoBase::setPrioridade(string prioridade) {
    this->prioridade = prioridade;
}

string AcaoBase::getPrioridade() const {
    return prioridade;
}

void AcaoBase::setInicio(Date inicio) {
    this->inicio = inicio;
}

Date AcaoBase::getInicio() const {
    return inicio;
}

void AcaoBase::setFim(Date fim) {
    this->fim = fim;
}

Date AcaoBase::getFim() const {
    return fim;
}

void AcaoBase::setNome(string nome) {
    this->nome = nome;
}

string AcaoBase::getNome() const {
    return nome;
}

int AcaoBase::getIdUtilizador() const {
    return idUtilizador;
};

void AcaoBase::escreve(ostream &out) const {
    out << setw(30) << nome << " | "
            << setw(12) << (inicio.isNull() ? "---" : inicio.toText("dd/mm/yyyy")) << " | "
            << setw(12) << (fim.isNull() ? "---" : fim.toText("dd/mm/yyyy")) << " | "
            << setw(12) << prioridade << " | "
            << setw(12) << estado << " | ";

//            << "|" << (criada.isNull() ? "N/criada" : criada.toText("DD-Mon-RR HH24:MI:SS.FF", 0))
          
}

const AcaoBase& AcaoBase::operator =(const AcaoBase &p) {
    this->idUtilizador = p.idUtilizador;
    this->nome = p.nome;
    this->criada = p.criada;
    this->modificada = p.modificada;
    this->fim = p.fim;
    this->inicio = p.inicio;
    this->prioridade = p.prioridade;
    this->estado = p.estado;
    return *this;
}

bool AcaoBase::operator ==(const AcaoBase &a) const {
    return ( idUtilizador == a.idUtilizador && criada == a.criada);
}

ostream& operator <<(ostream &out, const AcaoBase &p) {
    p.escreve(out);
    return out;
}
