/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              virtual void insere(Tarefa* t,BDados* ligacao)               */
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/

#include "PriorityQueueTarefa.h"
#include "Prioridade.h"
#include "BDDados.h"

PriorityQueueTarefa::PriorityQueueTarefa() : Queue(){}

PriorityQueueTarefa::PriorityQueueTarefa(Comparacao comp, const list<Tarefa*>& tarefas,BDados* ligacao)
{
    this->comp = comp;
    
    for(Tarefa* t : tarefas)
    {
        insere(t,ligacao);
    }
}

PriorityQueueTarefa::PriorityQueueTarefa(const PriorityQueueTarefa& q) : Queue()
{
    comp = q.comp;
}

PriorityQueueTarefa::~PriorityQueueTarefa(){}

void PriorityQueueTarefa::insere(Tarefa* t,BDados* ligacao)
{
    Queue<Tarefa*> tmp = *this;
    Tarefa* elem;
    
    while(!vazia())
        retira(elem);
    
    while(!tmp.vazia() && maiorOuIgual(tmp.frente(),t,ligacao))
    {
        tmp.retira(elem);
        Queue<Tarefa*>::insere(elem);
    }
    
    Queue<Tarefa*>::insere(t);
    
    while(!tmp.vazia())
    {
        tmp.retira(elem);
        Queue::insere(elem);
    }
}

bool PriorityQueueTarefa::maiorOuIgual(Tarefa* maior, Tarefa* menor,BDados* ligacao)
{
    switch(comp)
    {
        case prioridade:
            return (ligacao->prioridadeToIdPrioridade(maior->getPrioridade())) >= (ligacao->prioridadeToIdPrioridade(menor->getPrioridade()));
            break;
        case estado:
            return (ligacao->estadoToIdEstado(maior->getEstado())) >= (ligacao->estadoToIdEstado(menor->getEstado()));
            break;
        case dataFim:
            return maior->getFim() >= menor->getFim();
            break;
        default:
            return maior->getCriada() >= menor->getCriada();
    }
}
