/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              list <Utilizador> listaUtilizadores()                        */
/*              bool le(Utilizador & uti)                                    */
/*              bool adiciona(Utilizador & uti)                              */
/*              bool atualiza(const Utilizador & uti)                        */
/*              bool elimina(const Utilizador & uti)                         */
/*              bool existeUtilizadorPorNomePassword(Utilizador & uti)       */
/*              list <Tarefa*> listaTarefasDeUtilizador(                     */
/*                      const Utilizador & uti)                              */
/*              list <Tarefa*> listaTarefasDelegadasAoUtilizador(            */
/*                      const Utilizador & uti,char aceite)                  */
/*              list<Tarefa*> listaTarefasPorContexto(                       */
/*                      const Utilizador& uti, string contexto)              */
/*              list <Tarefa*> listaTarefasPorContextos(                     */
/*                      const Utilizador& uti,list<Contexto> contextos)      */
/*              list<Tarefa> listarTarefasDeProjeto(Projeto & p)             */
/*              list<Tarefa> listarTarefasPorNome(string nome)               */
/*              bool limparContextosDaTarefa(list<Tarefa*> tarefas)          */
/*              bool atualiza(Tarefa & t)                                    */
/*              bool atualiza(Tarefa & t)                                    */
/*              bool le(Tarefa & t)                                          */
/*              bool adiciona(Tarefa & t)                                    */
/*              bool elimina(const Tarefa & t)                               */
/*              bool adiciona(TarefaDelegada & t)                            */
/*              bool atualiza(TarefaDelegada & t)                            */
/*              bool le(TarefaDelegada & t)                                  */
/*              bool elimina(TarefaDelegada & t)                             */
/*              bool adicionaDependenciaTarefa(Tarefa & t, Tarefa & tDep)    */
/*              bool eliminaDependenciaTarefa(Tarefa & t, Tarefa & tDep)     */
/*              ListAdjGrafo<Tarefa, int> criaGrafodependenciaTarefas(       */
/*                      Utilizador u)                                        */
/*              ListAdjGrafo<Tarefa, int> criaGrafodependenciaTarefas(       */
/*                      Projeto j)                                           */
/*              bool adiciona(Contexto & c)                                  */
/*              bool le(Contexto & c)                                        */
/*              bool atualiza(const Contexto & c)                            */
/*              bool desligarContexto(const Contexto& c)                     */
/*              bool elimina(const Contexto & c)                             */
/*              bool adiciona(const Tarefa & t,const Contexto & c)           */
/*              list<Contexto> listaContextosDeTarefa(const Tarefa& t)       */
/*              list<Contexto> listaContextosDeUtilizador(                   */
/*                      const Utilizador& uti)                               */
/*              list<Anotacao> listaAnotacoesDeAcaoBase(AcaoBase & t)const   */
/*              bool adicionaAnotacaoPorFicheiro(AcaoBase b, string fileName,*/
/*                      string tipoAno)                                      */
/*              bool adicionaAnotacaoPorTexto(AcaoBase b, string nome,       */
/*                      string tipoAno)                                      */
/*              bool elimina(Anotacao &a)                                    */
/*              list<Anotacao>listaAnotacoesDeReferencia(                    */
/*                      const Referencia & t)const                           */
/*              bool le(Projeto & projeto)                                   */
/*              list<Projeto>  listarProjetosDeUtilizador(Utilizador uti)    */
/*              bool transformarTarefaProjeto(Tarefa & t,Projeto & p)        */
/*              bool adicionarTarefaAProjeto(Tarefa t, Projeto p)            */
/*              bool removerTarefaDeProjeto(Tarefa t, Projeto p)             */
/*              Projeto * projetoDaTarefa(const Tarefa & t)                  */
/*              bool atualiza(Projeto & t)                                   */
/*              bool elimina(const Projeto & t)                              */
/*              bool transformarTarefaReferencia(Tarefa & t, Referencia &p)  */
/*              list<Referencia> listaReferenciasDeUtilizador(Utilizador u)  */
/*              bool elimina(Referencia & r)                                 */
/*              Timestamp agora()                                            */
/*              vector<string> listaPrioridades()                            */
/*              vector<string> listaEstados()                                */
/*              int tipoAnotacaoToIdTipoAnotacao(string p)                   */
/*              int prioridadeToIdPrioridade(string p)                       */
/*              int estadoToIdEstado(string p)                               */
/*              string idUtilizadorToNome(int idUtilizador)                  */
/*              int nomeUtilizadorToId(string const nome)                    */
/*              int contextoToIdContexto(string c)                           */
/*              vector<string> listaTipoAnotacao()                           */
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/
#include "BDDados.h"
#include "AcaoBase.h"
#include "TarefaDelegada.h"
#include "CSVio.h"
#include "API.h"
#include <fstream>
#include <sstream>

BDados::BDados(std::string user, std::string passwd, std::string db, string tablePrefix) {
    env = Environment::createEnvironment(Environment::DEFAULT);
    ligacao = env->createConnection(user, passwd, db);
    commit = true;
    this->tablePrefix = tablePrefix;
}

BDados::~BDados() {
    env->terminateConnection(ligacao);
    Environment::terminateEnvironment(env);
    commit = true;
}


// Funções relativas à classe Utilizador

std::list <Utilizador> BDados::listaUtilizadores() {
    Statement *instrucao;
    std::list <Utilizador> ret;

    instrucao = ligacao->createStatement("SELECT * FROM " + tablePrefix + "UTILIZADOR");
    ResultSet *rset = instrucao->executeQuery();
    while (rset->next()) {
        Utilizador c(rset->getInt(1), rset->getString(4), rset->getInt(3), rset->getString(2));
        ret.push_back(c);
    }
    instrucao->closeResultSet(rset);
    return ret;
}

bool BDados::existeUtilizadorPorNomePassword(Utilizador & uti) {
    Statement *instrucao;

    instrucao = ligacao->createStatement("SELECT * FROM " + tablePrefix + "UTILIZADOR WHERE NOME= :NOME and PASSWORD = :PASSWORD");
    instrucao->setString(1, uti.GetNome());
    instrucao->setString(2, uti.GetPassword());
    ResultSet *rset = instrucao->executeQuery();
    if (rset->next()) {
        uti.SetIdUtilizador(rset->getInt(1));
        instrucao->closeResultSet(rset);
        return true;
    } else {
        instrucao->closeResultSet(rset);
        return false;
    }
}

bool BDados::existeUtilizadorPorNome(Utilizador & uti) {
    Statement *instrucao;
    instrucao = ligacao->createStatement("SELECT * FROM " + tablePrefix + "UTILIZADOR WHERE NOME= :NOME");
    instrucao->setString(1, uti.GetNome());
    ResultSet *rset = instrucao->executeQuery();
    if (rset->next()) {
        instrucao->closeResultSet(rset);
        return true;
    } else {
        instrucao->closeResultSet(rset);
        return false;
    }
}

bool BDados::le(Utilizador & uti) {
    Statement *instrucao;
    string sqlStmt = "SELECT * FROM " + tablePrefix + "UTILIZADOR WHERE ID_UTILIZADOR = :ID_UTILIZADOR";
    instrucao = ligacao->createStatement(sqlStmt);
    instrucao->setInt(1, uti.GetIdUtilizador());
    ResultSet *rset = instrucao->executeQuery();
    try {
        rset->next();
        uti.SetNome(rset->getString(4));
        uti.SetPassword(rset->getString(2));
        uti.SetCodigo(rset->getInt(3));
    } catch (SQLException ex) {
        cout << "Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return false;
    }
    ligacao->terminateStatement(instrucao);
    return true;
}

bool BDados::adiciona(Utilizador & uti) {
    Statement *instrucao;
    string sqlStmt = "INSERT INTO " + tablePrefix + "UTILIZADOR (nome, password, codigo ) VALUES (:nome,:password,:codigo)";
    instrucao = ligacao->createStatement(sqlStmt);
    try {
        instrucao->setString(1, uti.GetNome());
        instrucao->setString(2, uti.GetPassword());
        instrucao->setInt(3, uti.GetCodigo());
        instrucao->executeUpdate();
        instrucao = ligacao->createStatement("SELECT " + tablePrefix + "SQ_UTILIZADOR.CURRVAL FROM DUAL");
        ResultSet *rset = instrucao->executeQuery();
        rset->next();
        uti.SetIdUtilizador(rset->getInt(1));
        instrucao->closeResultSet(rset);
    } catch (SQLException ex) {
        return false;
    }
    ligacao->terminateStatement(instrucao);
    if (commit) ligacao->commit();
    return true;
}

bool BDados::atualiza(const Utilizador & uti) {
    Statement *instrucao;
    string sqlStmt = "UPDATE " + tablePrefix + "Utilizador SET nome = :nome, password = :password, codigo = :codigo WHERE id_utilizador = :id_utilizador";
    instrucao = ligacao->createStatement(sqlStmt);
    try {
        instrucao->setString(1, uti.GetNome());
        instrucao->setString(2, uti.GetPassword());
        instrucao->setInt(3, uti.GetCodigo());
        instrucao->setInt(4, uti.GetIdUtilizador());
        instrucao->executeUpdate();
    } catch (SQLException ex) {
        cout << "Exception thrown for updateRow" << endl;
        cout << "Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return false;
    }
    ligacao->terminateStatement(instrucao);
    return true;
}

bool BDados::elimina(const Utilizador & uti) {
    Statement *instrucao;
    string sqlStmt = "DELETE FROM " + tablePrefix + "Utilizador WHERE id_utilizador = :id_utilizador";
    instrucao = ligacao->createStatement(sqlStmt);
    try {
        instrucao->setInt(1, uti.GetIdUtilizador());
        instrucao->executeUpdate();
    } catch (SQLException ex) {
        cout << "Exception thrown for updateRow" << endl;
        cout << "Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return false;
    }
    ligacao->terminateStatement(instrucao);
    if (commit) ligacao->commit();
    return true;
}

//Funções relativas a classe Tarefas

list <Tarefa*> BDados::listaTarefasDeUtilizador(const Utilizador & uti) {
    Statement *instrucao;
    std::list <Tarefa*> ret;
    instrucao = ligacao->createStatement("SELECT * FROM " + tablePrefix + "TAREFA_COMPLETA WHERE id_utilizador = :id_utilizador "
            " AND PRIORIDADE != 'qqdia' AND ID_UTILIZADOR NOT IN (SELECT ID_UTILIZADOR FROM " + tablePrefix + "TAREFA_DELEGADA WHERE " + tablePrefix + "TAREFA_DELEGADA.TAREFA_CRIADA = " + tablePrefix + "TAREFA_COMPLETA.CRIADA)");
    instrucao->setInt(1, uti.GetIdUtilizador());
    ResultSet *rset = instrucao->executeQuery();
    while (rset->next()) {
        Tarefa* ab = new Tarefa(rset->getInt(1), rset->getString(3), rset->getTimestamp(2),
                rset->getTimestamp(5), rset->getDate(10), rset->getDate(4),
                rset->getString(7), rset->getString(9), rset->getIntervalDS(11),
                rset->getIntervalDS(12));
        ret.push_back(ab);
    }
    instrucao->closeResultSet(rset);
    std::list<Tarefa*> tmp = listaTarefasDelegadasAoUtilizador(uti, ACEITE_TRUE);
    std::list<Tarefa*>::iterator it = ret.end();
    std::list<Tarefa*>::iterator it1 = tmp.begin();
    std::list<Tarefa*>::iterator it2 = tmp.end();
    ret.insert(it, it1, it2);
    return ret;
}

list <Tarefa*> BDados::listaTarefasDeUtilizadorQQDia(const Utilizador & uti) {
    Statement *instrucao;
    std::list <Tarefa*> ret;
    instrucao = ligacao->createStatement("SELECT * FROM " + tablePrefix + "TAREFA_COMPLETA WHERE id_utilizador = :id_utilizador "
            "and prioridade = 'qqdia' AND ID_UTILIZADOR NOT IN (SELECT ID_UTILIZADOR FROM " + tablePrefix + "TAREFA_DELEGADA WHERE " + tablePrefix + "TAREFA_DELEGADA.TAREFA_CRIADA = " + tablePrefix + "TAREFA_COMPLETA.CRIADA)");
    instrucao->setInt(1, uti.GetIdUtilizador());
    ResultSet *rset = instrucao->executeQuery();
    while (rset->next()) {
        Tarefa* ab = new Tarefa(rset->getInt(1), rset->getString(3), rset->getTimestamp(2),
                rset->getTimestamp(5), rset->getDate(10), rset->getDate(4),
                rset->getString(7), rset->getString(9), rset->getIntervalDS(11),
                rset->getIntervalDS(12));
        ret.push_back(ab);
    }
    instrucao->closeResultSet(rset);
    std::list<Tarefa*> tmp = listaTarefasDelegadasAoUtilizador(uti, ACEITE_TRUE);
    std::list<Tarefa*>::iterator it = ret.end();
    std::list<Tarefa*>::iterator it1 = tmp.begin();
    std::list<Tarefa*>::iterator it2 = tmp.end();
    ret.insert(it, it1, it2);
    return ret;
}

std::list<Tarefa*> BDados::listaTarefasPorContexto(const Utilizador& uti, string contexto) {
    int idContexto = contextoToIdContexto(contexto);
    list<Tarefa*> tarefas;
    Statement* instrucao;
    string sqlStatement = "select * from " + tablePrefix + "tarefa_completa "
            "join contextos_da_tarefa on tarefa_completa.criada = contextos_da_tarefa.tarefa_criada "
            "and contextos_da_tarefa.id_utilizador = tarefa_completa.id_utilizador "
            "and tarefa_completa.id_utilizador = :1 "
            "where contexto_id_contexto = :2";
    instrucao = ligacao->createStatement(sqlStatement);
    ResultSet* res;
    try {
        instrucao->setInt(1, uti.GetIdUtilizador());
        instrucao->setInt(2, idContexto);
        res = instrucao->executeQuery();
        while (res->next()) {
            Tarefa* t = new Tarefa(res->getInt(1), res->getString(3), res->getTimestamp(2), res->getTimestamp(5), res->getDate(10),
                    res->getDate(4), res->getString(7), res->getString(9),
                    res->getIntervalDS(11), res->getIntervalDS(12));
            tarefas.push_back(t);
        }
    } catch (SQLException ex) {
        cout << "Lista tarefas por contexto error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return tarefas;
    }
    instrucao->closeResultSet(res);
    ligacao->terminateStatement(instrucao);
    return tarefas;
}

std::list<Tarefa*> BDados::listaTarefasPorContextos(const Utilizador& uti, list<Contexto> contextos) {
    Statement* instrucao;
    list<Tarefa*> lista;
    ResultSet* res;
    string sqlStatement = "SELECT DISTINCT t.id_utilizador,t.criada,t.nome,t.modificada,t.data_fim,t.data_inicio,t.prioridade,t.estado,t.duracao_efetiva,t.duracao_estimada "
            "FROM " + tablePrefix + "TAREFA_COMPLETA t "
            "join " + tablePrefix + "contextos_da_tarefa ct on t.id_utilizador = ct.id_utilizador "
            "and t.criada = ct.tarefa_criada "
            "where (t.id_estado != 2 and t.id_prioridade != 1)";

    int i = 0;
    for (Contexto c : contextos) {
        i++;
        stringstream ss;
        if (i == 1)
            ss << " and (ct.contexto_id_contexto = :" << i;
        else
            ss << " or ct.contexto_id_contexto = :" << i;
        ss << ")";
        sqlStatement.append(ss.str());
    }

    instrucao = ligacao->createStatement(sqlStatement);

    try {
        unsigned int j = 0;
        for (Contexto c : contextos) {
            j++;
            instrucao->setInt(j, c.getIdContexto());
        }
        res = instrucao->executeQuery();
        while (res->next()) {
            Tarefa* t = new Tarefa(res->getInt(1), res->getString(3), res->getTimestamp(2),
                    res->getTimestamp(4), res->getDate(5), res->getDate(6),
                    res->getString(7), res->getString(8), res->getIntervalDS(9),
                    res->getIntervalDS(10));
            lista.push_back(t);
        }
    } catch (SQLException ex) {
        cout << "Lista tarefas por contextos error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return lista;
    }
    instrucao->closeResultSet(res);
    ligacao->terminateStatement(instrucao);
    return lista;
}

std::list<Tarefa*> BDados::listaTarefasDelegadasPorUtilizador(const Utilizador& uti)
{
    Statement* instrucao;
    std::list<Tarefa*> lista;
    instrucao = ligacao->createStatement("SELECT * FROM " + tablePrefix + "TAREFA_DELEGADA WHERE ID_UTILIZADOR = :1");
    instrucao->setInt(1, uti.GetIdUtilizador());
    ResultSet* res;
    try {
        res = instrucao->executeQuery();
        while (res->next()) {
            Tarefa t(uti.GetIdUtilizador(), res->getTimestamp(1));
            le(t);
            Tarefa* td = new TarefaDelegada(t, res->getInt(2), res->getString(5)[0], res->getString(4)[0]);
            ((TarefaDelegada*) td)->setNomeUtilizador(idUtilizadorToNome(t.getIdUtilizador()));
            ((TarefaDelegada*) td)->setNomeUtilizadorDestino(idUtilizadorToNome(res->getInt(2)));
            lista.push_back(td);
        }
    } catch (SQLException ex) {
        cout << "Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return lista;
    }
    instrucao->closeResultSet(res);
    ligacao->terminateStatement(instrucao);
    return lista;
}

std::list<Tarefa*> BDados::listaTarefasDelegadasAoUtilizador(const Utilizador& uti, char aceite) {
    Statement* instrucao;
    std::list<Tarefa*> lista;
    instrucao = ligacao->createStatement("SELECT * FROM " + tablePrefix + "TAREFA_DELEGADA WHERE ID_UTILIZADOR_DESTINO = :ID_UTI AND ACEITE = :aceite");
    instrucao->setInt(1, uti.GetIdUtilizador());
    string a = string(1, aceite);
    instrucao->setString(2, a);
    ResultSet* res;
    try {
        res = instrucao->executeQuery();
        while (res->next()) {
            Tarefa t(res->getInt(2), res->getTimestamp(1));
            le(t);
            Tarefa* td = new TarefaDelegada(t, uti.GetIdUtilizador(), res->getString(5)[0], res->getString(4)[0]);
            ((TarefaDelegada*) td)->setNomeUtilizador(idUtilizadorToNome(t.getIdUtilizador()));
            ((TarefaDelegada*) td)->setNomeUtilizadorDestino(idUtilizadorToNome(uti.GetIdUtilizador()));
            lista.push_back(td);
        }
    } catch (SQLException ex) {
        cout << "Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return lista;
    }
    instrucao->closeResultSet(res);
    ligacao->terminateStatement(instrucao);
    return lista;
}

TarefaDelegada BDados::getTarefaDelegadaPor(Utilizador & u, Tarefa & t) {
    Statement* instrucao;
    Tarefa nula(0);
    TarefaDelegada td(nula, 0, 0, 0);
    instrucao = ligacao->createStatement(
            "SELECT * FROM " + tablePrefix + "TAREFA_DELEGADA"
            " WHERE ID_UTILIZADOR = :1 AND TAREFA_CRIADA = :2");
    instrucao->setInt(1, u.GetIdUtilizador());
    instrucao->setTimestamp(2, t.getCriada());    
    ResultSet* res;
    try {
        res = instrucao->executeQuery();
         if(res->next()) {        
            Utilizador origem(t.getIdUtilizador());
            le(origem);
            Utilizador destino(res->getInt(3));
            le(destino);

            td = TarefaDelegada(t, origem, destino, res->getString(5)[0], res->getString(4)[0]);        
         }
    } catch (SQLException ex) {
        cout << "Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
    }    
    
    instrucao->closeResultSet(res);
    ligacao->terminateStatement(instrucao);
    return TarefaDelegada(td);
}

TarefaDelegada BDados::getTarefaDelegadaPara(Utilizador & u, Tarefa & t) {
    Statement* instrucao;
    Tarefa nula(0);
    TarefaDelegada td(nula, 0, 0, 0);
    instrucao = ligacao->createStatement(
            "SELECT * FROM " + tablePrefix + "TAREFA_DELEGADA"
            " WHERE ID_UTILIZADOR = :1 AND TAREFA_CRIADA = :2 AND ID_UTILIZADOR_DESTINO = :3");
    instrucao->setInt(1, t.getIdUtilizador());
    instrucao->setTimestamp(2, t.getCriada());
    instrucao->setInt(3, u.GetIdUtilizador());
    ResultSet* res;
    try {
        res = instrucao->executeQuery();
        if(res->next()) {        
            Utilizador origem(t.getIdUtilizador());
            le(origem);
            Utilizador destino(res->getInt(3));
            le(destino);

            td = TarefaDelegada(t, origem, destino, res->getString(5)[0], res->getString(4)[0]);        
        }
    } catch (SQLException ex) {
        cout << "Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
    }    
    
    instrucao->closeResultSet(res);
    ligacao->terminateStatement(instrucao);
    return TarefaDelegada(td);
}

list<Tarefa> BDados::listarTarefasPorNome(string nome) {

    Statement *instrucao;
    std::list <Tarefa> ret;
    instrucao = ligacao->createStatement("SELECT * FROM " + tablePrefix + "TAREFA_COMPLETA WHERE nome=:nome");
    instrucao->setString(1, nome);
    ResultSet *rset = instrucao->executeQuery();
    while (rset->next()) {
        Tarefa ab(rset->getInt(1), rset->getString(3), rset->getTimestamp(2),
                rset->getTimestamp(5), rset->getDate(10), rset->getDate(4),
                rset->getString(7), rset->getString(9), rset->getIntervalDS(11),
                rset->getIntervalDS(12));
        ret.push_back(ab);
    }
    return ret;
}

bool BDados::limparContextosDaTarefa(list<Tarefa*> tarefas) {
    Statement* instrucao;
    string sqlStatement = "DELETE FROM " + tablePrefix + "CONTEXTOS_DA_TAREFA WHERE";
    int i = 0;
    for (Tarefa* t : tarefas) {
        stringstream ss;
        if (i == 0) {
            ss << " (ID_UTILIZADOR = :" << ++i;
            ss << " AND TAREFA_CRIADA = :" << ++i << ")";
        } else {
            ss << " OR (ID_UTILIZADOR = :" << ++i;
            ss << " AND TAREFA_CRIADA = :" << ++i << ")";
        }
        sqlStatement.append(ss.str());
    }

    instrucao = ligacao->createStatement(sqlStatement);

    try {
        unsigned int j = 0;
        for (Tarefa* t : tarefas) {
            instrucao->setInt(++j, t->getIdUtilizador());
            instrucao->setTimestamp(++j, t->getCriada());
        }

        instrucao->executeUpdate();
    } catch (SQLException ex) {
        cout << "Limpar contextos da tarefa error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        ligacao->rollback();
        return false;
    }
    ligacao->commit();
    ligacao->terminateStatement(instrucao);
    return true;
}

bool BDados::le(Tarefa & t) {
    Statement *instrucao;
    string sqlStmt = "SELECT * FROM " + tablePrefix + "TAREFA_COMPLETA WHERE ID_UTILIZADOR = :ID_UTILIZADOR AND criada= :criada";
    instrucao = ligacao->createStatement(sqlStmt);
    instrucao->setInt(1, t.idUtilizador);
    instrucao->setTimestamp(2, t.criada);
    ResultSet *rset = instrucao->executeQuery();
    try {
        rset->next();
        Tarefa ab(rset->getInt(1), rset->getString(3), rset->getTimestamp(2),
                rset->getTimestamp(5), rset->getDate(10), rset->getDate(4),
                rset->getString(7), rset->getString(9), rset->getIntervalDS(11),
                rset->getIntervalDS(12));
        t = ab;
    } catch (SQLException ex) {
        cout << "Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return false;
    }
    ligacao->terminateStatement(instrucao);
    return true;
}

bool BDados::le(Projeto & projeto) {
    Statement *instrucao;
    string sqlStmt = "SELECT * FROM " + tablePrefix + "PROJETO_COMPLETO WHERE ID_UTILIZADOR = :ID_UTILIZADOR AND criada= :criada";
    instrucao = ligacao->createStatement(sqlStmt);
    instrucao->setInt(1, projeto.idUtilizador);
    instrucao->setTimestamp(2, projeto.criada);
    ResultSet *rset = instrucao->executeQuery();
    try {
        rset->next();
        projeto.setNome(rset->getString(3));
        projeto.setInicio(rset->getDate(4));
        projeto.setFim(rset->getDate(5));
        projeto.setPrioridade(rset->getString(10));
        projeto.setEstado(rset->getString(9));
    } catch (SQLException ex) {
        cout << "Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return false;
    }
    ligacao->terminateStatement(instrucao);
    return true;
}

vector<string> BDados::listaPrioridades() {
    Statement *instrucao;
    vector<string> prioridades;
    string sqlStmt = "SELECT DESCRICAO FROM PRIORIDADE ORDER BY ID_PRIORIDADE";
    instrucao = ligacao->createStatement(sqlStmt);
    try {
        ResultSet *rset = instrucao->executeQuery();
        while (rset->next()) {
            prioridades.push_back(rset->getString(1));
        }
    } catch (SQLException ex) {
        cout << "Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return prioridades;
    }
    ligacao->terminateStatement(instrucao);
    return prioridades;
}

vector<string> BDados::listaEstados() {
    Statement *instrucao;
    vector<string> estados;
    string sqlStmt = "SELECT DESCRICAO FROM ESTADO ORDER BY ID_ESTADO";
    instrucao = ligacao->createStatement(sqlStmt);
    try {
        ResultSet *rset = instrucao->executeQuery();
        while (rset->next()) {
            estados.push_back(rset->getString(1));
        }
    } catch (SQLException ex) {
        cout << "Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return estados;
    }
    ligacao->terminateStatement(instrucao);
    return estados;
}

vector<string> BDados::listaTipoAnotacao() {
    Statement *instrucao;
    vector<string> tipoAnotacao;
    string sqlStmt = "SELECT DESCRICAO FROM TIPO_ANOTACAO ORDER BY ID_TIPO_ANOTACAO";
    instrucao = ligacao->createStatement(sqlStmt);
    try {
        ResultSet *rset = instrucao->executeQuery();
        while (rset->next()) {
            tipoAnotacao.push_back(rset->getString(1));
        }
    } catch (SQLException ex) {
        cout << "Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return tipoAnotacao;
    }
    ligacao->terminateStatement(instrucao);
    return tipoAnotacao;
}

int BDados::prioridadeToIdPrioridade(string p) {
    Statement *instrucao;
    string sqlStmt = "SELECT ID_PRIORIDADE FROM PRIORIDADE WHERE DESCRICAO = :DESCRICAO";
    instrucao = ligacao->createStatement(sqlStmt);
    instrucao->setString(1, p);
    ResultSet *rset = instrucao->executeQuery();
    int idPrioridade;
    try {
        rset->next();
        idPrioridade = rset->getInt(1);
    } catch (SQLException ex) {
        cout << "Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return -1;
    }
    ligacao->terminateStatement(instrucao);
    return idPrioridade;
}

Timestamp BDados::agora() {
    Statement *instrucao;
    string sqlStmt = "select current_timestamp from dual";
    instrucao = ligacao->createStatement(sqlStmt);
    ResultSet *rset = instrucao->executeQuery();
    Timestamp t;
    try {
        rset->next();
        t = rset->getTimestamp(1);
    } catch (SQLException ex) {
        cout << "Agora - Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return NULL;
    }
    ligacao->terminateStatement(instrucao);
    return t;

}

void BDados::setLigacao(Connection* ligacao) {
    this->ligacao = ligacao;
}

Connection* BDados::getLigacao() const {
    return ligacao;
}

int BDados::estadoToIdEstado(string p) {
    Statement *instrucao;
    string sqlStmt = "SELECT ID_ESTADO FROM ESTADO WHERE DESCRICAO = :DESCRICAO";
    instrucao = ligacao->createStatement(sqlStmt);
    instrucao->setString(1, p);
    ResultSet *rset = instrucao->executeQuery();
    int idEstado;
    try {
        rset->next();
        idEstado = rset->getInt(1);
    } catch (SQLException ex) {
        cout << "Error number EstadoToIdEstado: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return -1;
    }
    ligacao->terminateStatement(instrucao);
    return idEstado;
}

int BDados::tipoAnotacaoToIdTipoAnotacao(string p) {
    Statement *instrucao;
    string sqlStmt = "SELECT ID_TIPO_ANOTACAO FROM TIPO_ANOTACAO WHERE DESCRICAO = :DESCRICAO";
    instrucao = ligacao->createStatement(sqlStmt);
    instrucao->setString(1, p);
    ResultSet *rset = instrucao->executeQuery();
    int idTipoAnotacao;
    try {
        rset->next();
        idTipoAnotacao = rset->getInt(1);
    } catch (SQLException ex) {
        cout << "Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return -1;
    }
    ligacao->terminateStatement(instrucao);
    return idTipoAnotacao;
}

bool BDados::adiciona(Tarefa & t) {
    Statement *iAcaoBase, *iTarefa;
    if (t.criada.isNull())
        t.criada = agora();
    t.modificada = t.criada;
    string sqlStmtAcaoBase = "INSERT INTO " + tablePrefix + "ACCAO_BASE (ID_UTILIZADOR, CRIADA, NOME,"
            "DATA_INICIO, DATA_FIM, ID_PRIORIDADE, ID_ESTADO ) "
            "VALUES (:ID_UTILIZADOR, :CRIADA, :NOME, "
            ":DATA_INICIO, :DATA_FIM, :ID_PRIORIDADE, :ID_ESTADO)";
    iAcaoBase = ligacao->createStatement(sqlStmtAcaoBase);
    try {
        iAcaoBase->setInt(1, t.idUtilizador);
        iAcaoBase->setTimestamp(2, t.criada);
        iAcaoBase->setString(3, t.nome);
        iAcaoBase->setDate(4, t.inicio);
        iAcaoBase->setDate(5, t.fim);
        iAcaoBase->setInt(6, prioridadeToIdPrioridade(t.prioridade));
        iAcaoBase->setInt(7, estadoToIdEstado(t.estado));
        iAcaoBase->executeUpdate();
    } catch (SQLException ex) {
        cout << "Acao Base Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return false;
    }
    ligacao->terminateStatement(iAcaoBase);

    string sqlStmtTarefa = "INSERT INTO " + tablePrefix + "TAREFA (ID_UTILIZADOR, CRIADA, DURACAO_EFETIVA, DURACAO_ESTIMADA) "
            "VALUES (:ID_UTILIZADOR, :CRIADA,:DURACAO_EFETIVA,:DURACAO_ESTIMADA)";
    iTarefa = ligacao->createStatement(sqlStmtTarefa);
    try {
        iTarefa->setInt(1, t.idUtilizador);
        iTarefa->setTimestamp(2, t.criada);
        iTarefa->setIntervalDS(3, t.duracaoEfetiva);
        iTarefa->setIntervalDS(4, t.duracaoEstimada);
        iTarefa->executeUpdate();
    } catch (SQLException ex) {
        cout << "Tarefa Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        ligacao->rollback();
        return false;
    }
    ligacao->terminateStatement(iTarefa);
    if (commit) ligacao->commit();
    return true;
}

bool BDados::atualiza(Tarefa & t) {
    Statement *iAcaoBase, *iTarefa;
    string sqlStmtAcaoBase = "UPDATE " + tablePrefix + "ACCAO_BASE SET NOME = :NOME, "
            "DATA_INICIO = :DATA_INICIO, DATA_FIM = :DATA_FIM,"
            " ID_PRIORIDADE = :ID_PRIORIDADE , ID_ESTADO = :ID_ESTADO"
            " WHERE ID_UTILIZADOR = :ID_UTILIZADOR AND CRIADA = :CRIADA ";
    iAcaoBase = ligacao->createStatement(sqlStmtAcaoBase);
    try {
        iAcaoBase->setString(1, t.nome);
        iAcaoBase->setDate(2, t.inicio);
        iAcaoBase->setDate(3, t.fim);
        iAcaoBase->setInt(4, prioridadeToIdPrioridade(t.prioridade));
        iAcaoBase->setInt(5, estadoToIdEstado(t.estado));
        iAcaoBase->setInt(6, t.idUtilizador);
        iAcaoBase->setTimestamp(7, t.criada);
        iAcaoBase->executeUpdate();
    } catch (SQLException ex) {
        cout << "Acao Base Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return false;
    }
    ligacao->terminateStatement(iAcaoBase);

    string sqlStmtTarefa = "UPDATE " + tablePrefix + "TAREFA SET DURACAO_EFETIVA = :DURACAO_EFETIVA, "
            "DURACAO_ESTIMADA= :DURACAO_ESTIMADA"
            " WHERE ID_UTILIZADOR= :ID_UTILIZADOR AND CRIADA= :CRIADA";
    iTarefa = ligacao->createStatement(sqlStmtTarefa);
    try {
        iTarefa->setInt(3, t.idUtilizador);
        iTarefa->setTimestamp(4, t.criada);
        iTarefa->setIntervalDS(1, t.duracaoEfetiva);
        iTarefa->setIntervalDS(2, t.duracaoEstimada);
        iTarefa->executeUpdate();
    } catch (SQLException ex) {
        cout << "Tarefa Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        ligacao->rollback();
        return false;
    }
    ligacao->terminateStatement(iTarefa);
    if (commit) ligacao->commit();
    return true;
}

bool BDados::elimina(const Tarefa & t) {
    Statement *iAcaoBase, *iTarefa, *iContextoTarefa;

    string sqlStmtTarefaContexto = "DELETE FROM " + tablePrefix + "CONTEXTOS_DA_TAREFA WHERE ID_UTILIZADOR= :ID_UTILIZADOR AND tAREFA_CRIADA= :CRIADA";
    iContextoTarefa = ligacao->createStatement(sqlStmtTarefaContexto);
    try {
        iContextoTarefa->setInt(1, t.idUtilizador);
        iContextoTarefa->setTimestamp(2, t.criada);
        iContextoTarefa->executeUpdate();
    } catch (SQLException ex) {
        cout << "Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        ligacao->rollback();
        return false;
    }
    ligacao->terminateStatement(iContextoTarefa);

    string sqlStmtTarefa = "DELETE FROM " + tablePrefix + "TAREFA WHERE ID_UTILIZADOR= :ID_UTILIZADOR AND CRIADA= :CRIADA";
    iTarefa = ligacao->createStatement(sqlStmtTarefa);
    try {
        iTarefa->setInt(1, t.idUtilizador);
        iTarefa->setTimestamp(2, t.criada);
        iTarefa->executeUpdate();
    } catch (SQLException ex) {
        cout << "Tarefa Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return false;
    }
    ligacao->terminateStatement(iTarefa);

    string sqlStmtAcaoBase = "DELETE FROM " + tablePrefix + "ACCAO_BASE WHERE ID_UTILIZADOR = :ID_UTILIZADOR AND CRIADA = :CRIADA";
    iAcaoBase = ligacao->createStatement(sqlStmtAcaoBase);
    try {
        iAcaoBase->setInt(1, t.idUtilizador);
        iAcaoBase->setTimestamp(2, t.criada);
        iAcaoBase->executeUpdate();
    } catch (SQLException ex) {
        cout << "Acao Base Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        ligacao->rollback();
        return false;
    }
    ligacao->terminateStatement(iAcaoBase);
    if (commit) ligacao->commit();
    return true;
}

bool BDados::adiciona(TarefaDelegada& t) {
    Statement* instrucao;
    string sqlStatement = "INSERT INTO " + tablePrefix + "TAREFA_DELEGADA (TAREFA_CRIADA,ID_UTILIZADOR,ID_UTILIZADOR_DESTINO,ACEITE,PUBLICA) "
            "VALUES(:1,:2,:3,:4,:5)";
    instrucao = ligacao->createStatement(sqlStatement);
    try {
        instrucao->setTimestamp(1, t.getCriada());
        instrucao->setInt(2, t.getIdUtilizador());
        instrucao->setInt(3, t.getIdUtilizadorDestino());
        string a(1, t.getAceiteChar());
        instrucao->setString(4, a);
        string b(1, t.getPublicaChar());
        instrucao->setString(5, b);
        instrucao->executeUpdate();
    } catch (SQLException ex) {
        cout << "Adicionar Tarefa Delegada Error: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        ligacao->rollback();
        return false;
    }
    ligacao->terminateStatement(instrucao);
    ligacao->commit();
    return true;
}

bool BDados::atualiza(TarefaDelegada& t) {
    Statement* instrucao;

    string sqlStatement = "UPDATE " + tablePrefix + "TAREFA_DELEGADA SET ACEITE = :1, PUBLICA = :2 "
            "WHERE TAREFA_CRIADA = :3 AND ID_UTILIZADOR = :4 AND ID_UTILIZADOR_DESTINO = :5";

    instrucao = ligacao->createStatement(sqlStatement);

    try {
        string a(1, t.getAceiteChar());
        instrucao->setString(1, a);
        string b(1, t.getPublicaChar());
        instrucao->setString(2, b);
        instrucao->setTimestamp(3, t.getCriada());
        instrucao->setInt(4, t.getIdUtilizador());
        instrucao->setInt(5, t.getIdUtilizadorDestino());
        instrucao->executeUpdate();
    } catch (SQLException ex) {
        cout << "Atualizar Tarefa Delegada Error Number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage();
        ligacao->rollback();
        return false;
    }
    ligacao->terminateStatement(instrucao);
    ligacao->commit();
    return true;
}

bool BDados::le(TarefaDelegada& t) {
    Statement* instrucao;

    string sqlStatement = "SELECT ACEITE, PUBLICA FROM " + tablePrefix + "TAREFA_DELEGADA WHERE TAREFA_CRIADA = :1 AND ID_UTILIZADOR = :2 AND ID_UTILIZADOR_DESTINO = :3";

    instrucao = ligacao->createStatement(sqlStatement);
    ResultSet* res;
    try {
        instrucao->setTimestamp(1, t.getCriada());
        instrucao->setInt(2, t.getIdUtilizador());
        instrucao->setInt(3, t.getIdUtilizadorDestino());
        res = instrucao->executeQuery();
        res->next();
        t.setAceiteChar(res->getString(1)[0]);
        t.setPublicaChar(res->getString(2)[0]);
        t.setNomeUtilizador(idUtilizadorToNome(t.getIdUtilizador()));
        t.setNomeUtilizadorDestino(idUtilizadorToNome(t.getIdUtilizadorDestino()));
    } catch (SQLException ex) {
        cout << "Leitura Tarefa Delegada error code: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        ligacao->rollback();
        return false;
    }
    ligacao->terminateStatement(instrucao);
    instrucao->closeResultSet(res);
    ligacao->commit();
    return true;
}

bool BDados::elimina(TarefaDelegada& t) {
    Statement* instrucao;
    string sqlStatement = "DELETE FROM " + tablePrefix + "TAREFA_DELEGADA WHERE TAREFA_CRIADA = :1 AND ID_UTILIZADOR = :2 AND ID_UTILIZADOR_DESTINO = :3";
    instrucao = ligacao->createStatement(sqlStatement);

    try {
        instrucao->setTimestamp(1, t.getCriada());
        instrucao->setInt(2, t.getIdUtilizador());
        instrucao->setInt(3, t.getIdUtilizadorDestino());
        instrucao->executeUpdate();
    } catch (SQLException ex) {
        cout << "Elimina Tarefa Delegada error code: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        ligacao->rollback();
        return false;
    }
    ligacao->terminateStatement(instrucao);
    ligacao->commit();
    return true;
}

string BDados::idUtilizadorToNome(int idUtilizador) {
    Statement* instrucao;
    string sqlStatement = "SELECT NOME FROM " + tablePrefix + "UTILIZADOR WHERE ID_UTILIZADOR = :1";
    instrucao = ligacao->createStatement(sqlStatement);
    ResultSet* res;
    try {
        instrucao->setInt(1, idUtilizador);
        res = instrucao->executeQuery();
        res->next();
        return res->getString(1);
    } catch (SQLException ex) {
        cout << "Ler Nome Utilizador: " << ex.getErrorCode();
        cout << ex.getMessage();
        ligacao->rollback();
        return "";
    }
}

long BDados::mediaDuracaoPorNomeParcial(string const parteDoNome) {
    Statement* instrucao;
    long mediaDuracao = 0;
    string sqlStatement = "select ( avg(extract( second from  duracao_efetiva) + "
            "extract ( minute from  duracao_efetiva) * 60 + "
            "extract ( hour   from  duracao_efetiva) * 3600 + "
            "extract ( day   from  duracao_efetiva) * 86400) ) / count(*) "
            "from " + tablePrefix + "tarefa_completa "
            "where duracao_efetiva is not null "
            "and lower(nome) like lower('%' || :1 || '%')";

    instrucao = ligacao->createStatement(sqlStatement);
    ResultSet* res;
    try {
        instrucao->setString(1, parteDoNome);
        res = instrucao->executeQuery();
        res->next();
        mediaDuracao += res->getInt(1);
    } catch (SQLException ex) {
        cout << "Ler id de Utilizador: " << ex.getErrorCode();
        cout << ex.getMessage();
        ligacao->rollback();
    }
}

int BDados::nomeUtilizadorToId(const string nome) {
    Statement* instrucao;
    string sqlStatement = "SELECT ID_UTILIZADOR FROM " + tablePrefix + "UTILIZADOR WHERE NOME = :1";
    instrucao = ligacao->createStatement(sqlStatement);
    ResultSet* res;
    try {
        instrucao->setString(1, nome);
        res = instrucao->executeQuery();
        res->next();
        return res->getInt(1);
    } catch (SQLException ex) {
        cout << "Ler id de Utilizador: " << ex.getErrorCode();
        cout << ex.getMessage();
        ligacao->rollback();
        return 0;
    }
}

bool BDados::adiciona(Contexto& c) {
    Statement *iContexto;
    string sqlStatement = "INSERT INTO " + tablePrefix + "CONTEXTO (ID_UTILIZADOR,NOME)"
            "VALUES(:1 , :2) RETURNING ID_CONTEXTO INTO :IDC";
    iContexto = ligacao->createStatement(sqlStatement);
    try {
        iContexto->setInt(1, c.idUtilizador);
        iContexto->setString(2, c.nome);
        iContexto->registerOutParam(3, OCCIINT);
        iContexto->executeUpdate();
        c.setIdContexto(iContexto->getInt(3));
    } catch (SQLException ex) {
        cout << "Contexto Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage();
        ligacao->rollback();
        return false;
    }
    ligacao->terminateStatement(iContexto);
    if (commit) ligacao->commit();
    return true;
}

bool BDados::atualiza(const Contexto& c) {
    Statement *iContexto;

    string sqlStatment = "UPDATE " + tablePrefix + "CONTEXTO SET NOME = :NOME "
            "WHERE ID_UTILIZADOR = :ID_UTILIZADOR AND ID_CONTEXTO = :ID_CONTEXTO";
    iContexto = ligacao->createStatement(sqlStatment);
    try {
        iContexto->setInt(2, c.idUtilizador);
        iContexto->setInt(3, c.idContexto);
        iContexto->setString(1, c.nome);
        iContexto->executeUpdate();
    } catch (SQLException ex) {
        cout << "Contexto Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage();
        ligacao->rollback();
        return false;
    }
    ligacao->terminateStatement(iContexto);
    if (commit) ligacao->commit();
    return true;
}

bool BDados::le(Contexto& c) {
    Statement* iContexto;

    string sqlStatement = "SELECT NOME FROM " + tablePrefix + "CONTEXTO "
            "WHERE ID_UTILIZADOR = :ID_UTILIZADOR AND ID_CONTEXTO = :ID_CONTEXTO";

    iContexto = ligacao->createStatement(sqlStatement);
    try {
        iContexto->setInt(1, c.idUtilizador);
        iContexto->setInt(2, c.idContexto);
        ResultSet* rset = iContexto->executeQuery();
        rset->next();
        c.setNome(rset->getString(1));
    } catch (SQLException ex) {
        cout << "Contexto Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return false;
    }
    ligacao->terminateStatement(iContexto);
    return true;
}

bool BDados::desligarContexto(const Contexto& c) {
    Statement* iContexto;
    string sqlStatement = "DELETE FROM " + tablePrefix + "CONTEXTOS_DA_TAREFA WHERE ID_UTILIZADOR = :ID_UTILIZADOR AND CONTEXTO_ID_CONTEXTO = :ID_CONTEXTO";
    iContexto = ligacao->createStatement(sqlStatement);
    try {
        iContexto->setInt(1, c.idUtilizador);
        iContexto->setInt(2, c.idContexto);
        iContexto->executeUpdate();
    } catch (SQLException ex) {
        cout << "Contextos_da_tarefa Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage();
        ligacao->rollback();
        return false;
    }
    ligacao->terminateStatement(iContexto);
    if (commit) ligacao->commit();
    return true;
}

bool BDados::elimina(const Contexto& c) {
    Statement* iContexto;
    string sqlStatement = "DELETE FROM " + tablePrefix + "CONTEXTO WHERE ID_UTILIZADOR = :ID_UTILIZADOR AND ID_CONTEXTO = :ID_CONTEXTO";
    iContexto = ligacao->createStatement(sqlStatement);
    try {
        iContexto->setInt(1, c.idUtilizador);
        iContexto->setInt(2, c.idContexto);
        iContexto->executeUpdate();
    } catch (SQLException ex) {
        cout << "Contexto Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage();
        ligacao->rollback();
        return false;
    }
    ligacao->terminateStatement(iContexto);
    if (commit) ligacao->commit();
    return true;
}

list<Contexto> BDados::listaContextosDeTarefa(const Tarefa& t) {
    //Falta qq coisa
    Statement* iContextoTarefa;
    list<Contexto> contextos;
    string sqlStatement = "SELECT CONTEXTO_ID_CONTEXTO FROM " + tablePrefix + "CONTEXTOS_DA_TAREFA WHERE TAREFA_CRIADA = :TAREFA_CRIADA AND ID_UTILIZADOR = :ID_UTILIZADOR";

    iContextoTarefa = ligacao->createStatement(sqlStatement);
    ResultSet* contextosDaTarefa;
    try {
        iContextoTarefa->setTimestamp(1, t.criada);
        iContextoTarefa->setInt(2, t.idUtilizador);
        contextosDaTarefa = iContextoTarefa->executeQuery();
    } catch (SQLException ex) {
        cout << "Contexto da tarefa Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage();
        ligacao->rollback();
        return contextos;
    }
    while (contextosDaTarefa->next()) {
        sqlStatement = "SELECT * FROM " + tablePrefix + "CONTEXTO WHERE ID_UTILIZADOR = :ID_UTILIZADOR AND ID_CONTEXTO = :ID_CONTEXTO";
        Statement* iContexto = ligacao->createStatement(sqlStatement);
        ResultSet* rset;
        try {
            iContexto->setInt(1, t.idUtilizador);
            iContexto->setInt(2, contextosDaTarefa->getInt(1));
            rset = iContexto->executeQuery();
            rset->next();
            Contexto* c = new Contexto(rset->getInt(1), rset->getInt(2), rset->getString(3));
            contextos.push_back(*c);
        } catch (SQLException ex) {
            cout << "Contexto Error number: " << ex.getErrorCode() << endl;
            cout << ex.getMessage();
            ligacao->rollback();
            return contextos;
        }
        iContexto->closeResultSet(rset);
        ligacao->terminateStatement(iContexto);
    }
    ligacao->terminateStatement(iContextoTarefa);
    return contextos;
}

list<Contexto> BDados::listaContextosDeUtilizador(const Utilizador& uti) {
    list<Contexto> contextos;
    Statement* instrucao;
    string sqlStatement = "SELECT * FROM " + tablePrefix + "CONTEXTO WHERE ID_UTILIZADOR = :1";
    instrucao = ligacao->createStatement(sqlStatement);
    ResultSet* res;
    try {
        instrucao->setInt(1, uti.GetIdUtilizador());
        res = instrucao->executeQuery();
        while (res->next()) {
            Contexto c(res->getInt(1), res->getInt(2), res->getString(3));
            contextos.push_back(c);
        }
    } catch (SQLException ex) {
        cout << "Lista contextos utilizador error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return contextos;
    }
    instrucao->closeResultSet(res);
    ligacao->terminateStatement(instrucao);
    return contextos;
}

bool BDados::adicionaAnotacaoPorFicheiro(AcaoBase b, string fileName, string tipoAno) {
    Statement *iInsert, *iSelect;
    string sqlStmt = "INSERT INTO " + tablePrefix + "anotacao (ID_UTILIZADOR, ACAOBASE_CRIADA, NOME, ID_TIPO_ANOTACAO) "
            "VALUES (:ID_UTILIZADOR, :ACAOBASE_CRIADA, :NOME, :ID_TIPO_ANOTACAO)  RETURNING ID_ANOTACAO INTO :IDC";
    iInsert = ligacao->createStatement(sqlStmt);

    iSelect = ligacao->createStatement("SELECT * FROM " + tablePrefix + "ANOTACAO_COM_TIPO WHERE ID_UTILIZADOR=:ID_UTILIZADOR AND ID_ANOTACAO= :ID_ANOTACAO for update");
    ResultSet *rset;
    try {
        iInsert->setInt(1, b.idUtilizador);
        iInsert->setTimestamp(2, b.criada);
        iInsert->setString(3, fileName);
        iInsert->setInt(4, tipoAnotacaoToIdTipoAnotacao(tipoAno));
        iInsert->registerOutParam(5, OCCIINT);
        iInsert->executeUpdate();
        int idAnotacao = iInsert->getInt(5);

        iSelect->setInt(1, b.idUtilizador);
        iSelect->setInt(2, idAnotacao);
        rset = iSelect->executeQuery();
        rset->next();
        Anotacao ano(rset->getInt(1), rset->getInt(3), rset->getString(4), rset->getString(6), rset->getBlob(7));

        ifstream inFile;
        size_t size = 0;

        inFile.open(fileName, ios::in | ios::binary | ios::ate);

        if (inFile.is_open()) {
            char* oData = 0;
            inFile.seekg(0, ios::end);
            size = inFile.tellg();
            inFile.seekg(0, ios::beg);
            oData = new char[size + 1];
            inFile.read(oData, size);
            ano.dados.open(OCCI_LOB_READWRITE);
            Stream *outstream = ano.dados.getStream(1, 0);
            outstream->writeBuffer(oData, size);
            char *c = (char *) "";
            outstream->writeLastBuffer(c, 0);

            ano.dados.closeStream(outstream);
            ano.dados.close();
            if (ano.dados.length() == 0)return false;
        } else {
            return false;
        }
    } catch (SQLException ex) {
        cout << "Anotacao de tarefa Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return false;
    }
    iSelect->closeResultSet(rset);
    ligacao->terminateStatement(iInsert);
    ligacao->terminateStatement(iSelect);
    if (commit) ligacao->commit();
    return true;
}

bool BDados::adicionaAnotacaoPorTexto(AcaoBase b, string nome, string tipoAno) {
    Statement *iInsert;
    string sqlStmt = "INSERT INTO " + tablePrefix + "anotacao (ID_UTILIZADOR, ACAOBASE_CRIADA, NOME, ID_TIPO_ANOTACAO) "
            "VALUES (:ID_UTILIZADOR, :ACAOBASE_CRIADA, :NOME, :ID_TIPO_ANOTACAO)";
    iInsert = ligacao->createStatement(sqlStmt);

    try {
        iInsert->setInt(1, b.idUtilizador);
        iInsert->setTimestamp(2, b.criada);
        iInsert->setString(3, nome);
        iInsert->setInt(4, tipoAnotacaoToIdTipoAnotacao(tipoAno));
        iInsert->executeUpdate();
    } catch (SQLException ex) {
        cout << "Anotacao de tarefa Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return false;
    }
    ligacao->terminateStatement(iInsert);
    if (commit) ligacao->commit();
    return true;
}

list<Anotacao> BDados::listaAnotacoesDeAcaoBase(AcaoBase t)const {
    list<Anotacao> listA;
    Statement *instrucao;
    instrucao = ligacao->createStatement("SELECT * FROM " + tablePrefix + "ANOTACAO_COM_TIPO WHERE ID_UTILIZADOR= :ID_UTILIZADOR AND ACAOBASE_CRIADA= :ACAOBASE_CRIADA");
    instrucao->setInt(1, t.idUtilizador);
    instrucao->setTimestamp(2, t.criada);
    ResultSet *rset = instrucao->executeQuery();
    while (rset->next()) {
        Anotacao ano(rset->getInt(1), rset->getInt(3), rset->getString(4), rset->getString(6), rset->getBlob(7));
        listA.push_back(ano);
    }
    instrucao->closeResultSet(rset);
    return listA;
}

list<Anotacao> BDados::listaAnotacoesDeReferencia(const Referencia & t)const {
    list<Anotacao> listA;
    Statement *instrucao;
    instrucao = ligacao->createStatement("SELECT * FROM " + tablePrefix + "ANOTACAO_COM_TIPO WHERE ID_UTILIZADOR= :ID_UTILIZADOR AND ID_REFERENCIA = :ID_REFERENCIA");
    instrucao->setInt(1, t.GetIdUtilizador());
    instrucao->setInt(2, t.GetIdReferencia());
    ResultSet *rset = instrucao->executeQuery();
    while (rset->next()) {
        Anotacao ano(rset->getInt(1), rset->getInt(3), rset->getString(4), rset->getString(6), rset->getBlob(7));
        listA.push_back(ano);
    }
    instrucao->closeResultSet(rset);
    return listA;
}

bool BDados::elimina(Anotacao& a) {
    Statement* iAnotacao;
    string sqlStmtTarefaContexto = "DELETE FROM " + tablePrefix + "ANOTACAO WHERE ID_UTILIZADOR= :ID_UTILIZADOR AND ID_ANOTACAO= :ID_ANOTACAO";
    iAnotacao = ligacao->createStatement(sqlStmtTarefaContexto);
    try {
        iAnotacao->setInt(1, a.idUtilizador);
        iAnotacao->setInt(2, a.idAnotacao);
        iAnotacao->executeUpdate();
    } catch (SQLException ex) {
        cout << "Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        ligacao->rollback();
        return false;
    }
    ligacao->terminateStatement(iAnotacao);
    if (commit) ligacao->commit();
    return true;
}

bool BDados::adiciona(const Tarefa& t, const Contexto& c) {
    Statement* iContextoTarefa;
    string sqlStatement = "INSERT INTO " + tablePrefix + "CONTEXTOS_DA_TAREFA (TAREFA_CRIADA,ID_UTILIZADOR,CONTEXTO_ID_CONTEXTO) "
            "VALUES (:TAREFA_CRIADA,:ID_UTILIZADOR,:CONTEXTO_ID_CONTEXTO)";
    iContextoTarefa = ligacao->createStatement(sqlStatement);

    try {
        iContextoTarefa->setTimestamp(1, t.criada);
        iContextoTarefa->setInt(2, c.idUtilizador);
        iContextoTarefa->setInt(3, c.idContexto);
        iContextoTarefa->executeUpdate();
    } catch (SQLException ex) {
        cout << "Contexto da tarefa Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage();
        ligacao->rollback();
        return false;
    }
    ligacao->terminateStatement(iContextoTarefa);
    if (commit) ligacao->commit();
    return true;
}

Projeto * BDados::projetoDaTarefa(const Tarefa & t) {
    Projeto * projeto = NULL;

    Statement *instrucao;
    string sqlStmt = "SELECT PROJ_ACAO_BASE_CRIADA FROM " + tablePrefix + "TAREFA WHERE ID_UTILIZADOR = :ID_UTILIZADOR AND criada= :criada";
    instrucao = ligacao->createStatement(sqlStmt);
    instrucao->setInt(1, t.idUtilizador);
    instrucao->setTimestamp(2, t.criada);
    ResultSet *rset = instrucao->executeQuery();
    try {
        rset->next();
        if (!rset->getTimestamp(1).isNull()) {
            AcaoBase ab(t.getIdUtilizador(), rset->getTimestamp(1));
            projeto = new Projeto(ab);
        }
    } catch (SQLException ex) {
        cout << "Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return NULL;
    }
    ligacao->terminateStatement(instrucao);


    if (projeto != NULL) le(*projeto);
    return projeto;
}

list<Projeto> BDados::listarProjetosDeUtilizador(Utilizador uti) {
    list<Projeto> listP;

    Statement *instrucao;
    instrucao = ligacao->createStatement("SELECT * FROM " + tablePrefix + "PROJETO_COMPLETO WHERE id_utilizador = :id_utilizador");
    instrucao->setInt(1, uti.GetIdUtilizador());
    ResultSet *rset = instrucao->executeQuery();
    while (rset->next()) {
        AcaoBase ab(rset->getInt(1), rset->getString(3), rset->getTimestamp(2),
                rset->getTimestamp(6), rset->getDate(5), rset->getDate(4),
                rset->getString(10), rset->getString(9));
        Projeto p(ab);
        listP.push_back(p);
    }
    instrucao->closeResultSet(rset);
    return listP;
}

bool BDados::transformarTarefaProjeto(Tarefa & t, Projeto &p) {
    Statement *iProjeto, *iTarefa, *iContextoTarefa;

    string sqlStmtTarefaContexto = "DELETE FROM " + tablePrefix + "CONTEXTOS_DA_TAREFA WHERE ID_UTILIZADOR= :ID_UTILIZADOR AND tAREFA_CRIADA= :CRIADA";
    iContextoTarefa = ligacao->createStatement(sqlStmtTarefaContexto);
    try {
        iContextoTarefa->setInt(1, t.idUtilizador);
        iContextoTarefa->setTimestamp(2, t.criada);
        iContextoTarefa->executeUpdate();
    } catch (SQLException ex) {
        cout << "Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        ligacao->rollback();
        return false;
    }
    ligacao->terminateStatement(iContextoTarefa);

    string sqlStmtTarefa = "DELETE FROM " + tablePrefix + "TAREFA WHERE ID_UTILIZADOR= :ID_UTILIZADOR AND CRIADA= :CRIADA";
    iTarefa = ligacao->createStatement(sqlStmtTarefa);
    try {
        iTarefa->setInt(1, t.idUtilizador);
        iTarefa->setTimestamp(2, t.criada);
        iTarefa->executeUpdate();
    } catch (SQLException ex) {
        cout << "Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return false;
    }
    ligacao->terminateStatement(iContextoTarefa);

    string sqlStmtProjeto = "INSERT INTO " + tablePrefix + "PROJETO (ACCAO_BASE_ID_UTILIZADOR, ACCAO_BASE_CRIADA) "
            "VALUES (:ID_UTILIZADOR, :CRIADA)";
    iProjeto = ligacao->createStatement(sqlStmtProjeto);
    try {
        iProjeto->setInt(1, t.idUtilizador);
        iProjeto->setTimestamp(2, t.criada);
        iProjeto->executeUpdate();
    } catch (SQLException ex) {
        cout << "Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        ligacao->rollback();
        return false;
    }
    ligacao->terminateStatement(iProjeto);
    if (commit) ligacao->commit();

    Projeto j((AcaoBase) t);
    p = j;
    return true;
}

bool BDados::removerTarefaDeProjeto(Tarefa t, Projeto p) {
    Statement *iTarefa;
    string sqlStmtTarefa = "UPDATE " + tablePrefix + "TAREFA SET PROJ_ACAO_BASE_CRIADA = NULL"
            " WHERE ID_UTILIZADOR= :ID_UTILIZADOR AND CRIADA= :CRIADA";
    iTarefa = ligacao->createStatement(sqlStmtTarefa);
    try {
        iTarefa->setInt(1, t.idUtilizador);
        iTarefa->setTimestamp(2, t.criada);
        iTarefa->executeUpdate();
    } catch (SQLException ex) {
        cout << "Tarefa Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        ligacao->rollback();
        return false;
    }
    ligacao->terminateStatement(iTarefa);
    if (commit) ligacao->commit();
    return true;
}

bool BDados::adicionarTarefaAProjeto(Tarefa t, Projeto p) {
    Statement *iTarefa;
    string sqlStmtTarefa = "UPDATE " + tablePrefix + "TAREFA SET PROJ_ACAO_BASE_CRIADA = :PROJ_ACAO_BASE_CRIADA"
            " WHERE ID_UTILIZADOR= :ID_UTILIZADOR AND CRIADA= :CRIADA";
    iTarefa = ligacao->createStatement(sqlStmtTarefa);
    try {
        iTarefa->setTimestamp(1, p.criada);
        iTarefa->setInt(2, t.idUtilizador);
        iTarefa->setTimestamp(3, t.criada);
        iTarefa->executeUpdate();
    } catch (SQLException ex) {
        cout << "Tarefa Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        ligacao->rollback();
        return false;
    }
    ligacao->terminateStatement(iTarefa);
    if (commit) ligacao->commit();
    return true;
}

list<Tarefa> BDados::listarTarefasDeProjeto(Projeto& p) {
    Statement *instrucao;
    std::list <Tarefa> ret;
    instrucao = ligacao->createStatement("SELECT * FROM " + tablePrefix + "TAREFA_COMPLETA WHERE ID_UTILIZADOR = :ID_UTILIZADOR AND PROJ_ACAO_BASE_CRIADA = :PROJ_ACAO_BASE_CRIADA");
    instrucao->setInt(1, p.idUtilizador);
    instrucao->setTimestamp(2, p.criada);

    ResultSet *rset = instrucao->executeQuery();
    while (rset->next()) {
        Tarefa ab(rset->getInt(1), rset->getString(3), rset->getTimestamp(2),
                rset->getTimestamp(5), rset->getDate(10), rset->getDate(4),
                rset->getString(7), rset->getString(9), rset->getIntervalDS(11),
                rset->getIntervalDS(12));
        ret.push_back(ab);
    }
    return ret;
    instrucao->closeResultSet(rset);
}

bool BDados::atualiza(Projeto & t) {
    Statement *iAcaoBase;
    string sqlStmtAcaoBase = "UPDATE " + tablePrefix + "ACCAO_BASE SET NOME = :NOME, "
            "DATA_INICIO = :DATA_INICIO, DATA_FIM = :DATA_FIM,"
            " ID_PRIORIDADE = :ID_PRIORIDADE , ID_ESTADO = :ID_ESTADO"
            " WHERE ID_UTILIZADOR = :ID_UTILIZADOR AND CRIADA = :CRIADA ";
    iAcaoBase = ligacao->createStatement(sqlStmtAcaoBase);
    try {
        iAcaoBase->setString(1, t.nome);
        iAcaoBase->setDate(2, t.inicio);
        iAcaoBase->setDate(3, t.fim);
        iAcaoBase->setInt(4, prioridadeToIdPrioridade(t.prioridade));
        iAcaoBase->setInt(5, estadoToIdEstado(t.estado));
        iAcaoBase->setInt(6, t.idUtilizador);
        iAcaoBase->setTimestamp(7, t.criada);
        iAcaoBase->executeUpdate();
    } catch (SQLException ex) {
        cout << "Acao Base Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return false;
    }
    ligacao->terminateStatement(iAcaoBase);
    if (commit) ligacao->commit();
    return true;
}

bool BDados::elimina(const Projeto & t) {
    Statement *iAcaoBase, *iProjeto, *iTarefa;

    string sqlStmtTarefa = "UPDATE " + tablePrefix + "TAREFA SET PROJ_ACAO_BASE_CRIADA = NULL WHERE ID_UTILIZADOR= :ID_UTILIZADOR AND PROJ_ACAO_BASE_CRIADA= :CRIADA";
    iTarefa = ligacao->createStatement(sqlStmtTarefa);

    try {
        iTarefa->setInt(1, t.idUtilizador);
        iTarefa->setTimestamp(2, t.criada);

        iTarefa->executeUpdate();
    } catch (SQLException ex) {
        cout << "Tarefa Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        ligacao->rollback();
        return false;
    }

    ligacao->terminateStatement(iTarefa);

    string sqlStmtProjeto = "DELETE FROM " + tablePrefix + "Projeto WHERE ACCAO_BASE_ID_UTILIZADOR= :ID_UTILIZADOR AND ACCAO_BASE_CRIADA= :CRIADA";
    iProjeto = ligacao->createStatement(sqlStmtProjeto);
    try {
        iProjeto->setInt(1, t.idUtilizador);
        iProjeto->setTimestamp(2, t.criada);
        iProjeto->executeUpdate();
    } catch (SQLException ex) {
        cout << "Projeto Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return false;
    }
    ligacao->terminateStatement(iProjeto);

    string sqlStmtAcaoBase = "DELETE FROM " + tablePrefix + "ACCAO_BASE WHERE ID_UTILIZADOR = :ID_UTILIZADOR AND CRIADA = :CRIADA";
    iAcaoBase = ligacao->createStatement(sqlStmtAcaoBase);
    try {
        iAcaoBase->setInt(1, t.idUtilizador);
        iAcaoBase->setTimestamp(2, t.criada);
        iAcaoBase->executeUpdate();
    } catch (SQLException ex) {
        cout << "Acao Base Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        ligacao->rollback();
        return false;
    }
    ligacao->terminateStatement(iAcaoBase);
    if (commit) ligacao->commit();
    return true;
}

bool BDados::adicionaDependenciaTarefa(Tarefa & t, Tarefa & tDep) {
    Statement *iTarefaDep;

    string sqlStmtTarefaDep = "INSERT INTO " + tablePrefix + "DEPENDENCIA_TAREFAS (ID_UTILIZADOR, CRIADA, CRIADA_DEP) "
            "VALUES (:ID_UTILIZADOR, :CRIADA, :CRIADA_DEP)";

    iTarefaDep = ligacao->createStatement(sqlStmtTarefaDep);
    try {
        iTarefaDep->setInt(1, t.idUtilizador);
        iTarefaDep->setTimestamp(2, t.criada);
        iTarefaDep->setTimestamp(3, tDep.criada);
        iTarefaDep->executeUpdate();
    } catch (SQLException ex) {
        cout << "Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        ligacao->rollback();
        return false;
    }

    ligacao->terminateStatement(iTarefaDep);
    if (commit) ligacao->commit();
    return true;
}

bool BDados::eliminaDependenciaTarefa(Tarefa & t, Tarefa & tDep) {
    Statement *iTarefaDep;
    ;
    string sqlStmtTarefaDep = "DELETE FROM " + tablePrefix + "DEPENDENCIA_TAREFAS WHERE ID_UTILIZADOR = :ID_UTILIZADOR AND CRIADA= :CRIADA AND CRIADA_DEP= :CRIADA_DEP";

    iTarefaDep = ligacao->createStatement(sqlStmtTarefaDep);
    try {
        iTarefaDep->setInt(1, t.idUtilizador);
        iTarefaDep->setTimestamp(2, t.criada);
        iTarefaDep->setTimestamp(3, tDep.criada);
        iTarefaDep->executeUpdate();
    } catch (SQLException ex) {
        cout << "Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        ligacao->rollback();
        return false;
    }
    ligacao->terminateStatement(iTarefaDep);
    if (commit) ligacao->commit();
    return true;
}

bool BDados::transformarTarefaReferencia(Tarefa & t, Referencia &r) {
    Statement *iReferencia, *iAnotacao;

    string sqlStmtProjeto = "INSERT INTO " + tablePrefix + "REFERENCIA (ID_UTILIZADOR, NOME) "
            "VALUES (:ID_UTILIZADOR, :NOME)  RETURNING ID_referencia INTO :IDR";
    iReferencia = ligacao->createStatement(sqlStmtProjeto);
    try {
        iReferencia->setInt(1, t.idUtilizador);
        iReferencia->setString(2, t.nome);
        iReferencia->registerOutParam(3, OCCIINT);
        iReferencia->executeUpdate();
        r.SetIdReferencia(iReferencia->getInt(3));
        r.SetIdUtilizador(t.idUtilizador);
        r.SetNome(t.nome);
    } catch (SQLException ex) {
        cout << "Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        ligacao->rollback();
        return false;
    }
    ligacao->terminateStatement(iReferencia);
    string sqlStmtTarefa = "UPDATE " + tablePrefix + "ANOTACAO SET ACAOBASE_CRIADA = NULL, ID_REFERENCIA = :ID_REFERENCIA WHERE ID_UTILIZADOR= :ID_UTILIZADOR AND ACAOBASE_CRIADA= :CRIADA";
    iAnotacao = ligacao->createStatement(sqlStmtTarefa);

    try {
        iAnotacao->setInt(1, r.idReferencia);
        iAnotacao->setInt(2, t.idUtilizador);
        iAnotacao->setTimestamp(3, t.criada);
        iAnotacao->executeUpdate();
    } catch (SQLException ex) {
        cout << "Update Anotacao Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        ligacao->rollback();
        return false;
    }
    elimina(t);
    ligacao->terminateStatement(iAnotacao);
    if (commit) ligacao->commit();
    return true;
}

list<Referencia> BDados::listaReferenciasDeUtilizador(Utilizador u) {
    list<Referencia> listR;
    Statement *instrucao;
    instrucao = ligacao->createStatement("SELECT * FROM " + tablePrefix + "REFERENCIA WHERE id_utilizador = :id_utilizador");
    instrucao->setInt(1, u.GetIdUtilizador());
    ResultSet *rset = instrucao->executeQuery();
    while (rset->next()) {
        Referencia r(rset->getInt(1), rset->getInt(2), rset->getString(3));
        listR.push_back(r);
    }
    instrucao->closeResultSet(rset);
    return listR;
}

bool BDados::elimina(Referencia & r) {
    Statement *iReferencia;
    string sqlStmtTarefaDep = "DELETE FROM " + tablePrefix + "REFERENCIA WHERE ID_UTILIZADOR = :ID_UTILIZADOR AND ID_REFERENCIA = :ID_REFERENCIA";

    iReferencia = ligacao->createStatement(sqlStmtTarefaDep);
    try {
        iReferencia->setInt(1, r.idUtilizador);
        iReferencia->setInt(2, r.idReferencia);
        iReferencia->executeUpdate();
    } catch (SQLException ex) {
        cout << "Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        ligacao->rollback();
        return false;
    }
    ligacao->terminateStatement(iReferencia);
    if (commit) ligacao->commit();
}

ListAdjGrafo<Tarefa, int> BDados::criaGrafodependenciaTarefas(Utilizador u) {
    Statement *instrucao;
    ListAdjGrafo<Tarefa, int> grafo;

    instrucao = ligacao->createStatement("SELECT * FROM " + tablePrefix + "DEPENDENCIA_TAREFAS WHERE id_utilizador = :id_utilizador");
    instrucao->setInt(1, u.GetIdUtilizador());

    ResultSet *rset = instrucao->executeQuery();
    while (rset->next()) {
        Timestamp criada = rset->getTimestamp(1);
        Timestamp criada_dep = rset->getTimestamp(2);
        int idUtilizador = rset->getInt(3);

        Tarefa t(idUtilizador, criada);
        Tarefa t_dep(idUtilizador, criada_dep);

        le(t);
        le(t_dep);

        grafo.juntar_vertice(t);
        grafo.juntar_vertice(t_dep);
        grafo.juntar_ramo(1, t, t_dep);
    }
    instrucao->closeResultSet(rset);
    return grafo;
}

list<Tarefa> BDados::listaTarefasDependentes(Tarefa & t) {
    Statement *instrucao;
    list<Tarefa> listTDep;

    instrucao = ligacao->createStatement("SELECT * FROM " + tablePrefix + "DEPENDENCIA_TAREFAS WHERE id_utilizador = :id_utilizador and criada = :criada");
    instrucao->setInt(1, t.getIdUtilizador());
    instrucao->setTimestamp(2, t.getCriada());
    ResultSet *rset = instrucao->executeQuery();
    while (rset->next()) {
        Timestamp criada_dep = rset->getTimestamp(2);
        int idUtilizador = rset->getInt(3);
        
        Tarefa tarefaDependente(idUtilizador, criada_dep);
        le(tarefaDependente);
        listTDep.push_back(tarefaDependente);
    }
    instrucao->closeResultSet(rset);
    return listTDep;
}

ListAdjGrafo<Tarefa, int> BDados::criaGrafodependenciaTarefas(Projeto j) {
    Statement *instrucao;
    ListAdjGrafo<Tarefa, int> grafo;

    instrucao = ligacao->createStatement("SELECT D.ID_UTILIZADOR, d.criada, d.criada_dep FROM " + tablePrefix + "DEPENDENCIA_TAREFAS D, " + tablePrefix + "TAREFA T, " + tablePrefix + "TAREFA TDEP "
            " WHERE d.id_utilizador = :1 and t.id_utilizador = :2 and tdep.id_utilizador = :3 "
            " and t.criada = d.criada and tdep.criada = d.criada_dep and "
            "(t.proj_acao_base_criada = :proj1 or tdep.proj_acao_base_criada= :proj2)");

    instrucao->setInt(1, j.idUtilizador);
    instrucao->setInt(2, j.idUtilizador);
    instrucao->setInt(3, j.idUtilizador);

    instrucao->setTimestamp(4, j.criada);
    instrucao->setTimestamp(5, j.criada);

    ResultSet *rset = instrucao->executeQuery();
    while (rset->next()) {
        int idUtilizador = rset->getInt(1);
        Timestamp criada = rset->getTimestamp(2);
        Timestamp criada_dep = rset->getTimestamp(3);

        Tarefa t(idUtilizador, criada);
        Tarefa t_dep(idUtilizador, criada_dep);
        le(t);
        le(t_dep);
        grafo.juntar_vertice(t);
        grafo.juntar_vertice(t_dep);
        grafo.juntar_ramo(1, t, t_dep);
    }
    instrucao->closeResultSet(rset);
    return grafo;




}

int BDados::contextoToIdContexto(string c) {
    int idContexto;
    Statement * instrucao;
    string sqlStatement = "SELECT ID_CONTEXTO FROM " + tablePrefix + "CONTEXTO WHERE NOME = :1";
    instrucao = ligacao->createStatement(sqlStatement);
    ResultSet* res;
    try {
        instrucao->setString(1, c);
        res = instrucao->executeQuery();
        res->next();
        idContexto = res->getInt(1);
    } catch (SQLException ex) {
        cout << "contextoToIdContexto Error number: " << ex.getErrorCode() << endl;
        cout << ex.getMessage() << endl;
        return -1;
    }
    ligacao->terminateStatement(instrucao);
    return idContexto;
}
