/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              virtual void escreve(ostream &out) const                     */
/*              virtual const AcaoBase& operator = (const AcaoBase &p)       */
/*              bool operator ==(const AcaoBase &a) const                    */
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/



#ifndef ACAOBASE_H
#define	ACAOBASE_H
#include <iostream>
#include <string>
#include <iomanip>
#include <occi.h>
#include "Anotacao.h"

using namespace std;
using namespace oracle::occi;

class AcaoBase{
     friend class BDados;
protected:
        int idUtilizador;
        string nome;
        Timestamp criada;
        Timestamp modificada;
        Date fim;
        Date inicio;
        string prioridade;
        string estado;
        list<Anotacao> * anotacoes;
public:
        AcaoBase(); 
        AcaoBase(const AcaoBase& a);
//        AcaoBase(const Anotacao& an);
        AcaoBase(int idUtilizador,const Timestamp criada);
        AcaoBase( int idUtilizador, string nome, Timestamp criada, Timestamp modificada,
        Date fim, Date inicio, string prioridade, string estado, list<Anotacao> * anotacoes);
        AcaoBase( int idUtilizador, string nome, Timestamp criada, Timestamp modificada,
        Date fim, Date inicio, string prioridade, string estado);
        AcaoBase(int idUtilizador);
        ~AcaoBase();    
        list<Anotacao> * getAnotacoes() const; 
        void setEstado(string estado);
        string getEstado() const ;
        void setAnotacoes(list<Anotacao> * anotacoes);
        void setPrioridade(string prioridade);     
        string getPrioridade() const;
        void setInicio(Date inicio) ;
        Date getInicio() const ;
        void setFim(Date fim) ;
        Date getFim() const;
        void setNome(string nome);
        string getNome() const ;
        int getIdUtilizador() const;    
        Timestamp getModificada() const;
        Timestamp getCriada() const;  
        virtual void escreve(ostream &out) const;
        virtual const AcaoBase& operator = (const AcaoBase &p);
        bool operator ==(const AcaoBase &a) const;        
};

ostream& operator << (ostream &out, const AcaoBase &p);
#endif	/* ACAOBASE_H */

