/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*               void escreve(ostream &out) const                            */
/*               const Projeto& operator =(const Projeto &p                  */
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/


#include "Projeto.h"

Projeto::Projeto() : AcaoBase() {
}

Projeto::Projeto(AcaoBase b) : AcaoBase(b) {
}

list<Tarefa> Projeto::GetTarefas() const {
    return tarefas;
}

void Projeto::SetTarefas(list<Tarefa> tarefas) {
    this->tarefas = tarefas;
}

const Projeto& Projeto::operator =(const Projeto &p) {
AcaoBase::operator=(p);
this->tarefas=p.tarefas;
return *this;
}

void Projeto::escreve(ostream &out) const{
    AcaoBase::escreve(out);
 }

ostream& operator << (ostream &out, const Projeto &p)
{
  p.escreve(out);
  return out;
}