/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              void escreve(ostream& out) const                             */
/*              const Contexto& operator=(const Contexto& c)                 */
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/


#include "Contexto.h"

Contexto::Contexto(){}

Contexto::Contexto(int idUtilizador, const string& nome)
{
    this->idUtilizador = idUtilizador;
    this->nome = nome;
}

Contexto::Contexto(int idUtilizador, int idContexto, const string& nome)
{
    this->idUtilizador = idUtilizador;
    this->idContexto = idContexto;
    this->nome = nome;
}

int Contexto::getIdContexto() const
{
    return idContexto;
}

int Contexto::getIdUtilizador() const
{
    return idUtilizador;
}

string Contexto::getNome() const
{
    return nome;
}

void Contexto::setIdContexto(int idContexto)
{
    this->idContexto = idContexto;
}

void Contexto::setNome(const string& nome)
{
    this->nome = nome;
}

void Contexto::escreve(ostream& out) const
{
    out << "ID_Utilizador: " << idUtilizador << endl
        << "ID_Contexto: " << idContexto << endl
        << "Nome: " << nome << endl;
}

const Contexto& Contexto::operator =(const Contexto& c)
{
    idUtilizador = c.idUtilizador;
    idContexto = c.idContexto;
    nome = c.nome;
    return *this;
}

ostream& operator << (ostream& out,const Contexto& c)
{
    c.escreve(out);
    return out;
}