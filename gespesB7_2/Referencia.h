/*****************************************************************************/
/* Identificação do programa: GPL-LAPR3                                      */
/* Nome:    Bruno Flávio, Joana Verde, Luís Teixeira, Franscisco Moreira     */
/* Data:    11/Dezembro/2012                                                 */
/* Descrição: Gestão Pessoal                                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Conteúdo:                                                                 */
/*      Instruções de compilação                                             */
/*      Includes                                                             */
/*      Funções:                                                             */
/*              void escreve(ostream &out) const                             */                                                   
/*      Classes:                                                             */
/*              API                                                          */
/*              Menu                                                         */
/*              Utilizador                                                   */
/*              Tarefa                                                       */
/*              TarefaDelegada                                               */
/*              Projeto                                                      */
/*              Referencia                                                   */
/*              AcaoBase                                                     */
/*              AcaoTemporal                                                 */
/*              Anotacao                                                     */
/*              Contexto                                                     */
/*              Estado                                                       */
/*              Temporal                                                     */
/*              Geografico                                                   */
/*              Prioridade                                                   */
/*              BDDAdos                                                      */
/*              CSVio                                                        */
/*              Lista                                                        */
/*              Lista_Iterador                                               */
/*              PriorityQueue                                                */
/*              PriorityQueueTarefa                                          */
/*              Queue                                                        */
/*              Stack                                                        */
/*              Vertice                                                      */
/*              ListAdjGrafo                                                 */
/*              No                                                           */
/*              Ramo                                                         */
/*                                                                           */
/*****************************************************************************/


#ifndef REFERENCIA_H
#define	REFERENCIA_H
#include <iostream>
#include <string>
using namespace std;


class Referencia {

    friend class BDados;
    
private:
    int idReferencia;
    int idUtilizador;
    string nome;
public:
    Referencia();
    Referencia(int idReferencia,int idUtilizador,string nome);
    void SetNome(string nome);
    string GetNome() const;
    void SetIdUtilizador(int idUtilizador);
    int GetIdUtilizador() const;
    void SetIdReferencia(int idReferencia);
    int GetIdReferencia() const;
 void escreve(ostream &out) const;
    
};

ostream& operator <<(ostream &out, const Referencia &p);

#endif	/* REFERENCIA_H */

