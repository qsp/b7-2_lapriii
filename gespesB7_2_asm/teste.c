#include <stdio.h>
#include <stdlib.h>
#include "fcnasm.h"

int main(void)
{
  char str[]="100110101\0";
  int vec[5];

  contextosAcao(str,vec,5);

  int i;
  for(i=0;i<5;i++)
    printf("[%d] = %d\n",i,vec[i]);
  
  printf("Maior contexto %d\n",maiorContextoAcao(vec,5));

  printf("Numero de contextos %d\n", numeroContextosAcao(vec,5));

  int prioridades[6];
  for(i=0;i<6;i++)
    prioridades[i] = 0; 
  char str1[]="5 ,Testar o método ,3 ,10001";
  char str2[]="21,Escrever o método\\, só depois de \\,escrever o teste,4,11";
  char str3[]="8,Actualizar tabela de requisitos,1,01";
  char str4[]="8,Actualizar\\, tabela de requisitos,1,01";
  char str5[]="8,Actualizar tabela de requisitos,4,01";
  contarAcoesPrioridade(str1,prioridades,2);
  contarAcoesPrioridade(str2,prioridades,2);
  contarAcoesPrioridade(str3,prioridades,2);
  contarAcoesPrioridade(str4,prioridades,2);
  contarAcoesPrioridade(str5,prioridades,2);

  char tmp[1024];
  char* split;
  split = parse(str2,tmp,',');
  split = parse(split,tmp,',');

  for(i=0;i<6;i++)
    printf("Prioridade %d - %d\n", i+1,prioridades[i]);
 
  printf("Split: %s\nResult: %s",split,tmp);

  return 0;
}
