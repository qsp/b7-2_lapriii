#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <sys/mman.h>
#include <fcntl.h>
#include "fcnasm.h"

#define NUMERO_CONTEXTOS 5
#define NUMERO_PRIORIDADES 6
#define POSICAO_VIRGULA 2

typedef struct {
    int numeroAcao;
    char nomeAcao[1024];
    int prioridade;
    int contextos[NUMERO_CONTEXTOS];
} tarefa;

int main(int argc, char** argv) {
    FILE* file;
    char line[1024];
    tarefa* tarefas;
    int tamanhoTarefas = 1;
    int numTarefas = 0;
    int contadorPrioridades[NUMERO_PRIORIDADES];
    int i, j;

    //Verifica se o programa foi chamado com o número minimo de argumentos
    if(argc<2){
      fprintf(stderr,"Número errado de argumentos\n"
	     "Use a opção \"--help\" para obter mais informações\n");
      exit(EXIT_FAILURE);
    }
    
    //imprime ajuda ao utilizador
    if (argc > 1 && strcmp(argv[1], "--help") == 0) {
        printf("Usage readcsv [options] file...\n"
                "\nOptions:\n"
                "--help\t\tMostra informção de ajuda\n"
                "-c\t\tMostra contextos das acções\n"
                "-mc\t\tMostra o maior contexto de cada uma das acções\n"
                "-nc\t\tMostra o número contextos de cada acção\n"
                "-p <int>\tMostra todas as acções com a prioridade indicada\n");
        exit(EXIT_SUCCESS);
    }

    //inicializa o array contadorPrioridades a zero(necessário para a chamada a função contarAcoesPrioridade) 
    for(i=0;i < NUMERO_PRIORIDADES;i++)
      contadorPrioridades[i] = 0;

    file = fopen(argv[argc - 1], "r");
    //verifica se o ficheiro foi aberto
    if (file == NULL) {
        fprintf(stderr, "Erro ao abrir %s", argv[0]);
        exit(EXIT_FAILURE);
    }

    //aloca memoria para um array de estruturas tarefa
    tarefas = (tarefa*) malloc(tamanhoTarefas * sizeof (tarefa));
    //verifica se conseguiu alocar memoria
    if(!tarefas){
      printf("Problema ao alocar memória");
      exit(EXIT_FAILURE);
    }

    j = 0;
    
    //leitura do ficheiro para o vector de estruturas
    while (fgets(line, sizeof (line), file) != NULL) {
        numTarefas++;
        if (numTarefas >= tamanhoTarefas) {
            tamanhoTarefas *= 2;
            tarefa* temp = (tarefa*) realloc(tarefas, tamanhoTarefas * sizeof(tarefa));
            if (!temp) {
                printf("Problema ao realocar memória, o ficheiro poderá não ter sido completamente lido");
                break;
            }
            tarefas = temp;
        }
	char* split;
	char token[1024];
	contarAcoesPrioridade(line,contadorPrioridades,POSICAO_VIRGULA);
        split = parse(line,token,',');
        tarefas[j].numeroAcao = atoi(token);
        split = parse(split,token, ',');
        strcpy(tarefas[j].nomeAcao,token);
        split = parse(split,token, ',');
        tarefas[j].prioridade = atoi(token);
        split = parse(split,token, ',');
        contextosAcao(token,tarefas[j].contextos,NUMERO_CONTEXTOS);
	j++;
    }
    
    //fecha o ficheiro
    fclose(file);
    
    //verifica a opção escolhida pelo o utilizador
    if (strcmp(argv[1], "-c") == 0) {
      for(i=0; i < numTarefas; i++){
	printf("\nNome: %s\nContextos: ",tarefas[i].nomeAcao);
	for(j=0;j < NUMERO_CONTEXTOS;j++)
	  if(tarefas[i].contextos[j])
	    printf("[%d]",tarefas[i].contextos[j]);
	printf("\n");
      }
    } else if (strcmp(argv[1], "-mc") == 0) {
      for(i=0; i < numTarefas; i++){
	printf("\nNome: %s\nMaior contexto: %d\n",tarefas[i].nomeAcao,maiorContextoAcao(tarefas[i].contextos,NUMERO_CONTEXTOS));
      }
    } else if (strcmp(argv[1], "-nc") == 0) {
      for(i=0; i < numTarefas; i++){
	printf("\nNome: %s\nNumero de contextos: %d\n",tarefas[i].nomeAcao,numeroContextosAcao(tarefas[i].contextos,NUMERO_CONTEXTOS));
      }
    } else if (strcmp(argv[1], "-p") == 0) {
        if (argc < 4) {
            fprintf(stderr, "Número errado de argumentos para a opção -p <int>\n");
            exit(EXIT_FAILURE);
        }
	int posicaoArray = atoi(argv[2]) - 1;
	printf("Número de ações com prioridade %s: %d\n",argv[2],contadorPrioridades[posicaoArray]);
    } else {
        printf("Tarefas: \n");
        for (i = 0; i < numTarefas; i++){
            printf("\nNr: %d\n"
                "Nome: %s\n"
                "Prioridade: %d\n"
                "Contextos: ", tarefas[i].numeroAcao, tarefas[i].nomeAcao, tarefas[i].prioridade);
	    int j;
	    for(j=0;j<NUMERO_CONTEXTOS;j++){
	      if(tarefas[i].contextos[j])
		printf("[%d]",tarefas[i].contextos[j]);
	    }
	    printf("\n");
	}
    }
    //liberta a memoria alocada para o array de tarefas
    free(tarefas);
    exit(EXIT_SUCCESS);
}
