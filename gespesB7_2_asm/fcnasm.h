#ifndef FCNASM_H
#define	FCNASM_H

/*
************************************************************************
Todas as funções declaradas neste ficheiro estão implentadas em Assembly
************************************************************************
*/

/*
************************************************************************
Função que dada uma string no formato "10101" preenche um vector com os 
contextos de uma acção
************************************************************************
*/
void contextosAcao(char* string,int* contextos,int tamanho);

/*
************************************************************************
Função que dado um vector com os contextos de uma acção devolve o maior
desses contextos
************************************************************************
*/
int maiorContextoAcao(int* contextos,int tamanho);

/*
************************************************************************
Função que dado um vector com os contextos de uma acção devolve o número
de contextos da acção
************************************************************************
*/
int numeroContextosAcao(int* contextos,int tamanho);

/*
************************************************************************
Função que dada uma linha do ficheiro e a posição da última virgula
antes do campo correspondente à prioridade, incrementa no vector a
posição correspondente a essa mesma prioridade
************************************************************************
*/
void contarAcoesPrioridade(char* str, int* prioridades,int posicao);

/*
************************************************************************

---AVISO-A string entrada é destruida como consequência desta função---

Função que dada uma linha do ficheiro copia para a string saida a todos
os caracteres presentes na string entrada até encontrar o separador
indicado que é substituido por '\0'. Caso o separador seja precedido de 
'\' é ignorado. Para a além de preencher o token a função retorna um 
apontador para a posição seguinte ao separador, destruindo assim a
string de entrada 

############################Exemplo#####################################
char entrada[]="21,Escrever o método\\, só depois de \\,escrever o teste,4,11";
char separador = ',';

Resultados após:
1ª Chamada:
split = "Escrever o método\\, só depois de \\,escrever o teste,4,11"
saida = "21"
2ª Chamada:
split = "4,11"
saida = "Escrever o método, só depois de ,escrever o teste"

************************************************************************
*/
char* parse(char* entrada,char* saida,char separador);

#endif	/* FCNASM_H */

