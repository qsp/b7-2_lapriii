	.text
	.global parse

parse:
	pushl %ebp
	movl %esp,%ebp

	pushl %ebx
	pushl %ecx
	pushl %esi
	pushl %edi

	movl 8(%ebp),%esi
	movl 12(%ebp),%edi
	movb 16(%ebp),%cl
	movl $0,%ebx

ciclo:	
	movb (%esi),%ch
	cmpb $0,%ch
	je fim_copia
	cmpb $'\\',%ch #se o caracter for '\' ignora e copia o proximo
		       #caracter mesmo que esta seja o separador
	je ignora
	cmpb %ch,%cl #se encontrar o separador termina a copia
	je fim_copia
	movb %ch,(%edi)
	incl %edi
	incl %esi
	jmp ciclo
ignora:
	incl %esi
	movb (%esi),%ch
	movb %ch,(%edi)
	incl %edi
	incl %esi
	jmp ciclo
fim_copia:
	movb $0,(%edi) #coloca o '\0' para terminar a string
	incl %esi
	movl %esi,%eax #devolve um apontador para o caracter depois do separador

	popl %edi
	popl %esi
	popl %ecx
	popl %ebx
	movl %ebp,%esp
	popl %ebp
	ret
