	.text
	.global numeroContextosAcao

numeroContextosAcao:
	pushl %ebp
	movl %esp,%ebp

	pushl %ecx
	pushl %esi

	movl 8(%ebp),%esi
	movl 12(%ebp),%ecx
	movl $0,%eax
	
ciclo:
	cmpl $0,(%esi) #se o valor for 0 salta para o fim, pois restantes também serão
	je fim
	addl $4,%esi
	loop ciclo

fim:
	movl 12(%ebp),%eax
	subl %ecx,%eax
	popl %esi
	popl %ecx
	movl %ebp,%esp
	popl %ebp
	ret
