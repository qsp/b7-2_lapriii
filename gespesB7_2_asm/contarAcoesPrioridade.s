	.text
	.global contarAcoesPrioridade

contarAcoesPrioridade:
	pushl %ebp
	movl %esp,%ebp

	pushl %ebx
	pushl %ecx
	pushl %edx
	pushl %esi
	pushl %edi

	movl 8(%ebp),%esi
	movl 12(%ebp),%edi
	movl 16(%ebp),%edx
	movl $0,%ecx
	movl $0,%ebx
ciclo:
	cmpb $0,(%esi)
	je fim
	cmpb $'\\',(%esi) #se o caracter for '\' ignora o seguinte
	je ignora
	cmpb $',',(%esi) #se encontrar uma ',' incrementa o contador
	jne proximo
	incl %ecx
	cmpl %ecx,%edx #verifica se já se encontra na posição certa
	je encontrado
proximo:
	incl %esi
	jmp ciclo
ignora:
	addl $2,%esi
	jmp ciclo
encontrado:	   #converte para inteiro os caracteres até à próxima virgula
	movl $0,%eax
	incl %esi
	cmpb $',',(%esi)
	je incrementa
	movb (%esi),%al
	subl $48,%eax
	addl %eax,%ebx
	jmp encontrado
incrementa:       #incrementa a posição do array correspondente à prioridade encontrada
	subl $1,%ebx
	imull $4,%ebx
	addl %ebx,%edi
	incl (%edi)
fim:
	popl %edi
	popl %esi
	popl %edx
	popl %ecx
	popl %ebx
	movl %ebp,%esp
	popl %ebp
	ret
