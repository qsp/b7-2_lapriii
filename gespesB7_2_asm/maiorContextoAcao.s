	.text
	.global maiorContextoAcao

maiorContextoAcao:
	pushl %ebp
	movl %esp,%ebp

	pushl %ebx
	pushl %ecx
	pushl %esi

	movl $0,%eax
	movl $0,%ebx
	movl 8(%ebp),%esi
	movl 12(%ebp),%ecx

ciclo:
	cmpl %ebx,%ecx #verifica se chegou ao fim do array
	je fim
	cmpl $0,(%esi) #verfica se a posição do array tem um valor diferente de 0
		       #se sim esse valor passa a ser o maior contexto
	je falso
	movl (%esi),%eax
falso:
	incl %ebx
	addl $4,%esi
	jmp ciclo
fim:
	popl %esi
	popl %ecx
	popl %esi
	movl %ebp,%esp
	popl %ebp
	ret
