	.text	
	.global contextosAcao

contextosAcao:
	pushl %ebp
	movl %esp,%ebp

	#proteger registos
	pushl %ebx
	pushl %ecx
	pushl %edx
	pushl %esi
	pushl %edi

	#aceder ao valor dos parametros
	movl 8(%ebp),%esi
	movl 12(%ebp),%edi
	movl $0,%ecx
	movl $0,%edx

ciclo:
	cmpl 16(%ebp),%ecx #verifica se já atingiu o número máximo de contextos
	je fim_string
	movb (%esi),%bl 
	cmpb $0,%bl #verifica se chegou ao fim da string
	je fim_string
	cmpb $'1',%bl #verifica se o se o contexto existe
	je verdadeiro
	movl $0,(%edi)
	jmp seguinte
verdadeiro:
	incl %edx
	movl %ecx,(%edi)
	incl (%edi)
	addl $4,%edi
seguinte:
	incl %esi
	incl %ecx
	jmp ciclo
fim_string:
	cmpl 16(%ebp),%edx
	je fim
	movl $0,(%edi)
	addl $4,%edi
	incl %edx
	jmp fim_string
fim:
	popl %edi
	popl %esi
	popl %ecx
	popl %ebx
	movl %ebp,%esp
	popl %ebp
	ret
